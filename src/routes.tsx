export default {
  home: {
    path: "/",
    exact: false,
    protected: true
  },
  homepage: {
    path: "/homepage",
    exact: false,
    protected: false
  },
  login: {
    path: "/login",
    exact: false,
    protected: false
  },
  apply: {
    path: "/apply",
    exact: false,
    protected: false
  },
  signup: {
    path: "/signup",
    exact: false,
    protected: false
  },
  resetPassword: {
    path: "/reset-password/:token",
    exact: false,
    protected: false
  },
  requestResetPassword: {
    path: "/reset-request",
    exact: true,
    protected: false
  },

  verifyEmail: {
    path: "/verify-email",
    exact: true,
    protected: false
  },

  contact: {
    path: "/contact",
    exact: true,
    protected: false
  },

  myClients: {
    path: "/clients",
    exact: false,
    protected: true
  },

  accountSettings: {
    path: "/account",
    exact: false,
    protected: true
  },

  accountManager: {
    path: "/manage-accounts",
    exact: false,
    protected: true
  },

  casesApplications: {
    path: "/applications",
    exact: false,
    protected: true
  },

  create: {
    path: "/create",
    exact: false,
    protected: true
  },

  detail: {
    path: "/:id",
    exact: false,
    protected: true
  },

  createContract: {
    path: "/:id/create-contract",
    exact: false,
    protected: true
  },

  editContract: {
    path: "/:id/edit-contract/:contractId",
    exact: false,
    protected: true
  }
};
