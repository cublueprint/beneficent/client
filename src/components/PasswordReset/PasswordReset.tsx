import {
  Button,
  Container,
  createMuiTheme,
  createStyles,
  IconButton,
  InputAdornment,
  makeStyles,
  TextField,
  ThemeProvider,
  Typography
} from "@material-ui/core";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import axios, { AxiosRequestConfig } from "axios";
import { Formik } from "formik";
import * as React from "react";
import { useHistory, useParams } from "react-router-dom";
import * as Yup from "yup";
import FormCardLayout from "../common/FormCardLayout/FormCardLayout";
import "./PasswordReset.scss";
import checkmark from "../../assets/blue_check_24px.png";

/**
 * Custom MUI Theme to add custom blue colour
 */
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "rgba(0, 0, 0, 0.87);"
    }
  }
});

/**
 * Add success style from LoginForm
 */
const useStyles = makeStyles(() =>
  createStyles({
    success: {
      borderColor: (props: any) => (!props.error && props.value ? "#2ED47A" : undefined)
    },
    absolute: {
      position: "absolute",
      top: "56px"
    }
  })
);
/**
 * Password Field Component with visibility toggle
 * @param props TextFieldProps from material-ui TextField
 */
const PasswordField = (props: any) => {
  const [show, setShow] = React.useState(false);

  const cls = useStyles(props);

  return (
    <TextField
      className="text-field"
      variant="outlined"
      fullWidth
      type={show ? "text" : "password"}
      InputProps={{
        classes: {
          notchedOutline: cls.success
        } as any,
        endAdornment: (
          <InputAdornment position="end">
            <IconButton onClick={() => setShow(!show)} aria-label="Toggle Password Visibility">
              {show ? <Visibility /> : <VisibilityOff />}
            </IconButton>
          </InputAdornment>
        )
      }}
      FormHelperTextProps={{
        classes: {
          root: cls.absolute
        }
      }}
      {...props}
    />
  );
};

/**
 * Success Message Props
 * @property On click event for go to login button
 */
type SuccessMessageProps = {
  redirectToLogin: (event: React.MouseEvent<HTMLButtonElement>) => void;
};

/**
 * Success message when password reset is successful
 * @param param0 - Object with redirectedToLogin property: function called when go to login button is clicked
 */
const SuccessMessage: React.FC<SuccessMessageProps> = ({ redirectToLogin }) => {
  return (
    <div className="success-message">
      <div className="centered">
        <img alt="check-mark" className="check" src={checkmark} />
      </div>
      <Typography className="heading" variant="h3">
        You're all set!
      </Typography>
      <Typography className="subtitle" variant="h6">
        Password reset successful. Proceed to login?
      </Typography>
      <Button color="primary" className="button" onClick={redirectToLogin} variant="contained">
        Login
      </Button>
    </div>
  );
};

/**
 * Password Reset Form Component
 */
const PasswordResetForm = () => {
  let { token } = useParams<{ token: string }>();

  //console.log(token);

  let history = useHistory();
  const [success, setSuccess] = React.useState(false);

  /**
   * Determine if current token is valid, redirect if it is not
   */
  // TODO: Remove the following eslint comments when back-end is done
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const getAuthStatus = () => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const options: AxiosRequestConfig = {
      url: `${process.env.REACT_APP_API_ENDPOINT || ""}/api/v1/auth/status`,
      method: "get",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    };
  };

  /**
   * Request information based on token (ie. email)
   */
  React.useEffect(() => {
    // TODO: deal with tokens, remove comment when backend implemented **************
    // getAuthStatus();
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <FormCardLayout>
        <Container maxWidth="xs">
          <div className="password-reset">
            {success ? (
              <SuccessMessage redirectToLogin={() => history.push("/login")} />
            ) : (
              <>
                <Typography className="heading" variant="h3">
                  Reset Password
                </Typography>
                <div className="content">
                  <Typography className="subtitle" variant="h6">
                    Please enter a new password below.
                  </Typography>
                  <Formik
                    initialValues={{ pass: "", confirm: "" }}
                    onSubmit={(values, { setSubmitting }) => {
                      // send new password
                      const options: AxiosRequestConfig = {
                        url: `${process.env.REACT_APP_API_ENDPOINT || ""}/api/v1/auth/reset-password`,
                        method: "post",
                        params: {
                          token: token
                        },
                        data: {
                          password: values.pass
                        }
                      };

                      axios(options)
                        .then(() => {
                          // good response
                          setSuccess(true);
                        })
                        .catch((err) => {
                          console.log(err.message);
                          setSubmitting(false);
                        });
                    }}
                    validationSchema={Yup.object().shape({
                      pass: Yup.string()
                        .min(8, "Must be at least 8 characters")
                        .required("Password is required")
                        .matches(/^(?=.*[a-zA-Z])/, "Must contains at least one letter")
                        .matches(/^(?=.*\d)/, "Must contains at least one number"),
                      confirm: Yup.string()
                        .equals([Yup.ref("pass")], "Passwords do not match")
                        .required("Re-enter your password")
                    })}
                  >
                    {(props) => {
                      const { values, handleChange, touched, errors, handleBlur, handleSubmit, isSubmitting } = props;
                      return (
                        <form onSubmit={handleSubmit}>
                          <PasswordField
                            error={!!(errors.pass && touched.pass)}
                            helperText={errors.pass && touched.pass && errors.pass}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            name="pass"
                            value={values.pass}
                            disabled={isSubmitting}
                            placeholder="Password"
                          />
                          <PasswordField
                            error={!!(errors.confirm && touched.confirm)}
                            helperText={errors.confirm && touched.confirm && errors.confirm}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            name="confirm"
                            value={values.confirm}
                            disabled={isSubmitting}
                            placeholder="Verify Password"
                          />
                          <div className="buttons">
                            <Button
                              variant="contained"
                              color="primary"
                              className="button"
                              disabled={
                                !!(!(touched.pass && touched.confirm) || errors.pass || errors.confirm) || isSubmitting
                              }
                              type="submit"
                            >
                              Reset
                            </Button>
                            <Button
                              variant="outlined"
                              color="primary"
                              className="button"
                              onClick={() => history.push("/login")}
                              disabled={isSubmitting}
                            >
                              Cancel
                            </Button>
                          </div>
                        </form>
                      );
                    }}
                  </Formik>
                </div>
              </>
            )}
          </div>
        </Container>
      </FormCardLayout>
    </ThemeProvider>
  );
};

export default PasswordResetForm;
