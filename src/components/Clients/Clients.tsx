import React from "react";
import { Breadcrumbs, Paper, Chip, Grid } from "@material-ui/core";
import "./Clients.scss";
import ClientList from "./ClientList/ClientList";
import TableFilters from "../common/Filters/TableFilters";
import { FilterCheckboxSelectionType, FilterSections, LoanOfficerType } from "../common/CommonTypes";
import CancelIcon from "@material-ui/icons/Cancel";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import { apiOfficers } from "../../services/api/officers/apiOfficers";
import { Application } from "../ApplicationsLayout/ApplicationsLayout";
import { apiClients } from "../../services/api/clients/apiClients";
import { TitleTypography } from "../HomePage/AdminHomePage/AdminHomePage";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import ClientDetails from "./ClientDetails/ClientDetails";

type FilterChipType = {
  type: FilterSections;
  title: string;
};

type Client = {
  application: Application;
  loanAmount: number;
  dateSigned: string;
  currentContract: boolean;
  status: string;
  officer: string;
  user: string;
  createdAt: string;
  updatedAt: string;
  id: string;
};

type OfficerStateType = {
  name: string;
  selected: boolean;
  id: string;
};

const MyClients = () => {
  const accessToken = localStorage.getItem("accessToken");
  const [selectedFilters, setSelectedFilters] = React.useState<Array<FilterChipType>>([]);
  const { path } = useRouteMatch();

  // TODO This state will be used to filter the list
  const [statusSelections, setStatusSelections] = React.useState<Array<FilterCheckboxSelectionType>>([
    { title: "Active Client", checked: false },
    { title: "Updating Client Contract", checked: false },
    { title: "Archived", checked: false }
  ]);

  function getClients(page, limit): Promise<any> {
    const clients = apiClients.getAll({
      applications: {
        page,
        limit,
        populate: "application,currentContract"
      }
    });

    return clients;
  }

  const [searchValue, setSearchValue] = React.useState("");
  const [officers, setOfficers] = React.useState([]);
  const [data, setData] = React.useState<Client[] | undefined>([]);
  const [totalRows, setTotalRows] = React.useState(0);
  const [page, setPage] = React.useState(1);
  const perPage = 10;

  // TODO This state will be used to filter the list
  // TODO This will be changed based on the database (call API for list of officers)
  // Maybe we can add ID to each Loan Officer so that we can avoid loan officer with the same name when we filter
  const [assignedToSelections, setAssignedToSelections] = React.useState<Array<LoanOfficerType>>([]);

  const [startDate, setStartDate] = React.useState<MaterialUiPickersDate | null>(null);
  const [endDate, setEndDate] = React.useState<MaterialUiPickersDate | null>(null);
  const [rangeSpecified, setRangeSpecified] = React.useState<MaterialUiPickersDate[]>([]);

  const handleRemoveChip = (title: string) => {
    setSelectedFilters(selectedFilters.filter((filter) => filter.title !== title));
  };

  const handleAddChip = (title: string, type: FilterSections) => {
    setSelectedFilters([...selectedFilters, { type, title }]);
  };

  const handleRemoveSortChip = () => {
    setSelectedFilters(selectedFilters.filter((filter) => filter.type !== FilterSections.SORT_BY));
  };

  const handleReplaceChipTitle = (oldTitle: string, newTitle: string) => {
    setSelectedFilters(
      selectedFilters.map((filter) => (filter.title !== oldTitle ? filter : { ...filter, title: newTitle }))
    );
  };

  /**
   * Handle input of start date
   * @param date value of Date from input
   */
  const handleStartDate = (date: MaterialUiPickersDate) => {
    setStartDate(date);
    let newFilters = selectedFilters.filter((filter) => filter.type !== FilterSections.AFTER);
    if (date) newFilters.push({ type: FilterSections.AFTER, title: date.format("MM/DD/yyyy") });
    setSelectedFilters(newFilters);
    if (date == null) {
      setRangeSpecified([startDate, endDate]);
    }
  };

  /**
   * Handle input of end date
   * @param date value of Date from input
   */
  const handleEndDate = (date: MaterialUiPickersDate) => {
    setEndDate(date);
    let newFilters = selectedFilters.filter((filter) => filter.type !== FilterSections.BEFORE);
    if (date) newFilters.push({ type: FilterSections.BEFORE, title: date.format("MM/DD/yyyy") });
    setSelectedFilters(newFilters);
    if (date == null) {
      setRangeSpecified([startDate, endDate]);
    }
  };

  /**
   * Handle apply button click
   */
  const handleApplyDate = () => {
    let newFilters = selectedFilters.filter(
      (filter) => filter.type !== FilterSections.BEFORE && filter.type !== FilterSections.AFTER
    );
    if (startDate) newFilters.push({ type: FilterSections.AFTER, title: startDate.format("MM/DD/yyyy") });
    if (endDate) newFilters.push({ type: FilterSections.BEFORE, title: endDate.format("MM/DD/yyyy") });
    setSelectedFilters(newFilters);
    setRangeSpecified([startDate, endDate]); //re-trigger API
  };

  /**
   * Handle toggle a check box of the Status section
   * @param title
   */
  const handleToggleStatus = (title: string) => {
    setStatusSelections(
      statusSelections.map((selection) => {
        if (selection.title !== title) return selection;
        if (!selection.checked) {
          handleAddChip(title, FilterSections.STATUS);
        } else {
          handleRemoveChip(selection.title);
        }
        return { ...selection, checked: !selection.checked };
      })
    );
    setPage(1);
  };

  /**
   * Handle toggle selected Loan Officers
   * @param name
   */
  const handleToggleChooseAssigned = (name: string) => {
    setAssignedToSelections(
      assignedToSelections.map((selection) => {
        if (selection.name !== name) return selection;
        if (!selection.selected) {
          handleAddChip(name, FilterSections.ASSIGNED_TO);
        } else {
          handleRemoveChip(selection.name);
        }
        return { ...selection, selected: !selection.selected };
      })
    );
    setPage(1);
  };

  const renderChips = () => {
    return selectedFilters.map((filter, index) => {
      let deleteFunction: (title: string) => void;
      switch (filter.type) {
        case FilterSections.STATUS:
          deleteFunction = handleToggleStatus;
          break;
        case FilterSections.ASSIGNED_TO:
          deleteFunction = handleToggleChooseAssigned;
          break;
        case FilterSections.BEFORE:
          deleteFunction = () => handleEndDate(null);
          break;
        case FilterSections.AFTER:
          deleteFunction = () => handleStartDate(null);
          break;
        case FilterSections.SORT_BY:
          deleteFunction = handleRemoveSortChip;
          break;
        default:
          break;
      }
      return (
        <Grid key={index} item>
          <Chip
            label={`${filter.type}: ${filter.title}`}
            onDelete={() => deleteFunction(filter.title)}
            deleteIcon={<CancelIcon style={{ color: "rgba(0, 0, 0, 0.6)" }} />}
          />
        </Grid>
      );
    });
  };

  /* Translate the applied status filters to format required by API */
  const getStatusFilterParam = (): string[] => {
    const statuses = statusSelections.filter((status) => {
      return status.checked;
    });

    const statusFilters: any[] = [];

    statuses.forEach((status) => {
      statusFilters.push(status.title);
    });
    return statusFilters;
  };

  /* Translate the applied assigned to filters to format required by API */
  const getAssignedToFilterParam = (): string[] => {
    const assignees = assignedToSelections.filter((officer) => {
      return officer.selected;
    });

    const assigneeFilters: any[] = [];

    assignees.forEach((officer) => {
      assigneeFilters.push(officer.id);
    });

    return assigneeFilters;
  };

  const fetchOfficers = React.useCallback(() => {
    apiOfficers
      .getAll({
        officers: {
          sortBy: "workload",
          populate: "user"
        }
      })
      .then((data) => {
        setOfficers(data.results);

        const officerState: any[] = [];

        data.results.map((officer) => {
          officerState.push({
            name: officer.user.firstName,
            selected: false,
            id: officer.id
          });
        });

        setAssignedToSelections(officerState);
      })
      .catch((err) => err);
  }, []);

  /* Initial render */
  React.useEffect(() => {
    fetchOfficers();
  }, []);

  /* When filter parameters change */
  React.useEffect(() => {
    apiClients
      .getAll({
        clients: {
          ...(startDate && { startDate: startDate.toString() }),
          ...(endDate && { endDate: endDate.toString() }),
          ...(searchValue && { nameOrEmail: searchValue }),
          status: getStatusFilterParam(),
          ...(getAssignedToFilterParam().length > 0 && { officer: getAssignedToFilterParam() }),
          page: page,
          limit: perPage,
          populate: "application,currentContract"
        }
      })
      .then((res) => {
        if (res.totalResults > 0) {
          setData(res.results);
          setTotalRows(res.totalResults);
        } else {
          setData(undefined);
          setTotalRows(0);
        }
      })
      .catch((err) => {
        console.log(err);
        if (err.code === 404) {
          setData(undefined);
          setTotalRows(0);
        }
        console.log(err);
      });
  }, [statusSelections, assignedToSelections, searchValue, rangeSpecified, page]);

  const onPaginate = (newPage) => {
    setPage(newPage);
  };

  return (
    <div className="my-clients">
      <React.Fragment>
        <Switch>
          <Route exact path={path}>
            <Breadcrumbs>
              <TitleTypography variant="h4" color="textPrimary">
                Clients
              </TitleTypography>
            </Breadcrumbs>
            <Paper className="main-content" elevation={0} square>
              <TableFilters
                statusSelections={statusSelections}
                handleToggleStatus={handleToggleStatus}
                assignedToSelections={assignedToSelections}
                handleToggleChooseAssigned={handleToggleChooseAssigned}
                startDate={startDate}
                endDate={endDate}
                setStartDate={setStartDate}
                setEndDate={setEndDate}
                handleApply={handleApplyDate}
                sorting={false}
                setSearchValue={setSearchValue}
                setPage={setPage}
                disableFuture={false}
              />
              <Grid container spacing={2} className="chips-field">
                {renderChips()}
              </Grid>
              <ClientList
                accessToken={accessToken}
                officers={officers}
                setData={setData}
                data={data}
                fetchOfficers={fetchOfficers}
                page={page}
                onPaginate={onPaginate}
                totalRows={totalRows}
                getClients={getClients}
                perPage={perPage}
              />
            </Paper>
          </Route>
          <Route path={path + "/:id"}>
            <ClientDetails />
          </Route>
        </Switch>
      </React.Fragment>
    </div>
  );
};

export default MyClients;
