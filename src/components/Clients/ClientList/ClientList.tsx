import React from "react";
import LoanOfficerDropdown from "../../common/LoanOfficerDropdown/LoanOfficerDropdown";
import MyTable, { ColumnType } from "../../common/Table/Table";
import moment from "moment";
import "./ClientList.scss";
import jwtDecode from "jwt-decode";
import { useRouteMatch, Link as RouterLink } from "react-router-dom";
import { apiClients } from "../../../services/api/clients/apiClients";

export type ClientListProps = {
  accessToken: string | null;
  officers: any;
  setData: (data: any) => void;
  fetchOfficers: () => void;
  totalRows: number;
  onPaginate: (newPage: any) => void;
  page: number;
  perPage: number;
  getClients: (page: number, perPage: number) => Promise<any>;
  data: any;
};

const ClientList: React.FC<ClientListProps> = ({
  accessToken,
  officers,
  setData,
  fetchOfficers,
  page,
  onPaginate,
  totalRows,
  perPage,
  getClients,
  data
}) => {
  const user = accessToken && jwtDecode<any>(accessToken).sub;
  const { path } = useRouteMatch();

  const columns: ColumnType<any>[] = [
    {
      title: "Name",
      render: (record) => (
        <RouterLink to={path + "/" + record.id} className="link">
          {record.application.firstName + " " + record.application.lastName}
        </RouterLink>
      )
    },
    {
      title: "Maturity Date",
      render: (record) =>
        record.currentContract ? moment(record.currentContract.finalPaymentDue).utc().format("MMM D, YYYY") : "No Contract"
    },
    {
      title: "Status",
      render: (record) => record.status
    },
    {
      title: "Assigned To",
      render: (record, index) => (
        <LoanOfficerDropdown
          officers={officers}
          value={record.officer}
          currentUser={user}
          onChange={(e) => {
            apiClients
              .patch(record.id, {
                clients: {
                  officer: e
                }
              })
              .then((res) => {
                if (res.status === 200) {
                  getClients(page, perPage).then((res) => {
                    setData(res.results);
                  });
                  fetchOfficers();
                }
              })
              .catch((err) => {
                console.log(err.message);
              });
          }}
        />
      )
    }
  ];

  return (
    <MyTable
      data={data}
      columns={columns}
      pagination={{
        perPage,
        onPaginate,
        totalRows,
        page: page
      }}
    />
  );
};

export default ClientList;
