import React, { useState, useEffect, useCallback } from "react";
import axios, { AxiosRequestConfig } from "axios";
import { Grid, Card, CardHeader, CardContent, Typography } from "@material-ui/core";
import { apiContracts } from "../../../../services/api/contracts/apiContracts";
import PaymentProgress from "./PaymentProcess/PaymentProcess";
import "./LoanSummary.scss";

type Wave = {
  invoice: string;
  invoiceUrl: string;
  totalAmountPaid: number;
  totalAmountRemaining: number;
  nextPaymentAmount: number;
  expectedTotalAmountPaid: number;
  nextPaymentDueDate: string;
  credit: number;
};

type Contract = {
  id: string;
  status: string;
  contractFileName: string;
  guarantorName: string;
  approvedLoanAmount: number;
  firstPaymentDue: string;
  finalPaymentDue: string;
  contractStartDate: string;
  wave: Wave;
  client: string;
  application: string;
  monthlyPayment: string;
  finalPayment: string;
  createdAt: string;
  updatedAt: string;
  contractCycleLength: number;
};

interface LoanSummaryProps {
  clientId: string | undefined;
}

const defaultContract = {
  wave: {
    totalAmountPaid: 0,
    credit: 0,
    totalAmountRemaining: 0,
    nextPaymentAmount: 0,
    expectedTotalAmountPaid: 0,
    nextPaymentDueDate: "1970-01-01T00:00:00.001Z"
  },
  finalPayment: 0,
  approvedLoanAmount: 0,
  firstPaymentDue: "1970-01-01T00:00:00.001Z",
  finalPaymentDue: "1970-01-01T00:00:00.001Z",
  monthlyPayment: 0,
  status: "Active",
  contractStartDate: "1970-01-01T00:00:00.001Z",
  application: "",
  createdBy: "",
  contractFileName: "",
  client: "",
  createdAt: "1970-01-01T00:00:00.001Z",
  updatedAt: "1970-01-01T00:00:00.001Z",
  contractCycleLength: 1,
  id: ""
};

function getContractInfo(accessToken, id): Promise<any> {
  const contractRoute = `${process.env.REACT_APP_API_ENDPOINT || ""}/api/v1/contracts?client=${id}&status=Active`;

  const contractOptions: AxiosRequestConfig = {
    url: contractRoute,
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`
    }
  };

  return axios(contractOptions).then((res) => res.data);
}

const LoanSummary: React.FC<LoanSummaryProps> = ({ clientId }) => {
  const accessToken = localStorage.getItem("accessToken") || "";
  const [contractData, setContractData] = useState<Contract>();

  const overdueDays = () => {
    const dateDueStr =
      contractData?.finalPaymentDue.substring(5, 7) +
      "/" +
      contractData?.finalPaymentDue.substring(8, 10) +
      "/" +
      contractData?.finalPaymentDue.substring(0, 4);

    const todaysDate = new Date();
    const dateDue = new Date(dateDueStr);

    var difference = todaysDate.getTime() - dateDue.getTime();
    var days = Math.ceil(difference / (1000 * 3600 * 24));

    if (contractData?.wave?.totalAmountRemaining && days > 0) {
      return days;
    } else {
      return 0;
    }
  };
  //currentMonth and first date day - adjust for edge cases

  const convertDateToString = (date) => {
    if (date !== undefined) {
      let monthMap = new Map();

      monthMap.set("01", "January");
      monthMap.set("02", "February");
      monthMap.set("03", "March");
      monthMap.set("04", "April");
      monthMap.set("05", "May");
      monthMap.set("06", "June");
      monthMap.set("07", "July");
      monthMap.set("08", "August");
      monthMap.set("09", "September");
      monthMap.set("10", "October");
      monthMap.set("11", "November");
      monthMap.set("12", "December");

      const extractedMonth = date.substring(5, 7);
      const extractedDay = date.substring(8, 10);
      const extractedYear = date.substring(0, 4);

      return monthMap.get(extractedMonth) + " " + extractedDay + ", " + extractedYear;
    }
    return "";
  };

  const calculateExpectedPaymentAmount = (contractData) => {
    let today = new Date();
    let startDate = new Date();
    let cycleCount = 0;

    //These checks are to avoid possible errors with undefined values
    if (contractData?.contractStartDate !== undefined) {
      startDate = new Date(contractData?.contractStartDate);
    }

    if (contractData?.contractCycleLength !== undefined) {
      cycleCount = Math.floor((today.getTime() - startDate.getTime()) / contractData?.contractCycleLength);
    }

    if (contractData?.monthlyPayment !== undefined) {
      return contractData?.monthlyPayment * cycleCount;
    }
  };

  useEffect(() => {
    getContractInfo(accessToken, clientId).then((response) => {
      let data;
      if (response.results) {
        data = response.results[0];
      } else {
        data = defaultContract;
      }
      setContractData(data);
    });
  }, [getContractInfo]);

  return (
    <Card className="loan-summary-card-container" style={{ width: "1065px" }}>
      <div id="loan-summary-header">
        <CardHeader title="Loan Summary" className="loan-summary-title" style={{ padding: "none" }} />
      </div>
      <hr id="loan-summary-line" />
      <CardContent>
        <Grid container direction="row" spacing={1} style={{ position: "relative" }}>
          <Grid item container direction="column" spacing={5} xs={6} style={{ width: "303px", flexBasis: "auto" }}>
            <Grid item>
              <Typography variant="body1">
                <b>Contract Date</b>
              </Typography>

              <Typography variant="body1">{convertDateToString(contractData?.contractStartDate)} </Typography>
            </Grid>

            <Grid item>
              <Typography variant="body1">
                <b>Maturity Date</b>
              </Typography>

              <Typography variant="body1">{convertDateToString(contractData?.finalPaymentDue)}</Typography>
            </Grid>

            <Grid item>
              <Typography variant="body1">
                <b>Next Pay Due Date</b>
              </Typography>

              <Typography variant="body1">
                {contractData?.wave.nextPaymentDueDate && convertDateToString(contractData?.wave.nextPaymentDueDate)}
              </Typography>
            </Grid>

            <Grid item>
              <Typography variant="body1">
                <b>Overdue</b>
              </Typography>
              <Typography variant="body1">{overdueDays()} Days</Typography>
              <Typography variant="body1">from the maturity date</Typography>
            </Grid>
          </Grid>
          <Grid item container direction="column" spacing={5} xs={12} style={{ width: "750px", flexBasis: "auto" }}>
            <Grid item container xs={12} direction="column">
              <Typography variant="body1">
                <b>Payment Progress Overview</b>
              </Typography>

              <Grid item xs={3} style={{ top: "200px", position: "relative" }}>
                {contractData && (
                  <PaymentProgress
                    approvedLoanAmount={contractData?.approvedLoanAmount}
                    expectedAmountToBePaid={calculateExpectedPaymentAmount(contractData)}
                    remaining={contractData?.approvedLoanAmount - contractData?.wave.totalAmountPaid}
                    paid={contractData?.wave.totalAmountPaid}
                  />
                )}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default LoanSummary;
