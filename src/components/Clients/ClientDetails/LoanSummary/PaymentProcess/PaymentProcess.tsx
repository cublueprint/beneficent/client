import React, { useState, useEffect, useCallback } from "react";
import axios, { AxiosRequestConfig } from "axios";
import {
  Avatar,
  LinearProgress,
  Card,
  CardHeader,
  CardContent,
  Paper,
  withStyles,
  Typography,
  Grid
} from "@material-ui/core";

import commaNumber from "comma-number";

import ProgressBar from "../../../../common/ProgressBar/ProgressBar";

import "./PaymentProcess.scss";

type PaymentProcess = {
  approvedLoanAmount: number | undefined;
  expectedAmountToBePaid: number | undefined;
  remaining: number | undefined;
  paid: number | undefined;
};

interface PaymentProcessProps {
  approvedLoanAmount: number | undefined;
  expectedAmountToBePaid: number | undefined;
  remaining: number | undefined;
  paid: number | undefined;
}

const StyledAvatar = withStyles(() => ({
  root: {
    height: "22px",
    width: "22px",
    fontSize: "14px",
    left: "18px",
    bottom: "2px"
  }
}))(Avatar);

const PaymentProcess: React.FC<PaymentProcessProps> = ({ approvedLoanAmount, expectedAmountToBePaid, remaining, paid }) => {
  const MAX = approvedLoanAmount!;
  const MIN = 0;

  const emptyString = "";

  const normalise = (value) => ((value - MIN) * 100) / (MAX - MIN);

  const paidColor = () => {
    if (paid! < expectedAmountToBePaid!) {
      return "#F7685B";
    } else {
      return "#70D19C";
    }
  };

  const StyledLinearProgress = withStyles(() => ({
    root: {
      backgroundColor: "#FFFFFF",
      padding: 3
    },
    bar: {
      backgroundColor: paidColor()
    }
  }))(LinearProgress);

  return (
    <Card className="payment-process-container" style={{ backgroundColor: "#F2F2F7" }}>
      <Grid container direction="row" spacing={1} style={{ position: "relative" }}>
        <Grid item container direction="column" spacing={5} xs={6} style={{ width: "800px", flexBasis: "auto" }}>
          <Grid item>
            <Typography variant="h4" className="approved-loan-amount-text">
              $ {commaNumber(approvedLoanAmount)}
            </Typography>
            <Typography variant="body1" className="approved-loan-amount-value" color="textSecondary">
              <b> Approved Loan Amount </b>
            </Typography>
          </Grid>
          <Grid
            item
            className="progress-bar"
            style={{ left: "-48px", width: "761px", justifyContent: "left", alignContent: "left" }}
          >
            <ProgressBar
              bgcolor={paidColor()}
              completed={normalise(paid)}
              secondCompleted={normalise(expectedAmountToBePaid)}
            />
          </Grid>
          <Grid item>
            <Grid container direction="row" style={{ width: "750px", position: "relative", left: "0px", bottom: "132px" }}>
              <Grid item xs={9}>
                <Typography variant="body1" className="expected-amount-to-be-paid-text">
                  Expected amount to be paid: ${commaNumber(expectedAmountToBePaid)}
                </Typography>
              </Grid>
              <Grid item xs={3}>
                <Grid container direction="column">
                  <Grid item>
                    <Typography variant="body1" className="remaining-value">
                      $ {commaNumber(remaining)}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant="body1" className="remaining-text">
                      Remaining
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid
                container
                direction="row"
                style={{ width: "150px", position: "relative", left: "-20px", bottom: "20px" }}
              >
                <Grid item xs={4}>
                  <StyledAvatar style={{ backgroundColor: paidColor(), left: "20px", bottom: "-5px" }}>
                    {emptyString}
                  </StyledAvatar>
                </Grid>
                <Grid item xs={8}>
                  <Grid container direction="column" style={{}}>
                    <Grid item>
                      <Typography variant="body1" className="paid-value">
                        $ {commaNumber(paid)}
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Typography variant="body1" className="paid-text">
                        Paid
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Card>
  );
};

export default PaymentProcess;
