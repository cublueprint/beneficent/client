import { Grid, Paper } from "@material-ui/core";
import * as React from "react";
import { useEffect, useState, useCallback } from "react";
import "./ClientDetails.scss";
import { Route, Switch, useParams } from "react-router-dom";
import routes from "../../../routes";
import { apiApplications } from "../../../services/api/applications/apiApplications";
import { apiClients } from "../../../services/api/clients/apiClients";
import ClientPayments from "./ClientPayments/ClientPayments";
import { apiOfficers } from "../../../services/api/officers/apiOfficers";
import { apiContracts } from "../../../services/api/contracts/apiContracts";
import { apiPayments } from "../../../services/api/payments/apiPayments";
import PersonalInformation from "../../ApplicationsLayout/ApplicationDetails/PersonalInformation/PersonalInformation";
import LoanSummary from "./LoanSummary/LoanSummary";

interface ContractInfo {
  id: string;
  status: string;
  contractFileName: string;
  dateCreated: string;
  dateUpdated: string;
  guarantorName: string;
  approvedLoanAmount: number;
  firstPaymentDue: string;
  finalPaymentDue: string;
  contractStartDate: string;
  client: string;
  application: string;
  monthlyPayment: number;
  finalPayment: string;
  wave: {
    totalAmountPaid: number;
    totalAmountRemaining: number;
    expectedTotalAmountPaid: number;
    nextPaymentAmount: string;
    nextPaymentDueDate: string;
    credit: number;
  };
  createdAt: string;
  updatedAt: string;
  contractCycleLength: number;
}

interface ClientInfo {
  id: string;
  dateSigned?: string;
  officer: string;
  wave: {
    customerId: string;
    invoiceId: string;
    invoiceUrl: string;
    productId: string;
  };
  application: string;
  currentContract?: string;
  status: string;
}
interface ApplicationInfo {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  address: string;
  city: string;
  province: string;
  sex: string;
  dateOfBirth: string;
  maritalStatus: string;
  citizenship: string;
  preferredLanguage: string;
  employmentStatus: string;
  loanAmountRequested: number;
  loanType: string;
  debtCircumstances: string;
  postalCode: string;
  guarantor: {
    hasGuarantor: boolean;
    fullName: string;
    email: string;
    phoneNumber: string;
  };
  acknowledgements: {
    loanPurpose: boolean;
    maxLoan: boolean;
    residence: boolean;
    netPositiveIncome: boolean;
    guarantorConsent: boolean;
    repayment: boolean;
  };
  recommendationInfo: string;
  emailOptIn: boolean;
  status: string;
  officer: string;
  user: string;
}

interface Payment {
  id: string;
  amountDue: number;
  amountPaid: number;
  dateDue: string;
  datePaid: string;
  client: string;
  createdAt: string;
  updatedAt: string;
}

interface getPaymentsResponse {
  results: Payment[];
  page: number;
  limit: number;
  totalPages: number;
  totalResults: number;
}

/**
 * Detail page of a client
 */
const ClientDetails = (props) => {
  const { id } = useParams<{ id: string }>();
  const token = window.localStorage.getItem("accessToken");
  const [contractInfo, setContractInfo] = useState<ContractInfo>();
  const [clientInfo, setClientInfo] = useState<ClientInfo>();
  const [applicationInfo, setApplicationInfo] = useState<ApplicationInfo>();
  const [payments, setPayments] = useState<Payment[]>([]);
  const [page, setPage] = useState(1);
  const [totalRows, setTotalRows] = useState(0);
  const perPage = 10;
  const [officers, setOfficers] = useState([]);

  //This could probably be refactored into useEffect
  /**
   * Fetch payment data
   */
  const fetchPayments = useCallback(async (): Promise<getPaymentsResponse | undefined> => {
    const response: getPaymentsResponse = await apiPayments.getAll({
      payments: { client: id, limit: perPage.toString(), page: page.toString() }
    });
    if (response instanceof Error) {
      console.error(response);
      return;
    }
    if (response.totalResults && response.results) {
      return response;
    }
  }, [token, perPage, page]);

  useEffect(() => {
    const onLoad = async () => {
      const response = await fetchPayments();
      if (response) {
        setTotalRows(response.totalResults);
        setPayments(response.results);
      }
    };
    onLoad();
  }, []);

  /**
   * fetches client object associated with id
   * fetches application object associated with client
   */
  useEffect(() => {
    apiClients
      .getSingle(id)
      .then((res) => {
        setClientInfo(res);
        fetchApplication(res.application);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, [id, token]);

  /**
   * fetches officers
   */
  useEffect(() => {
    apiOfficers
      .getAll({
        officers: {
          sortBy: "workload",
          populate: "user"
        }
      })
      .then((data) => {
        setOfficers(data.results);
      })
      .catch((err) => err);
  }, [token]);

  /**
   * fetches application object associated with id
   * @param id application id
   */
  const fetchApplication = useCallback(
    (id) => {
      apiApplications
        .getSingle(id)
        .then((res) => {
          setApplicationInfo(res);
        })
        .catch((err) => {
          console.log(err.message);
        });
    },
    [applicationInfo]
  );

  return (
    <Grid className="application-details-container" spacing={3} container data-testid="skeleton-container">
      <Switch>
        <Route exact path={routes.myClients.path + "/:id"}>
          <Grid spacing={2} item container direction="row">
            <Grid item xs={12}>
              <PersonalInformation
                setAppInfo={(info) => setApplicationInfo(info)}
                officers={officers}
                appInfo={applicationInfo!}
              ></PersonalInformation>
            </Grid>
            <Grid item xs={12}>
              <LoanSummary clientId={id} />
            </Grid>
            <Grid item xs={12}>
              {clientInfo && (
                <ClientPayments invoiceUrl={clientInfo?.wave.invoiceUrl} payments={payments} totalRows={totalRows} />
              )}
            </Grid>
          </Grid>
        </Route>
      </Switch>
    </Grid>
  );
};

export default ClientDetails;
