import React, { useState, useEffect, useCallback } from "react";
import moment from "moment";
import { Button, Card, CardHeader } from "@material-ui/core";
import "./ClientPayments.scss";
import { apiPayments } from "../../../../services/api/payments/apiPayments";
import MyTable, { ColumnType } from "../../../common/Table/Table";

interface Payment {
  id: string;
  amountDue: number;
  amountPaid: number;
  dateDue: string;
  datePaid: string;
  client: string;
  createdAt: string;
  updatedAt: string;
}

interface getPaymentsResponse {
  results: Payment[];
  page: number;
  limit: number;
  totalPages: number;
  totalResults: number;
}

interface ClientPaymentsProps {
  invoiceUrl: string | undefined;
  payments: Payment[];
  totalRows: number;
}

const ClientPayments: React.FC<ClientPaymentsProps> = ({ invoiceUrl, payments, totalRows }) => {
  const [page, setPage] = useState(1);
  const perPage = 10;

  const onPaginate = (newPage) => {
    setPage(newPage);
  };

  const columns: ColumnType<any>[] = [
    {
      title: "Amount Due",
      render: (record) => `$${record.amountDue.toFixed(2)}`
    },
    {
      title: "Amount Paid",
      render: (record) => `$${record.amountPaid.toFixed(2)}`
    },
    {
      title: "Due Date",
      render: (record) =>
        moment(record.dateDue as string)
          .utc()
          .format("MMM D, YYYY")
    },
    {
      title: "Date Paid",
      render: (record) =>
        moment(record.datePaid as string)
          .utc()
          .format("MMM D, YYYY")
    }
  ];

  return (
    <Card className="payments-card-container" style={{ width: "1065px" }}>
      <div className="payments-card-header">
        <CardHeader title="Payment History" style={{ padding: "none" }} />
        <Button
          className="payments-card-view-button"
          variant="contained"
          color="primary"
          onClick={() => window.open(`${invoiceUrl}`, "_blank")}
        >
          View Invoice
        </Button>
      </div>

      <MyTable
        data={payments}
        columns={columns}
        pagination={{
          perPage,
          onPaginate,
          totalRows,
          page: page
        }}
      />
    </Card>
  );
};

export default ClientPayments;
