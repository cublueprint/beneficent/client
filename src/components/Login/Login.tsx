import * as React from "react";
import { useState, useEffect } from "react";
import "./Login.scss";
import FormCardLayout from "../common/FormCardLayout/FormCardLayout";
import ROUTES from "../../routes";

import { makeStyles, createStyles, MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { Typography, TextField, InputAdornment, Button, IconButton } from "@material-ui/core/";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import Link from "@material-ui/core/Link";
import { useHistory } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from "yup";

type Account = {
  email: string;
  password: string;
};

/**
 * Custom theme for the buttons
 */
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "rgba(0, 0, 0, 0.87);"
    },
    secondary: {
      main: "#DA6E5D"
    }
  },
  typography: {
    fontFamily: "Lato"
  }
});

/**
 * Overriding styles of some inner components
 */
const useStyles = makeStyles((theme: any) =>
  createStyles({
    /**
     * Override the font on TextFields and their error text
     */
    inputFont: {
      fontFamily: "Lato"
    },

    /**
     * Green color on successful validation
     */
    successfulValidation: {
      borderColor: "#2ED47A"
    }
  })
);

interface LoginPageProps {
  /**
   * An async function that submits the login form
   * to validate email and password to server.
   */
  handleSubmitLoginForm(account: Account): Promise<void>;
  validRefresh(): Promise<boolean>;
}

/**
 * Login form page component for the application.
 */
const Login = ({ handleSubmitLoginForm, validRefresh }: LoginPageProps) => {
  const classes = useStyles();
  const history = useHistory();
  /**
   * Represent the state of the "show password" button
   */
  const [showingPassword, setShowingPassword] = useState(false);

  useEffect(() => {
    const isAuthenticated = async (): Promise<void> => {
      if (await validRefresh()) {
        history.push(ROUTES.home.path);
      }
    };
    isAuthenticated();
  }, [history, validRefresh]);

  return (
    <MuiThemeProvider theme={theme}>
      <div className="login-form">
        <FormCardLayout>
          <Typography className="title" variant="h3">
            Login
          </Typography>

          <Formik
            initialValues={{
              email: "",
              password: ""
            }}
            onSubmit={async (
              values: Account,
              { setSubmitting, resetForm, setFieldValue, setFieldError, setFieldTouched }
            ) => {
              try {
                await handleSubmitLoginForm(values);
              } catch (err: any) {
                resetForm();
                setFieldValue("email", values.email, false);
                setFieldTouched("email", true, false);
                setFieldError("email", err.message);
                setSubmitting(false);
              }
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string().email("Please enter a valid email.").required("Email is required."),
              password: Yup.string().required("Password is required.")
            })}
          >
            {(props) => {
              const { values, touched, errors, isSubmitting, handleChange, handleBlur, handleSubmit } = props;

              return (
                <form data-testid="login-form" onSubmit={handleSubmit} className="form">
                  <TextField
                    name="email"
                    className="text-field"
                    variant="outlined"
                    placeholder="Email"
                    value={values.email}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    error={Boolean(errors.email && touched.email)}
                    helperText={touched.email ? errors.email : ""}
                    InputProps={{
                      classes: {
                        input: classes.inputFont,
                        notchedOutline:
                          !Boolean(errors.email && touched.email) && values.email ? classes.successfulValidation : ""
                      }
                    }}
                    FormHelperTextProps={{
                      classes: {
                        root: classes.inputFont
                      }
                    }}
                  />

                  <TextField
                    name="password"
                    className="text-field"
                    variant="outlined"
                    placeholder="Password"
                    type={showingPassword ? "text" : "password"}
                    value={values.password}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    error={Boolean(errors.password && touched.password)}
                    helperText={touched.password ? errors.password : ""}
                    InputProps={{
                      classes: {
                        input: classes.inputFont,
                        notchedOutline:
                          !Boolean(errors.password && touched.password) && values.password
                            ? classes.successfulValidation
                            : ""
                      },
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton onClick={() => setShowingPassword(!showingPassword)} edge="end">
                            {showingPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                    FormHelperTextProps={{
                      classes: {
                        root: classes.inputFont
                      }
                    }}
                  />
                  <div className="forgot-password">
                    <Typography className="text forgot-password-text">
                      {"Forgot password? "}
                      <Link href={ROUTES.requestResetPassword.path} color="secondary">
                        Reset.
                      </Link>
                    </Typography>
                    <Button
                      data-testid="login-button"
                      className="button"
                      color="primary"
                      variant={
                        !Boolean(errors.email || errors.password) && values.email && values.password
                          ? "contained"
                          : "outlined"
                      }
                      type="submit"
                      disabled={
                        isSubmitting ||
                        !Boolean(!Boolean(errors.email || errors.password) && values.email && values.password)
                      }
                    >
                      Login
                    </Button>
                  </div>
                </form>
              );
            }}
          </Formik>
        </FormCardLayout>
      </div>
    </MuiThemeProvider>
  );
};

export default Login;
