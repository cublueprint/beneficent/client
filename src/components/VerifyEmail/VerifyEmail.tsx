import { Button, Container, createMuiTheme, createStyles, makeStyles, ThemeProvider, Typography } from "@material-ui/core";
import axios, { AxiosRequestConfig } from "axios";
import * as React from "react";
import { useHistory, useLocation } from "react-router-dom";
import FormCardLayout from "../common/FormCardLayout/FormCardLayout";
import checkmark from "../../assets/blue_check_24px.png";
import "./VerifyEmail.scss";
import { useEffect, useState } from "react";
import jwtDecode from "jwt-decode";

/**
 * Custom MUI Theme to add custom blue colour
 */
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "rgba(0, 0, 0, 0.87);"
    }
  }
});

/**
 * Add success style from LoginForm
 */
const useStyles = makeStyles(() =>
  createStyles({
    success: {
      borderColor: (props: any) => (!props.error && props.value ? "#2ED47A" : undefined)
    },
    absolute: {
      position: "absolute",
      top: "56px"
    }
  })
);

/**
 * Success Message Props
 * @property On click event for go to login button
 */
type MessageProps = {
  didVerify: Boolean;
  isLoading: Boolean;
  redirectToLogin: (event: React.MouseEvent<HTMLButtonElement>) => void;
};

/**
 * Message displayed when email verification is successful or has failed
 * @param param - Object with redirectedToLogin property: function called when go to login button is clicked
 */
const Message: React.FC<MessageProps> = ({ didVerify, isLoading, redirectToLogin }) => {
  return (
    <div className="success-message">
      {isLoading && !didVerify ? (
        <div className="loading">
          <Typography className="heading" variant="h3">
            Verifying Account...
          </Typography>
        </div>
      ) : null}

      {didVerify ? (
        <>
          <div className="centered">
            <img alt="check-mark" width="100px" className="check" src={checkmark} />
          </div>
          <Typography className="heading" variant="h3">
            You're all set!
          </Typography>
          <Typography className="subtitle" variant="h6">
            Email verification successful.
          </Typography>
          <Typography className="subtitle" variant="h6">
            If this is a new account, you will receive an email to set your password before being able to login.
          </Typography>
          <Button color="primary" className="button" onClick={redirectToLogin} variant="contained">
            Login
          </Button>
        </>
      ) : !isLoading ? (
        <>
          <Typography className="heading" variant="h3">
            Email verification failed.
          </Typography>
          <Typography className="subtitle" variant="h6">
            Email could not be verified. Please ask an admin to re-send the verification link.
          </Typography>
        </>
      ) : null}
    </div>
  );
};

/**
 * Verify Email Component
 */
const VerifyEmail = () => {
  const { search } = useLocation();
  const query = new URLSearchParams(search);
  const token = query.get("token");

  let history = useHistory();
  const [success, setSuccess] = useState(false);
  const [loading, setLoading] = useState(true);

  /**
   * Verify email token and set success or failure accordingly
   */
  useEffect(() => {
    const options: AxiosRequestConfig = {
      url: `${process.env.REACT_APP_API_ENDPOINT || ""}/api/v1/auth/verify-email?token=${token}`,
      method: "post",
      headers: {
        "Content-Type": "application/json"
      }
    };

    axios(options)
      .then((res) => {
        // good response
        setLoading(false);
        setSuccess(true);
      })
      .catch((err) => {
        console.log(err.message);
        setLoading(false);
        setSuccess(false);
      });
  }, [token]);

  return (
    <ThemeProvider theme={theme}>
      <FormCardLayout>
        <Container maxWidth="xs">
          <div className="verify-email">
            <Message didVerify={success} isLoading={loading} redirectToLogin={() => history.push("/login")} />
          </div>
        </Container>
      </FormCardLayout>
    </ThemeProvider>
  );
};

export default VerifyEmail;
