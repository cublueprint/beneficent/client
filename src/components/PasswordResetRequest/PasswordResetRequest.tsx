import * as React from "react";
import { useState } from "react";
import "./PasswordResetRequest.scss";
import axios from "axios";
import check_box from "../../assets/blue_check_24px.png";
import ROUTES from "../../routes";

import { makeStyles, createStyles, MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Formik } from "formik";
import * as Yup from "yup";
import FormCardLayout from "../common/FormCardLayout/FormCardLayout";
import { useHistory } from "react-router-dom";

/**
 * Overriding styles of some inner components
 */
const useStyles = makeStyles((theme: any) =>
  createStyles({
    /**
     * Override the font on TextFields and their error text
     */
    inputFont: {
      fontFamily: "Lato"
    },

    /**
     * Green color on successful validation
     */
    successfulValidation: {
      borderColor: "#2ED47A"
    }
  })
);

/**
 * Custom theme for the buttons
 */
const buttonTheme = createMuiTheme({
  palette: {
    primary: {
      main: "rgba(0, 0, 0, 0.87);"
    }
  },
  typography: {
    fontFamily: "Lato"
  }
});

/**
 * User give his/her email and we send back an email to change password
 */
const PasswordResetRequest = () => {
  const classes = useStyles();
  const history = useHistory();

  /**
   * Show that if the request was sent successfully
   */
  const [sentSuccessfully, setSentSuccessfully] = useState(false);

  /**
   * Validate the email to the server to send a reset password link
   * @param account a string represent email of the user
   */
  const handleSubmitResetRequest = async (email: String) => {
    try {
      const url = `${process.env.REACT_APP_API_ENDPOINT || ""}/api/v1/auth/forgot-password`;
      await axios.post(url, {
        email
      });
      // console.log(response);
      setSentSuccessfully(true);
    } catch (err: any) {
      throw new Error("The email given does not exist. Please try again.");
    }
  };

  return (
    <div className="reset-request">
      <FormCardLayout>
        <Typography className="title" variant="h3">
          Send Password Reset
        </Typography>

        {sentSuccessfully ? null : (
          <Typography className="text" variant="h6">
            Enter your email below and we will help recover your account.
          </Typography>
        )}

        <Formik
          initialValues={{
            email: ""
          }}
          onSubmit={async (values, { setSubmitting, resetForm, setFieldValue, setFieldError, setFieldTouched }) => {
            setSubmitting(true);
            try {
              await handleSubmitResetRequest(values.email);
            } catch (err: any) {
              resetForm();
              setFieldValue("email", values.email, false);
              setFieldTouched("email", true, false);
              setFieldError("email", err.message);
              setSubmitting(false);
            }
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string().email("Please enter a valid email.").required("Email is required.")
          })}
        >
          {(props) => {
            const { values, touched, errors, isSubmitting, handleChange, handleBlur, handleSubmit } = props;

            return sentSuccessfully ? (
              <>
                <Typography className="sent-text" variant="h6">
                  A password reset request email was sent to:
                </Typography>
                <Typography className="sent-email" variant="h6">
                  <b>{values.email}</b>
                </Typography>
                <img className="check" src={check_box} alt="Check box" />
                <MuiThemeProvider theme={buttonTheme}>
                  <Button
                    color="primary"
                    variant="contained"
                    className="sent-button"
                    onClick={() => setSentSuccessfully(false)}
                  >
                    <Typography className="button" variant="button">
                      Retry
                    </Typography>
                  </Button>
                </MuiThemeProvider>
              </>
            ) : (
              <MuiThemeProvider theme={buttonTheme}>
                <form data-testid="reset-request-form" onSubmit={handleSubmit} className="form">
                  <TextField
                    name="email"
                    className="email-request-field"
                    variant="outlined"
                    placeholder="Email"
                    value={values.email}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    error={Boolean(errors.email && touched.email)}
                    helperText={errors.email}
                    InputProps={{
                      classes: {
                        input: classes.inputFont,
                        notchedOutline:
                          !Boolean(errors.email && touched.email) && values.email ? classes.successfulValidation : ""
                      }
                    }}
                    FormHelperTextProps={{
                      classes: {
                        root: classes.inputFont
                      }
                    }}
                    disabled={sentSuccessfully}
                  />

                  <div className="button-group">
                    <Button
                      data-testid="submit-button"
                      className="button"
                      color="primary"
                      variant={values.email && !errors.email ? "contained" : "outlined"}
                      type="submit"
                      disabled={isSubmitting || !Boolean(values.email) || Boolean(errors.email)}
                    >
                      Send
                    </Button>

                    <Button
                      className="button"
                      color="primary"
                      variant="outlined"
                      onClick={() => history.push(ROUTES.login.path)}
                    >
                      Cancel
                    </Button>
                  </div>
                </form>
              </MuiThemeProvider>
            );
          }}
        </Formik>
      </FormCardLayout>
    </div>
  );
};

export default PasswordResetRequest;
