import {
  Button,
  createMuiTheme,
  Divider,
  Chip,
  Grid,
  ThemeProvider,
  withStyles,
  FormControl,
  Select,
  MenuItem,
  InputBase,
  Paper
} from "@material-ui/core";
import ArrowBack from "@material-ui/icons/ArrowBack";
import CancelIcon from "@material-ui/icons/Cancel";
import * as React from "react";
// eslint-disable-next-line import/named
import { Link as RouterLink, Route, RouteComponentProps, Switch, useRouteMatch, withRouter } from "react-router-dom";
import routes from "../../routes";
import { TitleTypography } from "../HomePage/AdminHomePage/AdminHomePage";
import "./ApplicationsLayout.scss";
import { Add, ExpandLess, ExpandMore } from "@material-ui/icons";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import MyTable, { ColumnType } from "../common/Table/Table";
import LoanOfficerDropdown from "../common/LoanOfficerDropdown/LoanOfficerDropdown";
import moment from "moment";
import { apiOfficers } from "../../services/api/officers/apiOfficers";
import ApplicationDetails from "./ApplicationDetails/ApplicationDetails";
import Dialogue from "../common/Dialogue/Dialogue";
import {
  FilterCheckboxSelectionType,
  FilterChooseSelectionType,
  FilterSections,
  LoanOfficerType
} from "../common/CommonTypes";
import TableFilters from "../common/Filters/TableFilters";
import jwtDecode from "jwt-decode";
import CreateApplication from "./CreateApplication/CreateApplication";
import { apiApplications } from "../../services/api/applications/apiApplications";

export type Application = {
  guarantor: {
    hasGuarantor: boolean;
    fullName: string;
    email: string;
    phoneNumber: string;
  };
  acknowledgements: {
    loanPurpose: boolean;
    maxLoan: boolean;
    residence: boolean;
    netPositiveIncome: boolean;
    guarantorConsent: boolean;
    repayment: boolean;
  };
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  address: string;
  city: string;
  province: string;
  sex: string;
  dateOfBirth: string;
  maritalStatus: string;
  citizenship: string;
  preferredLanguage: string;
  employmentStatus: string;
  loanAmountRequested: number;
  loanType: string;
  debtCircumstances: string;
  recommendationInfo: string;
  emailOptIn: boolean;
  status: string;
  officer: string;
  user: string;
  dateApplied: string;
  createdAt: string;
  updatedAt: string;
  id: string;
};

export interface FormikSubmitObject {
  handleSubmit: () => void;
}

/**
 * Map path to breadcrumb name
 */
export const breadcrumbMap = {
  [routes.casesApplications.path]: "Applications",
  [routes.create.path]: "Create",
  [routes.detail.path]: "Details"
};

type FilterChipType = {
  type: FilterSections;
  title: string;
};

type OfficerStateType = {
  name: string;
  selected: boolean;
  id: string;
};

interface BackButtonProps {
  /**
   * Array of path names of current path
   */
  pathnames: Array<string>;
}

/**
 * Customized Next Nav icon for breadcrumbs
 */

const BackButton = ({ pathnames }: BackButtonProps) => {
  if (pathnames[pathnames.length - 2] === "/create-contract" || pathnames[pathnames.length - 2] === "/edit-contract") {
    // to account for last /:contractId in the route
    pathnames.length -= 2;
  } else {
    pathnames.length -= 1;
  }
  let url = pathnames.join("");

  return (
    <RouterLink
      className="back-parent"
      to={"" + url}
      onClick={() => {
        dispatchEvent(new PopStateEvent("popstate")); // force popstate event to force partial reload
      }}
    >
      <TitleTypography className="back-button" variant="h6" color="textPrimary">
        <ArrowBack style={{ marginRight: "12px" }} />
        Back
      </TitleTypography>
    </RouterLink>
  );
};

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#DA6E5D",
      contrastText: "#000"
    }
  },
  typography: {
    allVariants: {
      fontFamily: "Lato"
    }
  }
});

/**
 * Customized Drop Down Input for Status field
 */
const StatusInput = withStyles(() => ({
  input: {
    width: 127,
    height: 16,
    border: "1px solid rgb(0, 0, 0, 0.12)",
    borderRadius: 4,
    fontSize: 16,
    color: "black",
    paddingTop: 6,
    paddingLeft: 22,
    paddingBottom: 10
  }
}))(InputBase);

/**
 * Renders Application Status dropdown to update the status of an
 *  application
 */
function StatusDropdown(props) {
  const { officer, onChange } = props;
  const [status, setStatus] = React.useState(props.value);
  const [warning, setWarning] = React.useState(officer == undefined ? true : false);

  const handleChangeStatus = (event) => {
    if (officer == undefined) {
      setWarning(true);
      setStatus(status);
    } else {
      setStatus(event.target.value);
    }
    // TODO: update on backend database
  };

  const renderItem = (appStatus) => (
    <MenuItem value={appStatus} key={appStatus} onClick={() => onChange(appStatus)}>
      <p className="status-menu-item">{appStatus}</p>
    </MenuItem>
  );

  const applicationStatus = [
    "Received",
    "Interview",
    "Accepted",
    "Rejected",
    "Contract Sent",
    "Contract Signed",
    "Active Client",
    "Archived",
    "Contacted",
    "Requested Information"
  ];
  return (
    <FormControl variant="outlined">
      <Select
        labelId="status-select-label"
        id="status-select"
        value={status}
        onChange={handleChangeStatus}
        input={<StatusInput />}
        inputProps={{
          classes: {
            icon: "dropdown-icon"
          }
        }}
        IconComponent={ExpandMore}
      >
        {warning ? (
          <Dialogue
            title={"Warning"}
            text={"You cannot change this status until a loan officer is assigned. Please assign a loan officer."}
            open={warning}
          />
        ) : null}
        <div className="status-dropdown">
          <p>Status</p>
          <ExpandLess className="icon" />
        </div>
        <Divider />
        {applicationStatus.map((appStatus) => renderItem(appStatus))}
      </Select>
    </FormControl>
  );
}

/**
 * Method to retrieve all applications from database
 */
async function getApplications(page, limit, sortBy: string | undefined = undefined): Promise<any> {
  const apps = await apiApplications.getAll({
    applications: {
      limit,
      page,
      sortBy
    }
  });
  return apps;
}

/**
 * HOC Layout Component for cases and applications
 * @param props RouteComponentProps given by withRouter
 */
const ApplicationsLayout: React.FC<RouteComponentProps> = ({ location }) => {
  const pathnames = location.pathname.split(/(?=\/)/g);
  const { path } = useRouteMatch();
  const accessToken = localStorage.getItem("accessToken");
  const createContractSubmissionRef = React.useRef<FormikSubmitObject>(null);
  // TODO Make this an array of selected chips, so when we select one,
  // add a chip with that information into this array
  const [selectedFilters, setSelectedFilters] = React.useState<Array<FilterChipType>>([]);
  const [rangeSpecified, setRangeSpecified] = React.useState<MaterialUiPickersDate[]>([]);

  // TODO This state will be used to filter the list
  const [statusSelections, setStatusSelections] = React.useState<Array<FilterCheckboxSelectionType>>([
    { title: "Received", checked: false },
    { title: "Interview", checked: false },
    { title: "Accepted", checked: false },
    { title: "Rejected", checked: false },
    { title: "Contract Sent", checked: false },
    { title: "Contract Signed", checked: false },
    { title: "Active Client", checked: false },
    { title: "Archived", checked: false }
  ]);

  // TODO This state will be used to filter the list
  const [sortBySelections, setSortBySelections] = React.useState<FilterChooseSelectionType>({
    selected: -1,
    selections: ["A-Z", "Z-A", "Newest", "Oldest"],
    translations: ["lastName:asc", "lastName:desc", "dateApplied:desc", "dateApplied:asc"]
  });

  // TODO This state will be used to filter the list
  // TODO This will be changed based on the database (call API for list of officers)
  // Maybe we can add ID to each Loan Officer so that we can avoid loan officer with the same name when we filter
  const [assignedToSelections, setAssignedToSelections] = React.useState<Array<LoanOfficerType>>([]);

  const [startDate, setStartDate] = React.useState<MaterialUiPickersDate | null>(null);
  const [endDate, setEndDate] = React.useState<MaterialUiPickersDate | null>(null);

  const handleRemoveChip = (title: string) => {
    setSelectedFilters(selectedFilters.filter((filter) => filter.title !== title));
  };

  const handleAddChip = (title: string, type: FilterSections) => {
    setSelectedFilters([...selectedFilters, { type, title }]);
  };

  const handleRemoveSortChip = () => {
    setSelectedFilters(selectedFilters.filter((filter) => filter.type !== FilterSections.SORT_BY));
    setSortBySelections({ ...sortBySelections, selected: -1 });
  };

  const handleReplaceChipTitle = (oldTitle: string, newTitle: string) => {
    setSelectedFilters(
      selectedFilters.map((filter) => (filter.title !== oldTitle ? filter : { ...filter, title: newTitle }))
    );
  };

  /**
   * Handle input of start date
   * @param date value of Date from input
   */
  const handleStartDate = (date: MaterialUiPickersDate) => {
    setStartDate(date);
    let newFilters = selectedFilters.filter((filter) => filter.type !== FilterSections.AFTER);
    if (date) newFilters.push({ type: FilterSections.AFTER, title: date.format("MM/DD/yyyy") });
    setSelectedFilters(newFilters);
    if (date == null) {
      setRangeSpecified([startDate, endDate]);
    }
  };

  /**
   * Handle input of end date
   * @param date value of Date from input
   */
  const handleEndDate = (date: MaterialUiPickersDate) => {
    setEndDate(date);
    let newFilters = selectedFilters.filter((filter) => filter.type !== FilterSections.BEFORE);
    if (date) newFilters.push({ type: FilterSections.BEFORE, title: date.format("MM/DD/yyyy") });
    setSelectedFilters(newFilters);
    if (date == null) {
      setRangeSpecified([startDate, endDate]);
    }
  };

  /**
   * Handle apply button click
   */
  const handleApplyDate = () => {
    let newFilters = selectedFilters.filter(
      (filter) => filter.type !== FilterSections.BEFORE && filter.type !== FilterSections.AFTER
    );
    if (startDate) newFilters.push({ type: FilterSections.AFTER, title: startDate.format("MM/DD/yyyy") });
    if (endDate) newFilters.push({ type: FilterSections.BEFORE, title: endDate.format("MM/DD/yyyy") });
    setSelectedFilters(newFilters);
    setRangeSpecified([startDate, endDate]); //re-trigger API
  };

  /**
   * Handle toggle a check box of the Status section
   * @param title
   */
  const handleToggleStatus = (title: string) => {
    setStatusSelections(
      statusSelections.map((selection) => {
        if (selection.title !== title) return selection;
        if (!selection.checked) {
          handleAddChip(title, FilterSections.STATUS);
        } else {
          handleRemoveChip(selection.title);
        }
        return { ...selection, checked: !selection.checked };
      })
    );
    setPage(1);
  };

  /**
   * Handle toggle selected Loan Officers
   * @param name
   */
  const handleToggleChooseAssigned = (name: string) => {
    setAssignedToSelections(
      assignedToSelections.map((selection) => {
        if (selection.name !== name) return selection;
        if (!selection.selected) {
          handleAddChip(name, FilterSections.ASSIGNED_TO);
        } else {
          handleRemoveChip(selection.name);
        }
        return { ...selection, selected: !selection.selected };
      })
    );
    setPage(1);
  };

  /**
   * Handle chosing the sort type
   * @param name
   */
  const handleChooseSortType = (index: number) => {
    if (sortBySelections.selected !== -1 && index >= 0) {
      const oldTitle = sortBySelections.selections[sortBySelections.selected];
      const newTitle = sortBySelections.selections[index];
      handleReplaceChipTitle(oldTitle, newTitle);
    } else {
      handleAddChip(sortBySelections.selections[index], FilterSections.SORT_BY);
    }
    setSortBySelections({ ...sortBySelections, selected: index });
    setPage(1);
  };

  const renderChips = () => {
    return selectedFilters.map((filter, index) => {
      let deleteFunction: (title: string) => void;
      switch (filter.type) {
        case FilterSections.STATUS:
          deleteFunction = handleToggleStatus;
          break;
        case FilterSections.ASSIGNED_TO:
          deleteFunction = handleToggleChooseAssigned;
          break;
        case FilterSections.BEFORE:
          deleteFunction = () => handleEndDate(null);
          break;
        case FilterSections.AFTER:
          deleteFunction = () => handleStartDate(null);
          break;
        case FilterSections.SORT_BY:
          deleteFunction = handleRemoveSortChip;
          break;
        default:
          break;
      }
      return (
        <Grid key={index} item>
          <Chip
            label={`${filter.type}: ${filter.title}`}
            onDelete={() => deleteFunction(filter.title)}
            deleteIcon={<CancelIcon style={{ color: "rgba(0, 0, 0, 0.6)" }} />}
          />
        </Grid>
      );
    });
  };

  const [totalRows, setTotalRows] = React.useState<number>(0);
  const [data, setData] = React.useState<Application[] | undefined>([]);
  const [page, setPage] = React.useState<number>(1);
  const [searchValue, setSearchValue] = React.useState<string>("");
  const perPage = 5;

  const onPaginate = (newPage) => {
    setPage(newPage);
  };

  const [officers, setOfficers] = React.useState([]);

  /* Translate the applied status filters to format required by API */
  const getStatusFilterParam = (): string[] => {
    const statuses = statusSelections.filter((status) => {
      return status.checked;
    });

    const statusFilters: any[] = [];

    statuses.forEach((status) => {
      statusFilters.push(status.title);
    });
    return statusFilters;
  };

  /* Translate the applied assigned to filters to format required by API */
  const getAssignedToFilterParam = (): string[] => {
    const assignees = assignedToSelections.filter((officer) => {
      return officer.selected;
    });

    const assigneeFilters: any[] = [];

    assignees.forEach((officer) => {
      assigneeFilters.push(officer.id);
    });

    return assigneeFilters;
  };

  const fetchOfficers = React.useCallback(() => {
    apiOfficers
      .getAll({
        officers: {
          sortBy: "workload",
          populate: "user"
        }
      })
      .then((data) => {
        setOfficers(data.results);

        const officerState: any[] = [];

        data.results.map((officer) => {
          officerState.push({
            name: officer.user.firstName,
            selected: false,
            id: officer.id
          });
        });

        setAssignedToSelections(officerState);
      })
      .catch((err) => err);
  }, []);

  /* Initial render */
  React.useEffect(() => {
    fetchOfficers();
  }, []);

  React.useEffect(() => {
    window.addEventListener("popstate", fetchOfficers, false); // force partial refresh on browser back button
  });

  /* When filter parameters change */
  React.useEffect(() => {
    apiApplications
      .getAll({
        applications: {
          ...(startDate && { startDate: startDate.toString() }),
          ...(endDate && { endDate: endDate.toString() }),
          ...(searchValue && { nameOrEmail: searchValue }),
          status: getStatusFilterParam(),
          ...(sortBySelections.selected !== -1 && { sortBy: sortBySelections.translations[sortBySelections.selected] }),
          ...(getAssignedToFilterParam().length > 0 && { officer: getAssignedToFilterParam() }),
          page: page,
          limit: perPage,
          displayAll: true
        }
      })
      .then((res) => {
        if (res.totalResults > 0) {
          setData(res.results);
          setTotalRows(res.totalResults);
        } else {
          setData(undefined);
          setTotalRows(0);
        }
      })
      .catch((err) => console.log(err));
  }, [statusSelections, sortBySelections, assignedToSelections, searchValue, rangeSpecified, page]);

  const user = accessToken && jwtDecode<any>(accessToken).sub;

  const columns: ColumnType<Application>[] = [
    {
      title: "Name",
      render: (record) => (
        <RouterLink to={path + "/" + record.id} className="link">
          {record.firstName + " " + record.lastName}
        </RouterLink>
      )
    },
    {
      title: "Date Applied",
      render: (record) => moment(record.dateApplied).utc().format("MMM D, YYYY")
    },
    {
      title: "Status",
      render: (record) => record.status
    },
    {
      title: "Assigned To",
      render: (record, index) => (
        <LoanOfficerDropdown
          officers={officers}
          value={record.officer}
          currentUser={user}
          onChange={(e) => {
            apiApplications
              .patch(record.id, {
                applications: {
                  officer: e
                }
              })
              .then((res) => {
                if (res.status === 200) {
                  data![index] = res.data;
                  setData(data);
                  fetchOfficers();
                }
              })
              .catch((err) => {
                console.log(err.message);
              });
          }}
        />
      )
    }
  ];

  return (
    <div className="applications-layout">
      <ThemeProvider theme={theme}>
        <div className="header">
          {pathnames[pathnames.length - 1] !== routes.casesApplications.path && <BackButton pathnames={pathnames} />}
          <Switch>
            <Route exact path={path + "/:id/full"}>
              <TitleTypography key={path} variant="h4" color="textPrimary">
                Client Details
              </TitleTypography>
            </Route>
            <Route>
              <div id="cases-applications-header">
                <TitleTypography variant="h4" color="textPrimary">
                  {breadcrumbMap[pathnames[pathnames.length - 1]]}
                </TitleTypography>
                {location.pathname === routes.casesApplications.path && (
                  <Button
                    color="primary"
                    style={{ color: "white" }}
                    variant="contained"
                    id="add-btn"
                    component={RouterLink}
                    to={`${path}${routes.create.path}`}
                  >
                    <Add style={{ marginRight: "5px" }} />
                    Create application
                  </Button>
                )}
              </div>
            </Route>
          </Switch>
        </div>

        <Switch>
          <Route exact path={path}>
            <Paper className="main" elevation={0} square>
              <TableFilters
                disableFuture={true}
                statusSelections={statusSelections}
                handleToggleStatus={handleToggleStatus}
                sortBySelections={sortBySelections}
                handleChooseSortType={handleChooseSortType}
                assignedToSelections={assignedToSelections}
                handleToggleChooseAssigned={handleToggleChooseAssigned}
                startDate={startDate}
                endDate={endDate}
                setStartDate={setStartDate}
                setEndDate={setEndDate}
                handleApply={handleApplyDate}
                sorting={true}
                setSearchValue={setSearchValue}
                setPage={setPage}
              />
              <Grid container spacing={2} className="chips-field">
                {renderChips()}
              </Grid>
              <MyTable
                data={data!}
                columns={columns}
                pagination={{
                  perPage,
                  onPaginate,
                  totalRows,
                  page
                }}
                sort={{
                  defaultIndex: 1,
                  defaultDirection: "desc"
                }}
              />
            </Paper>
          </Route>
          <Route exact path={path + "/create"}>
            <CreateApplication officers={officers} />
          </Route>
          <Route path={path + "/:id"}>
            <ApplicationDetails createEditContractSubmissionRef={createContractSubmissionRef} />
          </Route>
        </Switch>
      </ThemeProvider>
    </div>
  );
};

export default withRouter(ApplicationsLayout);
