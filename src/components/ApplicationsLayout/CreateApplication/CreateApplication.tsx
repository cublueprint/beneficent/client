import * as React from "react";
import { useState } from "react";
import {
  Grid,
  Card,
  CardHeader,
  CardContent,
  Typography,
  TextField,
  Checkbox,
  RadioGroup,
  Radio,
  FormControlLabel,
  Select,
  MenuItem,
  Button,
  Divider,
  withStyles,
  makeStyles,
  createMuiTheme,
  FormHelperText,
  MuiThemeProvider
} from "@material-ui/core";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { Formik } from "formik";
import * as Yup from "yup";
import NumberFormat from "react-number-format";
import "./CreateApplication.scss";
import LoanOfficerDropdown from "../../common/LoanOfficerDropdown/LoanOfficerDropdown";
import { apiApplications } from "../../../services/api/applications/apiApplications";
import Dialogue from "../../common/Dialogue/Dialogue";
import MomentUtils from "@date-io/moment";
/*
import ROUTES from "../../../routes";
import { useHistory } from "react-router-dom";
*/

/**
 * Props of FieldTitle
 */
interface FieldTitleProps {
  text: string;
  required?: boolean;
  bold?: boolean;
}

/**
 * Smaller title for small fields
 */
const FieldTitle = ({ text, required = false, bold = false }: FieldTitleProps) => (
  <Typography
    style={{ marginBottom: 7, fontWeight: bold ? "bold" : 400 }}
    className={`field-title ${required ? "required" : ""}`}
  >
    {text}
  </Typography>
);

/**
 * FormControlLabel with height/margin changed
 */
const StyledFormControlLabel = withStyles({
  root: {
    height: 30,
    marginRight: 40
  }
})(FormControlLabel);

/**
 * Checkbox colored as design
 */
const ColoredCheckbox = withStyles({
  root: {
    "&$checked": {
      color: "#DA6E5D",
      "&$disabled": {
        color: "rgba(30, 105, 255, 0.38)"
      }
    }
  },
  checked: {},
  disabled: {}
})((props: any) => <Checkbox color="default" {...props} />);

/**
 * Radio button colored as design
 */
const ColoredRadio = withStyles({
  root: {
    "&$checked": {
      color: "#DA6E5D",
      "&$disabled": {
        color: "rgba(30, 105, 255, 0.38)"
      }
    }
  },
  checked: {},
  disabled: {}
})((props: any) => <Radio color="default" {...props} />);

const RedButtonTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#DA6E5D"
    }
  },
  typography: {
    fontFamily: "Lato"
  }
  // shadows: Array<string>(25).fill('none')
});

/**
 * Props of StyledSelect
 */
interface StyledSelectProps {
  name: string;
  error?: boolean;
  maxWidth?: number;
  currentValue: string | undefined | null;
  selections: Array<string>;
  disabled?: boolean;
  maxItemShowed?: number;
  onChange: (event: React.ChangeEvent<{ value: unknown }>) => void;
  onBlur: (event: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
}

/**
 * A styled Select component with custom width
 * @param param0 StyledSelectProps
 */
const StyledSelect = ({
  name,
  error,
  currentValue,
  selections,
  disabled,
  maxItemShowed = 6,
  onChange,
  onBlur
}: StyledSelectProps) => {
  const itemHeight = 55;

  return (
    <Select
      fullWidth
      name={name}
      error={error}
      style={{ maxHeight: 56, padding: 0 }}
      disabled={disabled}
      value={currentValue}
      variant="outlined"
      onChange={onChange}
      onBlur={onBlur}
      MenuProps={{
        getContentAnchorEl: null,
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "left"
        },
        PaperProps: {
          style: {
            maxHeight: maxItemShowed * itemHeight,
            padding: 0
          }
        },
        MenuListProps: { disablePadding: true }
      }}
    >
      {selections.map((selection, index) => (
        <MenuItem
          style={{ height: itemHeight, borderBottom: "1px solid rgba(33, 33, 33, 0.08)" }}
          key={index}
          value={selection}
        >
          {selection}
        </MenuItem>
      ))}
    </Select>
  );
};

interface NumberFormatProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

function CurrencyFormat(props: NumberFormatProps) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      thousandSeparator
      isNumericString
      isAllowed={(values) => {
        const { formattedValue, floatValue } = values;
        return Boolean(formattedValue === "" || (floatValue && floatValue <= 1000000));
      }}
    />
  );
}

function PhoneFormat(props: NumberFormatProps) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      format="(###)-###-####"
      placeholder="(___)-___-____"
      mask={["_", "_", "_", "_", "_", "_", "_", "_", "_", "_"]}
      isNumericString
    />
  );
}

interface CheckFieldsLabelProps {
  label: string;
  error: boolean;
}

const CheckFieldsLabel = ({ label, error }: CheckFieldsLabelProps) => (
  <Typography color={error ? "error" : "textPrimary"} variant="body2">
    {label}
  </Typography>
);

interface ErrorTextProps {
  error: string | undefined;
}

const ErrorText = ({ error }: ErrorTextProps) => (
  <FormHelperText error={Boolean(error)} className={error ? "error-text" : "hidden"}>
    {error}
  </FormHelperText>
);

/**
 * Style override TextField
 */
const useStyle = makeStyles({
  textField: {
    "&$textFieldDisabled": {
      color: "rgba(0,0,0,0.85)"
    }
  },
  textFieldDisabled: {}
});

enum Acknowledgements {
  LOAN_PURPOSE = 0,
  MAX_LOAN = 1,
  REPAYMENT = 2,
  RESIDENCE = 3,
  NET_POSITIVE_INCOME = 4,
  GUARANTOR_CONSENT = 5
}

enum Services {
  CAREER_COACHING = 0,
  RESUME_CRITIQUE = 1,
  FINANCIAL_COACHING = 2,
  COMMUNITY_RESOURCES = 3
}

/**
 * Mockup type of full info
 */
type DetailsInfoType = {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  preferredLanguage: string;
  maritalStatus: string;
  employmentStatus: string;
  address: string;
  city: string;
  province: string;
  postalCode: string;
  sex: string;
  dateOfBirth: Date | null;
  citizenship: string;
  loanAmountRequested: number | null;
  // TODO These values may change based on the back-end
  loanType: "credit" | "emergency" | "other" | null;
  debtCircumstances: string;
  guarantor: boolean | null;
  recommendationInfo: "LinkedIn" | "Other" | "";
  // TODO This is array of chosen indices. We may change this based on the backend
  acknowledgements: {
    [Acknowledgements.LOAN_PURPOSE]: boolean;
    [Acknowledgements.MAX_LOAN]: boolean;
    [Acknowledgements.REPAYMENT]: boolean;
    [Acknowledgements.RESIDENCE]: boolean;
    [Acknowledgements.NET_POSITIVE_INCOME]: boolean;
    [Acknowledgements.GUARANTOR_CONSENT]: boolean;
  };
  services: {
    [Services.CAREER_COACHING]: boolean;
    [Services.RESUME_CRITIQUE]: boolean;
    [Services.FINANCIAL_COACHING]: boolean;
    [Services.COMMUNITY_RESOURCES]: boolean;
  };
  mailOptIn: boolean;
};

interface CreateApplicationProps {
  /**
   * An optional prop that specifies if the page is for view only or for applying
   */
  officers?: {
    workload: number;
    user: {
      role: string;
      isEmailVerified: boolean;
      firstName: string;
      lastName: string;
      phoneNumber: string;
      address: string;
      country: string;
      email: string;
      createdAt: string;
      updatedAt: string;
      id: string;
    };
    createdAt: string;
    updatedAt: string;
    id: string;
  }[];
  viewOnly?: boolean;
  handleSubmitApplication?: Function;
}

// TODO This may change based on the design
const maritalStatusSelections = ["Single", "Separated", "Married", "Widow/Widower", "Divorced", "Common Law"];

const sexSelections = ["Male", "Female", "X"];

const provinces = [
  "Alberta",
  "British Columbia",
  "Manitoba",
  "New Brunswick",
  "Newfoundland and Labrador",
  "Northwest Territories",
  "Nova Scotia",
  "Nunavut",
  "Ontario",
  "Prince Edward Island",
  "Quebec",
  "Saskatchewan",
  "Yukon"
];

const employmentStatusSelections = ["Employed Full Time", "Employed Part Time", "Seasonal/Contract", "Unemployed"];

const preferredLanguagesSelections = ["English", "French", "Arabic", "Other"];

const citizenshipSelections = ["Canadian", "Permanent Resident", "Other"];

/**
 * **Full** detail page of an application
 */
const CreateApplication: React.FC<CreateApplicationProps> = (props) => {
  const accessToken = localStorage.getItem("accessToken");
  const classes = useStyle();
  const { officers, viewOnly } = props;
  const [assigned, setAssigned] = React.useState("");
  /*
  const history = useHistory();
  useEffect(() => {
    history.push(ROUTES.home.path);
  }, [history]);
  */

  /* eslint-disable */
  const [info, setInfo] = useState<DetailsInfoType>({
    firstName: "Clyde",
    lastName: "D'Souza",
    email: "Clyde.Dsouza@email.com",
    phone: "123-456----7890",
    preferredLanguage: "English",
    employmentStatus: "Unemployed",
    maritalStatus: "Single",
    address: "123 Fake Street",
    city: "Toronto",
    province: "Ontario",
    postalCode: "A1B2C3",
    sex: "Male",
    dateOfBirth: null,
    citizenship: "Canadian",
    loanAmountRequested: 1500,
    loanType: "emergency",
    debtCircumstances: "",
    guarantor: false,
    recommendationInfo: "LinkedIn",
    acknowledgements: {
      [Acknowledgements.LOAN_PURPOSE]: true,
      [Acknowledgements.MAX_LOAN]: true,
      [Acknowledgements.REPAYMENT]: true,
      [Acknowledgements.RESIDENCE]: true,
      [Acknowledgements.NET_POSITIVE_INCOME]: true,
      [Acknowledgements.GUARANTOR_CONSENT]: true
    },
    services: {
      [Services.CAREER_COACHING]: false,
      [Services.RESUME_CRITIQUE]: true,
      [Services.FINANCIAL_COACHING]: true,
      [Services.COMMUNITY_RESOURCES]: true
    },
    mailOptIn: false
  });

  const emptyInfo: DetailsInfoType = {
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    preferredLanguage: "",
    maritalStatus: "",
    employmentStatus: "",
    address: "",
    city: "",
    province: "",
    postalCode: "",
    sex: "",
    dateOfBirth: null,
    citizenship: "",
    loanAmountRequested: null,
    loanType: null,
    debtCircumstances: "",
    guarantor: null,
    recommendationInfo: "",
    acknowledgements: {
      [Acknowledgements.LOAN_PURPOSE]: false,
      [Acknowledgements.MAX_LOAN]: false,
      [Acknowledgements.REPAYMENT]: false,
      [Acknowledgements.RESIDENCE]: false,
      [Acknowledgements.NET_POSITIVE_INCOME]: false,
      [Acknowledgements.GUARANTOR_CONSENT]: false
    },
    services: {
      [Services.CAREER_COACHING]: false,
      [Services.RESUME_CRITIQUE]: false,
      [Services.FINANCIAL_COACHING]: false,
      [Services.COMMUNITY_RESOURCES]: false
    },
    mailOptIn: false
  };

  const handleAssignedTo = (e) => {
    setAssigned(e);
  };
  const [showModal, setModal] = React.useState(false);

  // POST application
  const postApplication = React.useCallback(
    (values, submitProps) => {
      let payload = {
        firstName: values.firstName,
        lastName: values.lastName,
        email: values.email,
        phoneNumber: values.phone,
        address: values.address,
        city: values.city,
        province: values.province,
        sex: values.sex,
        dateOfBirth: values.dateOfBirth,
        maritalStatus: values.maritalStatus,
        citizenship: values.citizenship,
        postalCode: values.postalCode,
        preferredLanguage: values.preferredLanguage,
        employmentStatus: values.employmentStatus,
        loanAmountRequested: values.loanAmountRequested,
        loanType: values.loanType,
        debtCircumstances: values.debtCircumstances,
        recommendationInfo: values.recommendationInfo,
        status: "Received",
        guarantor: {
          hasGuarantor: values.guarantor
        },
        acknowledgements: {
          loanPurpose: values.acknowledgements["0"],
          maxLoan: values.acknowledgements["1"],
          repayment: values.acknowledgements["2"],
          residence: values.acknowledgements["3"],
          netPositiveIncome: values.acknowledgements["4"],
          guarantorConsent: values.acknowledgements["5"]
        },
        services: {
          careerCoaching: values.services["0"],
          resumeCritique: values.services["1"],
          financialCoaching: values.services["2"],
          communityResources: values.services["3"]
        },
        emailOptIn: values.mailOptIn
      };
      if (assigned != "") {
        payload["officer"] = assigned;
      }

      apiApplications
        .post({
          applications: payload
        })
        .then((res) => {
          if (res.code == undefined) {
            setModal(true);
            submitProps.resetForm();
            setAssigned("");
          }
        });
    },
    [accessToken, assigned]
  );

  return (
    <Formik
      onSubmit={(values, submitProps) => {
        setModal(false);
        postApplication(values, submitProps);
      }}
      enableReinitialize
      validateOnBlur={false}
      validateOnChange={false}
      initialValues={viewOnly ? info : emptyInfo}
      validationSchema={Yup.object().shape({
        firstName: Yup.string().required("First name is required"),
        lastName: Yup.string().required("Last name is required"),
        email: Yup.string().email("Email must be valid email").required("Email is required"),
        phone: Yup.string().required("Phone number is required"),
        maritalStatus: Yup.string().required("Marital status is required"),
        employmentStatus: Yup.string().required("Employment status is required"),
        preferredLanguage: Yup.string().required("Preferred language is required"),
        address: Yup.string().required("Address is required"),
        city: Yup.string().required("City is required"),
        province: Yup.string().required("Province is required"),
        postalCode: Yup.string().matches(
          /^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ -]?\d[ABCEGHJ-NPRSTV-Z]\d$/i,
          "Postal code must be a valid Canadian postal code"
        ),
        sex: Yup.string().required("Sex is required").oneOf(sexSelections, "Choose a valid sex"),
        dateOfBirth: Yup.date().nullable().typeError("Invalid date entered").required("Date of birth is required"),
        citizenship: Yup.string().required("Citizenship is required"),
        loanAmountRequested: Yup.number().nullable().required("Loan amount requested is required"),
        loanType: Yup.string().nullable().required("Loan type is required"),
        debtCircumstances: Yup.string().required("Debt circumstances is required"),
        guarantor: Yup.boolean().nullable().required("Guarantor is required"),
        recommendationInfo: Yup.string().required("Recommendation info is required"),
        mailOptIn: Yup.boolean().required("Mail opt-in selection is required")
      })}
    >
      {(props) => {
        const { values, touched, errors, setFieldValue, handleChange, handleBlur, handleSubmit } = props;

        return (
          <MuiPickersUtilsProvider utils={MomentUtils}>
            <form data-testid="form" className="create-application-form" onSubmit={handleSubmit}>
              <Card className="form-card">
                <CardHeader title="Personal Information" />
                <Divider variant="middle" className="divider-top" />

                <CardContent>
                  <Grid container spacing={4}>
                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="First Name" required />
                        <TextField
                          data-testid="firstName"
                          name="firstName"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.firstName)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          InputProps={{
                            classes: {
                              root: classes.textField,
                              disabled: classes.textFieldDisabled
                            }
                          }}
                          value={values.firstName}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.firstName} />
                      </Grid>

                      <Grid item xs={4}>
                        <FieldTitle text="Last Name" required />
                        <TextField
                          data-testid="lastName"
                          name="lastName"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.lastName)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          InputProps={{
                            classes: {
                              root: classes.textField,
                              disabled: classes.textFieldDisabled
                            }
                          }}
                          value={values.lastName}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.lastName} />
                      </Grid>

                      {!officers ? (
                        <Grid item xs={3} />
                      ) : (
                        <Grid item xs={3}>
                          <FieldTitle text="Assigned" />
                          <LoanOfficerDropdown officers={officers} value={assigned} onChange={handleAssignedTo} />
                        </Grid>
                      )}
                    </Grid>

                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="Email" required />
                        <TextField
                          data-testid="email"
                          name="email"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.email)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          InputProps={{
                            classes: {
                              root: classes.textField,
                              disabled: classes.textFieldDisabled
                            }
                          }}
                          value={values.email}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.email} />
                      </Grid>

                      <Grid item xs={4}>
                        <FieldTitle text="Phone Number" required />
                        <TextField
                          data-testid="phone"
                          type="tel"
                          name="phone"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.phone)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          InputProps={{
                            classes: {
                              root: classes.textField,
                              disabled: classes.textFieldDisabled
                            },
                            inputComponent: PhoneFormat as any
                          }}
                          value={values.phone.match(/\d/g)}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.phone} />
                      </Grid>

                      <Grid item xs={3}>
                        <FieldTitle text="Preferred Language" required />
                        <StyledSelect
                          name="preferredLanguage"
                          error={Boolean(errors.preferredLanguage)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          currentValue={values.preferredLanguage}
                          selections={preferredLanguagesSelections}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.preferredLanguage} />
                      </Grid>
                    </Grid>

                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="Marital Status" required />
                        <StyledSelect
                          name="maritalStatus"
                          error={Boolean(errors.maritalStatus)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          currentValue={values.maritalStatus}
                          selections={maritalStatusSelections}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.maritalStatus} />
                      </Grid>

                      <Grid item xs={4}>
                        <FieldTitle text="Citizenship" required />
                        <StyledSelect
                          name="citizenship"
                          error={Boolean(errors.citizenship)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          currentValue={values.citizenship}
                          selections={citizenshipSelections}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.citizenship} />
                      </Grid>

                      <Grid item xs={3}>
                        <FieldTitle text="Sex" required />
                        <StyledSelect
                          name="sex"
                          error={Boolean(errors.sex)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          currentValue={values.sex}
                          selections={sexSelections}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.sex} />
                        <Typography className="field-subtitle" variant="body2" color="textSecondary" display="inline">
                          Please enter the sex that is specified on your legal documents.
                        </Typography>
                      </Grid>
                    </Grid>

                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="Employment Status" required />
                        <StyledSelect
                          name="employmentStatus"
                          error={Boolean(errors.employmentStatus)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          currentValue={values.employmentStatus}
                          selections={employmentStatusSelections}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.employmentStatus} />
                      </Grid>

                      <Grid item xs={4}>
                        <FieldTitle text="Date of Birth" required />
                        <KeyboardDatePicker
                          id="dateOfBirth"
                          inputVariant="outlined"
                          variant="inline"
                          disableToolbar
                          fullWidth
                          format="yyyy/MM/DD"
                          value={values.dateOfBirth}
                          error={Boolean(errors.dateOfBirth)}
                          helperText=""
                          onChange={(date) => setFieldValue("dateOfBirth", date)}
                        />
                        <ErrorText error={errors.dateOfBirth} />
                      </Grid>
                      <Grid item xs={3} />
                    </Grid>
                  </Grid>
                </CardContent>

                <Divider variant="middle" className="divider-mid" />

                <CardContent>
                  <Grid container spacing={4}>
                    <Grid item xs={12}>
                      <FieldTitle text="Address" required />
                      <TextField
                        data-testid="address"
                        name="address"
                        variant="outlined"
                        fullWidth
                        error={Boolean(errors.address)}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        InputProps={{
                          classes: {
                            root: classes.textField,
                            disabled: classes.textFieldDisabled
                          }
                        }}
                        value={values.address}
                        disabled={viewOnly}
                      />
                      <ErrorText error={errors.address} />
                    </Grid>

                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="City" required />
                        <TextField
                          data-testid="city"
                          name="city"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.city)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          InputProps={{
                            classes: {
                              root: classes.textField,
                              disabled: classes.textFieldDisabled
                            }
                          }}
                          value={values.city}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.city} />
                      </Grid>

                      <Grid item xs={4}>
                        <FieldTitle text="Province" required />
                        <StyledSelect
                          name="province"
                          error={Boolean(errors.province)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          maxWidth={700}
                          currentValue={values.province}
                          selections={provinces}
                          disabled={viewOnly}
                          maxItemShowed={5}
                        />
                        <ErrorText error={errors.province} />
                      </Grid>

                      <Grid item xs={3}>
                        <FieldTitle text="Postal Code" required />
                        <TextField
                          data-testid="postalCode"
                          name="postalCode"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.postalCode)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          InputProps={{
                            classes: {
                              root: classes.textField,
                              disabled: classes.textFieldDisabled
                            }
                          }}
                          value={values.postalCode}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.postalCode} />
                      </Grid>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>

              <Card className="form-card">
                <CardHeader title="Loan Request" />
                <Divider variant="middle" className="divider-top" />

                <CardContent>
                  <Grid container spacing={4}>
                    <Grid item xs={10}>
                      <FieldTitle text="Loan Amount Requested" required bold />
                      <Typography className="field-subtitle" variant="body2" color="textSecondary">
                        Please note the maximum amount is $4,000
                      </Typography>
                      <TextField
                        data-testid="loanAmountRequested"
                        name="loanAmountRequested"
                        variant="outlined"
                        fullWidth
                        error={Boolean(errors.loanAmountRequested)}
                        onChange={(e) => setFieldValue("loanAmountRequested", Number(e.target.value))}
                        onBlur={handleBlur}
                        InputProps={{
                          classes: {
                            root: classes.textField,
                            disabled: classes.textFieldDisabled
                          },
                          inputComponent: CurrencyFormat as any
                        }}
                        value={values.loanAmountRequested ? values.loanAmountRequested : ""}
                        disabled={viewOnly}
                      />
                      <ErrorText error={errors.loanAmountRequested} />
                    </Grid>

                    <Grid item xs={10}>
                      <FieldTitle text="Loan Type" required bold />
                      <RadioGroup name="loanType" row onChange={handleChange} value={values.loanType}>
                        <StyledFormControlLabel
                          value="Credit"
                          disabled={viewOnly}
                          control={<ColoredRadio />}
                          label={<CheckFieldsLabel label="Credit Card" error={Boolean(errors.loanType)} />}
                        />
                        <StyledFormControlLabel
                          value="Emergency"
                          disabled={viewOnly}
                          control={<ColoredRadio />}
                          label={<CheckFieldsLabel label="Emergency" error={Boolean(errors.loanType)} />}
                        />
                        <StyledFormControlLabel
                          value="Other"
                          disabled={viewOnly}
                          control={<ColoredRadio />}
                          label={<CheckFieldsLabel label="Other" error={Boolean(errors.loanType)} />}
                        />
                      </RadioGroup>
                      <ErrorText error={errors.loanType} />
                    </Grid>

                    <Grid item xs={10}>
                      <FieldTitle text="Debt Circumstances" required bold />
                      <Typography className="field-subtitle" variant="body2" color="textSecondary">
                        Please describe the circumstances of your debt. Why are you in debt?
                      </Typography>
                      <TextField
                        data-testid="debtCircumstances"
                        name="debtCircumstances"
                        variant="outlined"
                        fullWidth
                        error={Boolean(errors.debtCircumstances)}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        rows={3}
                        multiline
                        InputProps={{
                          classes: {
                            root: classes.textField,
                            disabled: classes.textFieldDisabled
                          }
                        }}
                        value={values.debtCircumstances}
                        disabled={viewOnly}
                      />
                      <ErrorText error={errors.debtCircumstances} />
                    </Grid>

                    <Grid item xs={10}>
                      <FieldTitle text="Do you have a guarantor?" required bold />
                      <Typography className="field-subtitle" variant="body2" color="textSecondary">
                        A guarantor is responsible for paying your loan in the unfortunate scenario that you cannot make your
                        payments. Please note that all loans require the consent of a guarantor.
                      </Typography>
                      <RadioGroup row value={values.guarantor}>
                        <StyledFormControlLabel
                          name="guarantor"
                          value={true}
                          disabled={viewOnly}
                          control={<ColoredRadio onChange={() => setFieldValue("guarantor", true)} />}
                          label={<CheckFieldsLabel label="Yes" error={Boolean(errors.guarantor)} />}
                        />
                        <StyledFormControlLabel
                          name="guarantor"
                          value={false}
                          disabled={viewOnly}
                          control={<ColoredRadio onChange={() => setFieldValue("guarantor", false)} />}
                          label={<CheckFieldsLabel label="No" error={!!touched.guarantor && !!errors.guarantor} />}
                        />
                      </RadioGroup>
                      <ErrorText error={errors.guarantor} />
                    </Grid>

                    <Grid item xs={10}>
                      <FieldTitle text="How did you hear about Beneficent?" required bold />
                      <TextField
                        data-testid="recommendationInfo"
                        name="recommendationInfo"
                        variant="outlined"
                        fullWidth
                        error={Boolean(errors.recommendationInfo)}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        InputProps={{
                          classes: {
                            root: classes.textField,
                            disabled: classes.textFieldDisabled
                          }
                        }}
                        value={values.recommendationInfo}
                        disabled={viewOnly}
                      />
                      <ErrorText error={errors.recommendationInfo} />
                    </Grid>
                    <Grid item>
                      <FieldTitle text="Are you interested in any of the following Beneficent services?" bold />

                      <Grid container direction="column">
                        <StyledFormControlLabel
                          value={0}
                          name={`services[${Services.CAREER_COACHING}]`}
                          control={
                            <ColoredCheckbox
                              checked={values.services[Services.CAREER_COACHING]}
                              disabled={viewOnly}
                              onChange={handleChange}
                            />
                          }
                          label={<CheckFieldsLabel label="Career Coaching" error={Boolean(errors.services?.[0])} />}
                          classes={{ root: "services-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={1}
                          name={`services[${Services.RESUME_CRITIQUE}]`}
                          control={
                            <ColoredCheckbox
                              checked={values.services[Services.RESUME_CRITIQUE]}
                              disabled={viewOnly}
                              onChange={handleChange}
                            />
                          }
                          label={<CheckFieldsLabel label="Resume Critique" error={Boolean(errors.services?.[1])} />}
                          classes={{ root: "services-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={2}
                          name={`services[${Services.FINANCIAL_COACHING}]`}
                          control={
                            <ColoredCheckbox
                              checked={values.services[Services.FINANCIAL_COACHING]}
                              disabled={viewOnly}
                              onChange={handleChange}
                            />
                          }
                          label={<CheckFieldsLabel label="Financial Coaching" error={Boolean(errors.services?.[2])} />}
                          classes={{ root: "services-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={3}
                          name={`services[${Services.COMMUNITY_RESOURCES}]`}
                          control={
                            <ColoredCheckbox
                              checked={values.services[Services.COMMUNITY_RESOURCES]}
                              disabled={viewOnly}
                              onChange={handleChange}
                            />
                          }
                          label={
                            <CheckFieldsLabel label="Community Resources" error={Boolean(errors.acknowledgements?.[3])} />
                          }
                          classes={{ root: "services-StyledFormControlLabel" }}
                        />
                      </Grid>
                    </Grid>

                    <Grid item>
                      <FieldTitle text="Acknowledgements" bold />
                      <Typography className="field-subtitle" variant="body2" color="textSecondary">
                        I acknowledge that the following criteria must be met in order to be eligible for a loan with
                        Beneficent:
                      </Typography>

                      <Grid container direction="column">
                        <StyledFormControlLabel
                          value={0}
                          name={`acknowledgements[${Acknowledgements.LOAN_PURPOSE}]`}
                          control={
                            <ColoredCheckbox
                              checked={values.acknowledgements[Acknowledgements.LOAN_PURPOSE]}
                              disabled={viewOnly}
                              onChange={handleChange}
                            />
                          }
                          label={
                            <CheckFieldsLabel
                              label="Loan purpose is for credit card, emergency,
                          or similar high interest bearing debt"
                              error={Boolean(errors.acknowledgements?.[0])}
                            />
                          }
                          classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={1}
                          name={`acknowledgements[${Acknowledgements.MAX_LOAN}]`}
                          control={
                            <ColoredCheckbox
                              checked={values.acknowledgements[Acknowledgements.MAX_LOAN]}
                              disabled={viewOnly}
                              onChange={handleChange}
                            />
                          }
                          label={
                            <CheckFieldsLabel
                              label="Maximum loan value of $4,000"
                              error={Boolean(errors.acknowledgements?.[1])}
                            />
                          }
                          classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={2}
                          name={`acknowledgements[${Acknowledgements.REPAYMENT}]`}
                          control={
                            <ColoredCheckbox
                              checked={values.acknowledgements[Acknowledgements.REPAYMENT]}
                              disabled={viewOnly}
                              onChange={handleChange}
                            />
                          }
                          label={
                            <CheckFieldsLabel
                              label="Repayment within 12 months"
                              error={Boolean(errors.acknowledgements?.[2])}
                            />
                          }
                          classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={3}
                          name={`acknowledgements[${Acknowledgements.RESIDENCE}]`}
                          control={
                            <ColoredCheckbox
                              checked={values.acknowledgements[Acknowledgements.RESIDENCE]}
                              disabled={viewOnly}
                              onChange={handleChange}
                            />
                          }
                          label={
                            <CheckFieldsLabel label="Residence in Canada" error={Boolean(errors.acknowledgements?.[3])} />
                          }
                          classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={4}
                          name={`acknowledgements[${Acknowledgements.NET_POSITIVE_INCOME}]`}
                          control={
                            <ColoredCheckbox
                              checked={values.acknowledgements[Acknowledgements.NET_POSITIVE_INCOME]}
                              disabled={viewOnly}
                              onChange={handleChange}
                            />
                          }
                          label={
                            <CheckFieldsLabel label="Positive net income" error={Boolean(errors.acknowledgements?.[4])} />
                          }
                          classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={5}
                          name={`acknowledgements[${Acknowledgements.GUARANTOR_CONSENT}]`}
                          control={
                            <ColoredCheckbox
                              checked={values.acknowledgements[Acknowledgements.GUARANTOR_CONSENT]}
                              disabled={viewOnly}
                              onChange={handleChange}
                            />
                          }
                          label={
                            <CheckFieldsLabel label="Guarantor consent" error={Boolean(errors.acknowledgements?.[5])} />
                          }
                          classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                        />
                      </Grid>
                      <ErrorText error={errors.acknowledgements ? "All acknowledgements must be checked" : undefined} />
                    </Grid>

                    <Grid item container direction="column">
                      <FieldTitle text="Email Opt-In" bold />
                      <StyledFormControlLabel
                        name="mailOptIn"
                        control={<ColoredCheckbox checked={values.mailOptIn} disabled={viewOnly} onChange={handleChange} />}
                        label={
                          <CheckFieldsLabel label="Consent to receive marketing emails" error={Boolean(errors.mailOptIn)} />
                        }
                      />
                      <ErrorText error={errors.mailOptIn} />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
              <MuiThemeProvider theme={RedButtonTheme}>
                <Button disabled={false} variant="contained" color="primary" type="submit" className="submit-button">
                  Submit
                </Button>

                {showModal ? (
                  <Dialogue title={"Success"} text={"This application has been submitted."} open={showModal} hasClose />
                ) : null}
              </MuiThemeProvider>
            </form>
          </MuiPickersUtilsProvider>
        );
      }}
    </Formik>
  );
};

export default CreateApplication;
