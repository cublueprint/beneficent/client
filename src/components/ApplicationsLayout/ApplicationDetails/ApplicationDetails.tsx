import { Button, Grid } from "@material-ui/core";
import * as React from "react";
import { RefObject, useEffect, useState } from "react";
import "./ApplicationDetails.scss";
import { Route, Switch, useParams } from "react-router-dom";
import routes from "../../../routes";
import CreateEditContract from "../ApplicationDetails/ApplicationOverview/ContractDetails/Contracts/CreateEditContract/CreateEditContract";
import EditApplication from "./EditApplication/EditApplication";
import ApplicationOverview from "./ApplicationOverview/ApplicationOverview";
import ApplicationDocuments from "./ApplicationDocuments/ApplicationDocuments";
import { FormikSubmitObject } from "../ApplicationsLayout";
import PersonalInformation from "./PersonalInformation/PersonalInformation";
import { apiApplications } from "../../../services/api/applications/apiApplications";
import { apiOfficers } from "../../../services/api/officers/apiOfficers";
import ContractCard from "./ApplicationOverview/ContractDetails/Contracts/ContractCard";
import { TitleTypography } from "../../HomePage/AdminHomePage/AdminHomePage";
import jwtDecode from "jwt-decode";

interface ApplicationDetailsProps {
  createEditContractSubmissionRef: RefObject<FormikSubmitObject>;
}

interface ApplicationInfo {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  address: string;
  city: string;
  province: string;
  sex: string;
  dateOfBirth: string;
  maritalStatus: string;
  citizenship: string;
  preferredLanguage: string;
  employmentStatus: string;
  loanAmountRequested: number;
  loanType: string;
  debtCircumstances: string;
  postalCode: string;
  guarantor: {
    hasGuarantor: boolean;
    fullName: string;
    email: string;
    phoneNumber: string;
  };
  acknowledgements: {
    loanPurpose: boolean;
    maxLoan: boolean;
    residence: boolean;
    netPositiveIncome: boolean;
    guarantorConsent: boolean;
    repayment: boolean;
  };
  services: {
    careerCoaching: boolean;
    resumeCritique: boolean;
    financialCoaching: boolean;
    communityResources: boolean;
  };
  recommendationInfo: string;
  emailOptIn: boolean;
  status: string;
  officer: string;
  user: string;
}

/**
 * Detail page of a application
 */
const ApplicationDetails = (props: ApplicationDetailsProps) => {
  const { createEditContractSubmissionRef } = props;
  const { id } = useParams<{ id: string }>();
  const token = window.localStorage.getItem("accessToken");
  const [applicationInfo, setApplicationInfo] = useState<ApplicationInfo>();
  const [officers, setOfficers] = useState([]);
  const user = token && jwtDecode<any>(token).sub;

  // TODO: Add redirection if the user-id is not available
  useEffect(() => {
    apiApplications
      .getSingle(id)
      .then((res) => {
        setApplicationInfo(res);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, [id, token]);

  useEffect(() => {
    apiOfficers
      .getAll({
        officers: {
          sortBy: "workload",
          populate: "user"
        }
      })
      .then((data) => {
        setOfficers(data.results);
      })
      .catch((err) => err);
  }, [token]);

  return (
    <Grid className="application-details-container" spacing={3} container data-testid="skeleton-container">
      <Switch>
        <Route exact path={routes.casesApplications.path + "/:id"}>
          <Grid spacing={2} item container direction="row">
            <Grid item xs={12}>
              <TitleTypography variant="h4" color="textPrimary">
                Application Details
              </TitleTypography>
            </Grid>
            <Grid item xs={12}>
              <PersonalInformation
                setAppInfo={(appInfo) => setApplicationInfo(appInfo)}
                officers={officers}
                appInfo={applicationInfo!}
              />
            </Grid>
            <Grid item xs={12}>
              <ApplicationOverview appInfo={applicationInfo!} />
            </Grid>
            <Grid item xs={12}>
              <ApplicationDocuments applicationId={id} userId={user} />
            </Grid>
            <Grid item xs={12}>
              <ContractCard applicationId={id} />
            </Grid>
          </Grid>
        </Route>
        <Route exact path={routes.casesApplications.path + "/:id/edit"}>
          <Grid item container>
            {/* TODO Replace the line below by <ApplicationFullDetails /> component*/}
            <EditApplication officersList={officers} appInfo={applicationInfo!}></EditApplication>
          </Grid>
        </Route>
        <Route exact path={routes.casesApplications.path + routes.createContract.path}>
          {/* TODO Replace this paper by <EditContract /> component */}
          <Grid item container>
            <CreateEditContract ref={createEditContractSubmissionRef} />
          </Grid>
        </Route>
      </Switch>
    </Grid>
  );
};

export default ApplicationDetails;
