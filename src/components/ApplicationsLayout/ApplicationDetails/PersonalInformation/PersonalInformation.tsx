import * as React from "react";
import { useEffect, useState } from "react";
import { Button, Grid, Card, CardHeader, CardContent, Typography, TextField } from "@material-ui/core";

import "./PersonalInformation.scss";
import LoanOfficerDropdown from "../../../common/LoanOfficerDropdown/LoanOfficerDropdown";
import jwtDecode from "jwt-decode";
import StatusDropdown from "../../../common/StatusDropdown/StatusDropdown";
import { apiApplications } from "../../../../services/api/applications/apiApplications";
import NumberFormat from "react-number-format";
import Dialogue from "../../../common/Dialogue/Dialogue";

interface PersonalInformationProps {
  /**
   * An optional prop that specifies if the page is for view only or for applying
   */
  officers: {
    workload: number;
    user: {
      role: string;
      isEmailVerified: boolean;
      firstName: string;
      lastName: string;
      phoneNumber: string;
      address: string;
      country: string;
      email: string;
      createdAt: string;
      updatedAt: string;
      id: string;
    };
    createdAt: string;
    updatedAt: string;
    id: string;
  }[];

  appInfo: {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    address: string;
    city: string;
    province: string;
    sex: string;
    dateOfBirth: string;
    maritalStatus: string;
    citizenship: string;
    preferredLanguage: string;
    employmentStatus: string;
    loanAmountRequested: number;
    loanType: string;
    debtCircumstances: string;
    guarantor: {
      fullName: string;
      email: string;
      phoneNumber: string;
    };
    recommendationInfo: string;
    emailOptIn: boolean;
    status: string;
    officer: string;
    user: string;
  };
  setAppInfo: (appInfo) => void;
}
interface NumberFormatProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

/**
 * Formats phone number input
 */
function PhoneFormat(props: NumberFormatProps) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      format="(###)-###-####"
      placeholder="(___)-___-____"
      mask={["_", "_", "_", "_", "_", "_", "_", "_", "_", "_"]}
      isNumericString
    />
  );
}

const PersonalInformation: React.FC<PersonalInformationProps> = (props) => {
  const { appInfo, officers, setAppInfo } = props;
  const accessToken = localStorage.getItem("accessToken");
  const user = accessToken && jwtDecode<any>(accessToken).sub;
  const [assigned, setAssigned] = useState(appInfo?.officer);
  const [status, setStatus] = useState(appInfo?.status);
  const [editMode, setEditMode] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const [guarantorInfo, setInfo] = useState({
    email: appInfo?.guarantor.email,
    fullName: appInfo?.guarantor.fullName,
    phoneNumber: appInfo?.guarantor.phoneNumber
  });
  const [defaultInfo, setDefault] = useState({
    ...guarantorInfo
  });

  /**
   * sets initial values and default values
   */
  useEffect(() => {
    setInfo({
      email: appInfo?.guarantor.email,
      fullName: appInfo?.guarantor.fullName,
      phoneNumber: appInfo?.guarantor.phoneNumber
    });

    setDefault({
      email: appInfo?.guarantor.email,
      fullName: appInfo?.guarantor.fullName,
      phoneNumber: appInfo?.guarantor.phoneNumber
    });

    setAssigned(appInfo?.officer);
    setStatus(appInfo?.status);
  }, [appInfo]);

  /**
   * set guarantor info to updated values
   * @param e event
   */
  const onChangeGuarantorInfo = (e) => {
    setInfo({ ...guarantorInfo, [e.target.name]: e.target.value });
  };

  /**
   * updates assigned loan officer
   * @param id selected officer id
   */
  const handleAssignedTo = (id) => {
    apiApplications
      .patch(appInfo?.id, {
        applications: {
          officer: id
        }
      })
      .then((res) => {
        if (res.code == undefined) {
          setAppInfo(res);
        }
      })
      .catch((err) => {
        console.log(err.message);
      });
  };

  /**
   * patch call to applications to change status
   * @param status selected status
   */
  const onChangeStatus = (status) => {
    apiApplications
      .patch(appInfo?.id, {
        applications: {
          status: status
        }
      })
      .then((res) => {
        if (res.code == undefined) {
          setAppInfo(res);
        }
        else {
          setShowModal(true);
          setErrorMessage(res.message)
        }
      })
      .catch((err) => {
        console.log(err.message);
      });
  };

  /**
   * Toggle edit mode
   */
  const onEdit = () => {
    setEditMode(!editMode);
  };

  /**
   * Toggle cancel and reset guarantor info to original values
   * before edit
   */
  const onCancel = () => {
    setInfo({ ...defaultInfo });
    setEditMode(!editMode);
  };

  /**
   * api call to update guarantor information
   */
  const updateApplication = () => {
    let payload = {
      guarantor: guarantorInfo
    };

    apiApplications
      .patch(appInfo?.id, {
        applications: payload
      })
      .then((res) => {
        if (res.code == undefined) {
          setEditMode(!editMode);
          setDefault({ ...guarantorInfo });
        }
      });
  };

  return (
    <Card className="application-overview" style={{ width: "1065px" }}>
      <div id="application-overview-header">
        <CardHeader title={appInfo?.firstName + " " + appInfo?.lastName} />
        <div>
          {!editMode ? (
            <Button id="app-ovr-view" color="primary" style={{ color: "white" }} variant="contained" onClick={onEdit}>
              Edit
            </Button>
          ) : (
            <>
              <Button id="app-ovr-view" color="primary" style={{ color: "white" }} variant="contained" onClick={onCancel}>
                Cancel
              </Button>
              <Button
                id="app-ovr-view"
                color="primary"
                style={{ color: "white", marginLeft: 30 }}
                variant="contained"
                onClick={updateApplication}
              >
                Save
              </Button>
            </>
          )}
        </div>
      </div>

      <hr id="ao-line" />
      <CardContent>
        <Grid container direction="row" spacing={3} style={{ position: "relative", marginBottom: 30 }}>
          <Grid item>
            <Typography variant="body1" className="application-overview-label">
              <b>Assigned To</b>
            </Typography>
              <LoanOfficerDropdown officers={officers} value={assigned || ""} currentUser={user} onChange={handleAssignedTo} />
         
          </Grid>
          <Grid item>
            <Typography variant="body1" className="application-overview-label">
              <b>Status</b>
            </Typography>
            {status && <StatusDropdown value={status} application={appInfo} onChange={onChangeStatus} />}
          </Grid>
        </Grid>
        <Grid container>
          <Grid item container style={{ width: "500px", flexBasis: "auto" }}>
            <Grid item container direction="row" xs={8} style={{ justifyContent: "space-around" }}>
              <Grid item xs={4}>
                <Typography variant="body1" className="application-overview-label">
                  <b>Phone</b>
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography variant="body1" className="application-overview-label" data-testid="app-number">
                  {appInfo?.phoneNumber}
                </Typography>
              </Grid>
            </Grid>
            <Grid item container direction="row" xs={8} style={{ justifyContent: "space-around" }}>
              <Grid item xs={4}>
                <Typography variant="body1" className="application-overview-label">
                  <b>Email</b>
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography variant="body1" className="application-overview-label" data-testid="app-email">
                  {appInfo?.email}
                </Typography>
              </Grid>
            </Grid>
            <Grid item container direction="row" xs={8} style={{ justifyContent: "space-around" }}>
              <Grid item xs={4}>
                <Typography variant="body1" className="application-overview-label">
                  <b>Address</b>
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography variant="body1" className="application-overview-label" data-testid="app-address">
                  {appInfo?.address}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item container direction="column" spacing={1} style={{ width: "500px", flexBasis: "auto" }}>
            <Grid item container direction="row" style={{ justifyContent: "space-evenly" }}>
              <Grid item xs={4}>
                <Typography variant="body1" className="application-overview-label">
                  <b>Guarantor Name</b>
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <TextField
                  data-testid="guarantor-name"
                  name="fullName"
                  variant="outlined"
                  fullWidth
                  value={guarantorInfo.fullName}
                  onChange={onChangeGuarantorInfo}
                  disabled={!editMode}
                  size="small"
                  inputProps={{
                    classes: {
                      disabled: !editMode
                    }
                  }}
                />
              </Grid>
            </Grid>
            <Grid item container direction="row" style={{ justifyContent: "space-evenly" }}>
              <Grid item xs={4}>
                <Typography variant="body1" className="application-overview-label">
                  <b>Guarantor Phone</b>
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <TextField
                  data-testid="guarantor-number"
                  name="phoneNumber"
                  variant="outlined"
                  fullWidth
                  value={guarantorInfo.phoneNumber}
                  onChange={onChangeGuarantorInfo}
                  InputProps={{
                    inputComponent: PhoneFormat as any
                  }}
                  disabled={!editMode}
                  size="small"
                />
              </Grid>
            </Grid>
            <Grid item container direction="row" style={{ justifyContent: "space-evenly" }}>
              <Grid item xs={4}>
                <Typography variant="body1" className="application-overview-label">
                  <b>Guarantor Email</b>
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <TextField
                  data-testid="guarantor-email"
                  name="email"
                  variant="outlined"
                  fullWidth
                  value={guarantorInfo.email}
                  onChange={onChangeGuarantorInfo}
                  disabled={!editMode}
                  size="small"
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </CardContent>
      {showModal ? <Dialogue title={"Error Changing Status"} text={errorMessage} open={showModal} onClose={() => setShowModal(false)} /> : null}
    </Card>
  );
};

export default PersonalInformation;
