import * as React from "react";
import { useState, useEffect } from "react";
import {
  Grid,
  Card,
  CardHeader,
  CardContent,
  Typography,
  TextField,
  Checkbox,
  RadioGroup,
  Radio,
  FormControlLabel,
  Select,
  MenuItem,
  Button,
  Divider,
  withStyles,
  FormHelperText
} from "@material-ui/core";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { Formik } from "formik";
import * as Yup from "yup";
import NumberFormat from "react-number-format";
import "../../CreateApplication/CreateApplication.scss";
import LoanOfficerDropdown from "../../../common/LoanOfficerDropdown/LoanOfficerDropdown";
import { apiApplications } from "../../../../services/api/applications/apiApplications";
import MomentUtils from "@date-io/moment";

/**
 * Props of FieldTitle
 */
interface FieldTitleProps {
  text: string;
  required?: boolean;
  bold?: boolean;
}

/**
 * Smaller title for small fields
 */
const FieldTitle = ({ text, required = false, bold = false }: FieldTitleProps) => (
  <Typography
    style={{ marginBottom: 7, fontWeight: bold ? "bold" : 400 }}
    className={`field-title ${required ? "required" : ""}`}
  >
    {text}
  </Typography>
);

/**
 * FormControlLabel with height/margin changed
 */
const StyledFormControlLabel = withStyles({
  root: {
    height: 30,
    marginRight: 40
  }
})(FormControlLabel);

/**
 * Checkbox colored as design
 */
const ColoredCheckbox = withStyles({
  root: {
    "&$checked": {
      color: "#DA6E5D",
      "&$disabled": {
        color: "rgba(30, 105, 255, 0.38)"
      }
    }
  },
  checked: {},
  disabled: {}
})((props: any) => <Checkbox color="default" {...props} />);

/**
 * Radio button colored as design
 */
const ColoredRadio = withStyles({
  root: {
    "&$checked": {
      color: "#DA6E5D",
      "&$disabled": {
        color: "rgba(30, 105, 255, 0.38)"
      }
    }
  },
  checked: {},
  disabled: {}
})((props: any) => <Radio color="default" {...props} />);

/**
 * Props of StyledSelect
 */
interface StyledSelectProps {
  name: string;
  error?: boolean;
  maxWidth?: number;
  currentValue: string | undefined | null;
  selections: Array<string>;
  disabled?: boolean;
  maxItemShowed?: number;
  onChange: (event: React.ChangeEvent<{ value: unknown }>) => void;
  onBlur: (event: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
}

/**
 * A styled Select component with custom width
 * @param param0 StyledSelectProps
 */
const StyledSelect = ({
  name,
  error,
  currentValue,
  selections,
  disabled,
  maxItemShowed = 6,
  onChange,
  onBlur
}: StyledSelectProps) => {
  const itemHeight = 55;

  return (
    <Select
      fullWidth
      name={name}
      error={error}
      style={{ maxHeight: 56, padding: 0 }}
      disabled={disabled}
      value={currentValue}
      variant="outlined"
      onChange={onChange}
      onBlur={onBlur}
      MenuProps={{
        getContentAnchorEl: null,
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "left"
        },
        PaperProps: {
          style: {
            maxHeight: maxItemShowed * itemHeight,
            padding: 0
          }
        },
        MenuListProps: { disablePadding: true }
      }}
    >
      {selections.map((selection, index) => (
        <MenuItem
          style={{ height: itemHeight, borderBottom: "1px solid rgba(33, 33, 33, 0.08)" }}
          key={index}
          value={selection}
        >
          {selection}
        </MenuItem>
      ))}
    </Select>
  );
};

interface NumberFormatProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

function CurrencyFormat(props: NumberFormatProps) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      thousandSeparator
      isNumericString
      isAllowed={(values) => {
        const { formattedValue, floatValue } = values;
        return Boolean(formattedValue === "" || (floatValue && floatValue <= 1000000));
      }}
    />
  );
}

function PhoneFormat(props: NumberFormatProps) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      format="(###)-###-####"
      placeholder="(___)-___-____"
      mask={["_", "_", "_", "_", "_", "_", "_", "_", "_", "_"]}
      isNumericString
    />
  );
}

interface CheckFieldsLabelProps {
  label: string;
  error: boolean;
}

const CheckFieldsLabel = ({ label, error }: CheckFieldsLabelProps) => (
  <Typography color={error ? "error" : "textPrimary"} variant="body2">
    {label}
  </Typography>
);

interface ErrorTextProps {
  error: string | undefined;
}

const ErrorText = ({ error }: ErrorTextProps) => (
  <FormHelperText error={Boolean(error)} className={error ? "error-text" : "hidden"}>
    {error}
  </FormHelperText>
);

enum Acknowledgements {
  LOAN_PURPOSE = 0,
  MAX_LOAN = 1,
  REPAYMENT = 2,
  RESIDENCE = 3,
  NET_POSITIVE_INCOME = 4,
  GUARANTOR_CONSENT = 5
}

enum Services {
  CAREER_COACHING = 0,
  RESUME_CRITIQUE = 1,
  FINANCIAL_COACHING = 2,
  COMMUNITY_RESOURCES = 3
}

interface EditApplicationProps {
  /**
   * An optional prop that specifies if the page is for view only or for applying
   */
  officersList: {
    workload: number;
    user: {
      role: string;
      isEmailVerified: boolean;
      firstName: string;
      lastName: string;
      phoneNumber: string;
      address: string;
      country: string;
      email: string;
      createdAt: string;
      updatedAt: string;
      id: string;
    };
    createdAt: string;
    updatedAt: string;
    id: string;
  }[];
  viewOnly?: boolean;
  handleSubmitApplication?: Function;

  appInfo: {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    address: string;
    city: string;
    province: string;
    sex: string;
    dateOfBirth: string;
    maritalStatus: string;
    citizenship: string;
    preferredLanguage: string;
    employmentStatus: string;
    loanAmountRequested: number;
    loanType: string;
    debtCircumstances: string;
    postalCode: string;
    guarantor: {
      hasGuarantor: boolean;
      fullName: string;
      email: string;
      phoneNumber: string;
    };
    acknowledgements: {
      loanPurpose: boolean;
      maxLoan: boolean;
      residence: boolean;
      netPositiveIncome: boolean;
      guarantorConsent: boolean;
      repayment: boolean;
    };
    services: {
      careerCoaching: boolean;
      resumeCritique: boolean;
      financialCoaching: boolean;
      communityResources: boolean;
    };
    recommendationInfo: string;
    emailOptIn: boolean;
    status: string;
    officer: string;
    user: string;
  };
}

// TODO This may change based on the design
const maritalStatusSelections = ["Single", "Separated", "Married", "Widow/Widower", "Divorced", "Common Law"];

const sexSelections = ["Male", "Female", "X"];

const provinces = [
  "Alberta",
  "British Columbia",
  "Manitoba",
  "New Brunswick",
  "Newfoundland and Labrador",
  "Northwest Territories",
  "Nova Scotia",
  "Nunavut",
  "Ontario",
  "Prince Edward Island",
  "Quebec",
  "Saskatchewan",
  "Yukon"
];

const employmentStatusSelections = ["Employed Full Time", "Employed Part Time", "Seasonal/Contract", "Unemployed"];

const preferredLanguagesSelections = ["English", "French", "Arabic", "Other"];

const citizenshipSelections = ["Canadian", "Permanent Resident", "Other"];

/**
 * **Full** detail page of an application
 */
const EditApplication: React.FC<EditApplicationProps> = (props) => {
  const { appInfo, officersList } = props;
  const [assigned, setAssigned] = React.useState(appInfo?.officer);
  const [viewOnly, setViewOnly] = React.useState(true);
  const [applicationInformation, setApplicationInformation] = React.useState(appInfo);
  const [defaultApplicationInformation, setDefaultApplicationInformation] = React.useState(appInfo);

  useEffect(() => {
    setAssigned(appInfo?.officer);
    setApplicationInformation(appInfo); //Used to display information on UI, gets updated as
    setDefaultApplicationInformation(appInfo); //Only updated when patch route is called, updates applicationInformation when cancel is pressed
  }, [appInfo]);

  const changeViewOnly = () => {
    setViewOnly(!viewOnly);
  };

  const handleValueChange = (e) => {
    setApplicationInformation({ ...applicationInformation, [e.target.name]: e.target.value });
  };

  const handleAcknowledgementChange = (option) => {
    let acknowledgement = Acknowledgements[option];
    // parse enum key to camelcase string using regex replace
    acknowledgement = acknowledgement!
      .toLowerCase()
      .replace(/_.{1}/g, (c) => c.toUpperCase())
      .replace(/_/g, "");
    applicationInformation.acknowledgements[acknowledgement!] = !applicationInformation.acknowledgements[acknowledgement!];
  };

  const handleServiceChange = (option) => {
    let service = Services[option];
    // parse enum key to camelcase string using regex replace
    service = service!
      .toLowerCase()
      .replace(/_.{1}/g, (c) => c.toUpperCase())
      .replace(/_/g, "");
    applicationInformation.services[service!] = !applicationInformation.services[service!];
  };

  const handleDateChange = (e) => {
    applicationInformation.dateOfBirth = e._d;
  };

  const handleAssignedTo = (e) => {
    setAssigned(e);
  };

  const updateApplication = () => {
    let payload = {
      firstName: applicationInformation.firstName,
      lastName: applicationInformation.lastName,
      email: applicationInformation.email,
      phoneNumber: applicationInformation.phoneNumber,
      address: applicationInformation.address,
      city: applicationInformation.city,
      province: applicationInformation.province,
      sex: applicationInformation.sex,
      dateOfBirth: applicationInformation.dateOfBirth,
      maritalStatus: applicationInformation.maritalStatus,
      citizenship: applicationInformation.citizenship,
      employmentStatus: applicationInformation.employmentStatus,
      loanAmountRequested: applicationInformation.loanAmountRequested,
      loanType: applicationInformation.loanType,
      debtCircumstances: applicationInformation.debtCircumstances,
      postalCode: applicationInformation.postalCode,
      guarantor: {
        hasGuarantor: applicationInformation.guarantor.hasGuarantor,
        fullName: applicationInformation.guarantor.fullName,
        email: applicationInformation.guarantor.email,
        phoneNumber: applicationInformation.guarantor.phoneNumber
      },
      officer: assigned,
      acknowledgements: {
        loanPurpose: applicationInformation.acknowledgements.loanPurpose,
        maxLoan: applicationInformation.acknowledgements.maxLoan,
        repayment: applicationInformation.acknowledgements.repayment,
        residence: applicationInformation.acknowledgements.residence,
        netPositiveIncome: applicationInformation.acknowledgements.netPositiveIncome,
        guarantorConsent: applicationInformation.acknowledgements.guarantorConsent
      },
      services: {
        careerCoaching: applicationInformation.services.careerCoaching,
        resumeCritique: applicationInformation.services.resumeCritique,
        financialCoaching: applicationInformation.services.financialCoaching,
        communityResources: applicationInformation.services.communityResources
      },
      recommendationInfo: applicationInformation.recommendationInfo
    };

    apiApplications
      .patch(appInfo?.id, {
        applications: payload
      })
      .then((res) => {
        if (res.code == undefined) {
          setDefaultApplicationInformation(applicationInformation);
          setViewOnly(!viewOnly);
        }
      });
  };

  const cancelUpdate = () => {
    setApplicationInformation(defaultApplicationInformation);
    setAssigned(defaultApplicationInformation.officer);
    setViewOnly(!viewOnly);
  };

  if (!applicationInformation) {
    return null;
  }
  return (
    <Formik
      onSubmit={(values, submitProps) => {}}
      enableReinitialize
      validateOnBlur={true}
      validateOnChange={false}
      initialValues={applicationInformation}
      validationSchema={Yup.object().shape({
        firstName: Yup.string().required("First name is required"),
        lastName: Yup.string().required("Last name is required"),
        email: Yup.string().email("Email must be valid email").required("Email is required"),
        phoneNumber: Yup.string()
          .matches(
            /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
            "Phone number must be a valid phone number"
          )
          .required("Phone number is required"),
        maritalStatus: Yup.string().required("Marital status is required"),
        employmentStatus: Yup.string().required("Employment status is required"),
        preferredLanguage: Yup.string().required("Preferred language is required"),
        address: Yup.string().required("Address is required"),
        city: Yup.string().required("City is required"),
        province: Yup.string().required("Province is required"),
        postalCode: Yup.string().matches(
          /^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ -]?\d[ABCEGHJ-NPRSTV-Z]\d$/i,
          "Postal code must be a valid Canadian postal code"
        ),
        sex: Yup.string().required("Sex is required").oneOf(sexSelections, "Choose a valid sex"),
        dateOfBirth: Yup.date().nullable().typeError("Invalid date entered").required("Date of birth is required"),
        citizenship: Yup.string().required("Citizenship is required"),
        loanAmountRequested: Yup.number().nullable().required("Loan amount requested is required"),
        loanType: Yup.string().nullable().required("Loan type is required"),
        debtCircumstances: Yup.string().required("Debt circumstances is required"),
        hasGuarantor: Yup.boolean().nullable().required("Guarantor is required"),
        recommendationInfo: Yup.string().required("Recommendation info is required"),
        emailOptIn: Yup.boolean().required("Mail opt-in selection is required")
      })}
    >
      {(props) => {
        const { values, touched, errors, setFieldValue, handleChange, handleBlur, handleSubmit } = props;

        return (
          <MuiPickersUtilsProvider utils={MomentUtils}>
            <form data-testid="form" className="create-application-form" onSubmit={handleSubmit}>
              <Card className="form-card">
                <Grid container justify="space-between">
                  <CardHeader title="Personal Information" />
                  <div>
                    {viewOnly ? (
                      <Button
                        id="app-ovr-view"
                        color="primary"
                        style={{ color: "white" }}
                        variant="contained"
                        onClick={changeViewOnly}
                      >
                        Edit
                      </Button>
                    ) : (
                      <>
                        <Button
                          id="app-ovr-view"
                          color="primary"
                          style={{ color: "white", marginRight: "30px" }}
                          variant="contained"
                          onClick={cancelUpdate}
                        >
                          Cancel
                        </Button>
                        <Button
                          id="app-ovr-view"
                          color="primary"
                          style={{ color: "white" }}
                          variant="contained"
                          onClick={updateApplication}
                        >
                          Save
                        </Button>
                      </>
                    )}
                  </div>
                </Grid>

                <Divider variant="middle" className="divider-top" />

                <CardContent>
                  <Grid container spacing={4}>
                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="First Name" required />
                        <TextField
                          data-testid="firstName"
                          name="firstName"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.firstName)}
                          onChange={handleValueChange}
                          onBlur={handleBlur}
                          value={applicationInformation.firstName}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.firstName} />
                      </Grid>

                      <Grid item xs={4}>
                        <FieldTitle text="Last Name" required />
                        <TextField
                          data-testid="lastName"
                          name="lastName"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.lastName)}
                          onChange={handleValueChange}
                          onBlur={handleBlur}
                          value={applicationInformation.lastName}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.lastName} />
                      </Grid>

                      <Grid item xs={3}>
                        <FieldTitle text="Assigned" required />
                        <LoanOfficerDropdown
                          officers={officersList}
                          value={assigned}
                          onChange={handleAssignedTo}
                          disabled={viewOnly}
                        />
                      </Grid>
                    </Grid>

                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="Email" required />
                        <TextField
                          data-testid="email"
                          name="email"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.email)}
                          onChange={handleValueChange}
                          onBlur={handleBlur}
                          value={applicationInformation.email}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.email} />
                      </Grid>

                      <Grid item xs={4}>
                        <FieldTitle text="Phone Number" required />
                        <TextField
                          data-testid="phoneNumber"
                          type="tel"
                          name="phoneNumber"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.phoneNumber)}
                          onChange={handleValueChange}
                          onBlur={handleBlur}
                          InputProps={{
                            inputComponent: PhoneFormat as any
                          }}
                          value={applicationInformation.phoneNumber.match(/\d/g)}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.phoneNumber} />
                      </Grid>

                      <Grid item xs={3}>
                        <FieldTitle text="Preferred Language" required />
                        <StyledSelect
                          name="preferredLanguage"
                          error={Boolean(errors.preferredLanguage)}
                          onChange={handleValueChange}
                          onBlur={handleBlur}
                          currentValue={applicationInformation?.preferredLanguage}
                          selections={preferredLanguagesSelections}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.preferredLanguage} />
                      </Grid>
                    </Grid>

                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="Marital Status" required />
                        <StyledSelect
                          name="maritalStatus"
                          error={Boolean(errors.maritalStatus)}
                          onChange={handleValueChange}
                          onBlur={handleBlur}
                          currentValue={applicationInformation.maritalStatus}
                          selections={maritalStatusSelections}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.maritalStatus} />
                      </Grid>

                      <Grid item xs={4}>
                        <FieldTitle text="Citizenship" required />
                        <StyledSelect
                          name="citizenship"
                          error={Boolean(errors.citizenship)}
                          onChange={handleValueChange}
                          onBlur={handleBlur}
                          currentValue={applicationInformation.citizenship}
                          selections={citizenshipSelections}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.citizenship} />
                      </Grid>

                      <Grid item xs={3}>
                        <FieldTitle text="Sex" required />
                        <StyledSelect
                          name="sex"
                          error={Boolean(errors.sex)}
                          onChange={handleValueChange}
                          onBlur={handleBlur}
                          currentValue={applicationInformation.sex}
                          selections={sexSelections}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.sex} />
                        <Typography className="field-subtitle" variant="body2" color="textSecondary" display="inline">
                          Please enter the sex that is specified on your legal documents.
                        </Typography>
                      </Grid>
                    </Grid>

                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="Employment Status" required />
                        <StyledSelect
                          name="employmentStatus"
                          error={Boolean(errors.employmentStatus)}
                          onChange={handleValueChange}
                          onBlur={handleBlur}
                          currentValue={applicationInformation.employmentStatus}
                          selections={employmentStatusSelections}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.employmentStatus} />
                      </Grid>

                      <Grid item xs={4}>
                        <FieldTitle text="Date of Birth" required />
                        {viewOnly ? (
                          <KeyboardDatePicker
                            disabled
                            id="dateOfBirth"
                            inputVariant="outlined"
                            variant="inline"
                            disableToolbar
                            fullWidth
                            format="yyyy/MM/DD"
                            value={applicationInformation.dateOfBirth}
                            error={Boolean(errors.dateOfBirth)}
                            helperText=""
                            onChange={(date) => {
                              handleDateChange(date);
                              setFieldValue("dateOfBirth", date);
                            }}
                          />
                        ) : (
                          <KeyboardDatePicker
                            id="dateOfBirth"
                            inputVariant="outlined"
                            variant="inline"
                            disableToolbar
                            fullWidth
                            format="yyyy/MM/DD"
                            value={applicationInformation.dateOfBirth}
                            error={Boolean(errors.dateOfBirth)}
                            helperText=""
                            onChange={(date) => {
                              handleDateChange(date);
                              setFieldValue("dateOfBirth", date);
                            }}
                          />
                        )}

                        <ErrorText error={errors.dateOfBirth} />
                      </Grid>
                      <Grid item xs={3} />
                    </Grid>
                  </Grid>
                </CardContent>

                <Divider variant="middle" className="divider-mid" />

                <CardContent>
                  <Grid container spacing={4}>
                    <Grid item xs={12}>
                      <FieldTitle text="Address" required />
                      <TextField
                        data-testid="address"
                        name="address"
                        variant="outlined"
                        fullWidth
                        error={Boolean(errors.address)}
                        onChange={handleValueChange}
                        onBlur={handleBlur}
                        defaultValue={applicationInformation.address}
                        disabled={viewOnly}
                      />
                      <ErrorText error={errors.address} />
                    </Grid>

                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="City" required />
                        <TextField
                          data-testid="city"
                          name="city"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.city)}
                          onChange={handleValueChange}
                          onBlur={handleBlur}
                          value={applicationInformation.city}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.city} />
                      </Grid>

                      <Grid item xs={4}>
                        <FieldTitle text="Province" required />
                        <StyledSelect
                          name="province"
                          error={Boolean(errors.province)}
                          onChange={handleValueChange}
                          onBlur={handleBlur}
                          maxWidth={700}
                          currentValue={applicationInformation.province}
                          selections={provinces}
                          disabled={viewOnly}
                          maxItemShowed={5}
                        />
                        <ErrorText error={errors.province} />
                      </Grid>

                      <Grid item xs={3}>
                        <FieldTitle text="Postal Code" />
                        <TextField
                          data-testid="postalCode"
                          name="postalCode"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.postalCode)}
                          onChange={handleValueChange}
                          onBlur={handleBlur}
                          value={applicationInformation.postalCode}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.postalCode} />
                      </Grid>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>

              <Card className="form-card">
                <CardHeader title="Loan Request" />
                <Divider variant="middle" className="divider-top" />

                <CardContent>
                  <Grid container spacing={4}>
                    <Grid item xs={10}>
                      <FieldTitle text="Loan Amount Requested" required bold />
                      <Typography className="field-subtitle" variant="body2" color="textSecondary">
                        Please note the maximum amount is $4,000
                      </Typography>
                      <TextField
                        data-testid="loanAmountRequested"
                        name="loanAmountRequested"
                        variant="outlined"
                        fullWidth
                        error={Boolean(errors.loanAmountRequested)}
                        onChange={handleValueChange}
                        onBlur={handleBlur}
                        InputProps={{
                          inputComponent: CurrencyFormat as any
                        }}
                        value={applicationInformation.loanAmountRequested ? applicationInformation.loanAmountRequested : ""}
                        disabled={viewOnly}
                      />
                      <ErrorText error={errors.loanAmountRequested} />
                    </Grid>

                    <Grid item xs={10}>
                      <FieldTitle text="Loan Type" required bold />
                      <RadioGroup
                        name="loanType"
                        row
                        onChange={handleValueChange}
                        defaultValue={applicationInformation.loanType}
                      >
                        <StyledFormControlLabel
                          value="Credit"
                          disabled={viewOnly}
                          control={<ColoredRadio />}
                          label={<CheckFieldsLabel label="Credit Card" error={Boolean(errors.loanType)} />}
                        />
                        <StyledFormControlLabel
                          value="Emergency"
                          disabled={viewOnly}
                          control={<ColoredRadio />}
                          label={<CheckFieldsLabel label="Emergency" error={Boolean(errors.loanType)} />}
                        />
                        <StyledFormControlLabel
                          value="Other"
                          disabled={viewOnly}
                          control={<ColoredRadio />}
                          label={<CheckFieldsLabel label="Other" error={Boolean(errors.loanType)} />}
                        />
                      </RadioGroup>
                      <ErrorText error={errors.loanType} />
                    </Grid>

                    <Grid item xs={10}>
                      <FieldTitle text="Debt Circumstances" required bold />
                      <Typography className="field-subtitle" variant="body2" color="textSecondary">
                        Please describe the circumstances of your debt. Why are you in debt?
                      </Typography>
                      <TextField
                        data-testid="debtCircumstances"
                        name="debtCircumstances"
                        variant="outlined"
                        fullWidth
                        error={Boolean(errors.debtCircumstances)}
                        onChange={handleValueChange}
                        onBlur={handleBlur}
                        rows={3}
                        multiline
                        value={applicationInformation.debtCircumstances}
                        disabled={viewOnly}
                      />
                      <ErrorText error={errors.debtCircumstances} />
                    </Grid>

                    <Grid item xs={10}>
                      <FieldTitle text="Do you have a guarantor?" required bold />
                      <Typography className="field-subtitle" variant="body2" color="textSecondary">
                        A guarantor is responsible for paying your loan in the unfortunate scenario that you cannot make your
                        payments. Please note that all loans require the consent of a guarantor.
                      </Typography>
                      <RadioGroup row defaultValue={applicationInformation.guarantor?.hasGuarantor ? "Yes" : "No"}>
                        <StyledFormControlLabel
                          name="guarantor"
                          value="Yes"
                          disabled={viewOnly}
                          control={
                            <ColoredRadio
                              onChange={() => {
                                setFieldValue("guarantor", false);
                                applicationInformation.guarantor.hasGuarantor = true;
                              }}
                            />
                          }
                          label={<CheckFieldsLabel label="Yes" error={Boolean(errors.guarantor)} />}
                        />
                        <StyledFormControlLabel
                          name="guarantor"
                          value="No"
                          disabled={viewOnly}
                          control={
                            <ColoredRadio
                              onChange={() => {
                                setFieldValue("guarantor", false);
                                applicationInformation.guarantor.hasGuarantor = false;
                              }}
                            />
                          }
                          label={<CheckFieldsLabel label="No" error={!!touched.guarantor && !!errors.guarantor} />}
                        />
                      </RadioGroup>
                      {/* <ErrorText error={errors.hasGuarantor} /> */}
                    </Grid>

                    <Grid item xs={10}>
                      <FieldTitle text="How did you hear about Beneficent?" required bold />
                      <TextField
                        data-testid="recommendationInfo"
                        name="recommendationInfo"
                        variant="outlined"
                        fullWidth
                        error={Boolean(errors.recommendationInfo)}
                        onChange={handleValueChange}
                        onBlur={handleBlur}
                        value={values.recommendationInfo}
                        disabled={viewOnly}
                      />
                      <ErrorText error={errors.recommendationInfo} />
                    </Grid>

                    <Grid item>
                      <FieldTitle text="Are you interested in any of the following Beneficent services?" bold />

                      <Grid container direction="column">
                        <StyledFormControlLabel
                          value={0}
                          name={`services[${Services.CAREER_COACHING}]`}
                          control={
                            <ColoredCheckbox
                              defaultChecked={applicationInformation.services.careerCoaching}
                              disabled={viewOnly}
                              onChange={() => {
                                handleServiceChange(0);
                              }}
                            />
                          }
                          label={<CheckFieldsLabel label="Career Coaching" error={Boolean(errors.services?.[0])} />}
                          classes={{ root: "services-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={1}
                          name={`services[${Services.RESUME_CRITIQUE}]`}
                          control={
                            <ColoredCheckbox
                              defaultChecked={applicationInformation.services.resumeCritique}
                              disabled={viewOnly}
                              onChange={() => {
                                handleServiceChange(1);
                              }}
                            />
                          }
                          label={<CheckFieldsLabel label="Resume Critique" error={Boolean(errors.services?.[1])} />}
                          classes={{ root: "services-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={2}
                          name={`services[${Services.FINANCIAL_COACHING}]`}
                          control={
                            <ColoredCheckbox
                              defaultChecked={applicationInformation.services.financialCoaching}
                              disabled={viewOnly}
                              onChange={() => {
                                handleServiceChange(2);
                              }}
                            />
                          }
                          label={<CheckFieldsLabel label="Financial Coaching" error={Boolean(errors.services?.[2])} />}
                          classes={{ root: "services-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={3}
                          name={`services[${Services.COMMUNITY_RESOURCES}]`}
                          control={
                            <ColoredCheckbox
                              defaultChecked={applicationInformation.services.communityResources}
                              disabled={viewOnly}
                              onChange={() => {
                                handleServiceChange(3);
                              }}
                            />
                          }
                          label={
                            <CheckFieldsLabel label="Community Resources" error={Boolean(errors.acknowledgements?.[3])} />
                          }
                          classes={{ root: "services-StyledFormControlLabel" }}
                        />
                      </Grid>
                    </Grid>

                    <Grid item>
                      <FieldTitle text="Acknowledgements" bold />
                      <Typography className="field-subtitle" variant="body2" color="textSecondary">
                        I acknowledge that the following criteria must be met in order to be eligible for a loan with
                        Beneficent:
                      </Typography>

                      <Grid container direction="column">
                        <StyledFormControlLabel
                          value={0}
                          name={`acknowledgements[${Acknowledgements.LOAN_PURPOSE}]`}
                          control={
                            <ColoredCheckbox
                              defaultChecked={applicationInformation.acknowledgements.loanPurpose}
                              disabled={viewOnly}
                              onChange={() => {
                                handleAcknowledgementChange(0);
                              }}
                            />
                          }
                          label={
                            <CheckFieldsLabel
                              label="Loan purpose is for credit card, emergency,
                          or similar high interest bearing debt"
                              error={Boolean(errors.acknowledgements?.[0])}
                            />
                          }
                          classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={1}
                          name={`acknowledgements[${Acknowledgements.MAX_LOAN}]`}
                          control={
                            <ColoredCheckbox
                              defaultChecked={applicationInformation.acknowledgements.maxLoan}
                              disabled={viewOnly}
                              onChange={() => {
                                handleAcknowledgementChange(1);
                              }}
                            />
                          }
                          label={
                            <CheckFieldsLabel
                              label="Maximum loan value of $4,000"
                              error={Boolean(errors.acknowledgements?.[1])}
                            />
                          }
                          classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={2}
                          name={`acknowledgements[${Acknowledgements.REPAYMENT}]`}
                          control={
                            <ColoredCheckbox
                              defaultChecked={applicationInformation.acknowledgements.repayment}
                              disabled={viewOnly}
                              onChange={() => {
                                handleAcknowledgementChange(2);
                              }}
                            />
                          }
                          label={
                            <CheckFieldsLabel
                              label="Repayment within 12 months"
                              error={Boolean(errors.acknowledgements?.[2])}
                            />
                          }
                          classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={3}
                          name={`acknowledgements[${Acknowledgements.RESIDENCE}]`}
                          control={
                            <ColoredCheckbox
                              defaultChecked={applicationInformation.acknowledgements.residence}
                              disabled={viewOnly}
                              onChange={() => {
                                handleAcknowledgementChange(3);
                              }}
                            />
                          }
                          label={
                            <CheckFieldsLabel label="Residence in Canada" error={Boolean(errors.acknowledgements?.[3])} />
                          }
                          classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={4}
                          name={`acknowledgements[${Acknowledgements.NET_POSITIVE_INCOME}]`}
                          control={
                            <ColoredCheckbox
                              defaultChecked={applicationInformation.acknowledgements.netPositiveIncome}
                              disabled={viewOnly}
                              onChange={() => {
                                handleAcknowledgementChange(4);
                              }}
                            />
                          }
                          label={
                            <CheckFieldsLabel label="Positive net income" error={Boolean(errors.acknowledgements?.[4])} />
                          }
                          classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                        />
                        <StyledFormControlLabel
                          value={5}
                          name={`acknowledgements[${Acknowledgements.GUARANTOR_CONSENT}]`}
                          control={
                            <ColoredCheckbox
                              defaultChecked={applicationInformation.acknowledgements.guarantorConsent}
                              disabled={viewOnly}
                              onChange={() => {
                                handleAcknowledgementChange(5);
                              }}
                            />
                          }
                          label={
                            <CheckFieldsLabel label="Guarantor consent" error={Boolean(errors.acknowledgements?.[5])} />
                          }
                          classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                        />
                      </Grid>
                      <ErrorText error={errors.acknowledgements ? "All acknowledgements must be checked" : undefined} />
                    </Grid>

                    <Grid item container direction="column">
                      <FieldTitle text="Email Opt-In" bold />
                      <StyledFormControlLabel
                        name="mailOptIn"
                        control={
                          <ColoredCheckbox
                            defaultChecked={applicationInformation.emailOptIn}
                            disabled={viewOnly}
                            onChange={() => {
                              applicationInformation.emailOptIn = !applicationInformation.emailOptIn;
                            }}
                          />
                        }
                        label={
                          <CheckFieldsLabel label="Consent to receive marketing emails" error={Boolean(errors.emailOptIn)} />
                        }
                      />
                      <ErrorText error={errors.emailOptIn} />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </form>
          </MuiPickersUtilsProvider>
        );
      }}
    </Formik>
  );
};

export default EditApplication;
