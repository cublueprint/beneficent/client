import * as React from "react";
import {
  Button,
  Grid,
  Card,
  CardHeader,
  CardContent,
  Typography,
  Checkbox,
  withStyles,
  FormControlLabel
} from "@material-ui/core";
import { Link as RouterLink } from "react-router-dom";
import routes from "../../../../routes";

import "./ApplicationOverview.scss";

/**
 * Checkbox colored as design
 */
const ColoredCheckbox = withStyles({
  root: {
    "&$checked": {
      color: "#DA6E5D",
      "&$disabled": {
        color: "rgba(30, 105, 255, 0.38)"
      }
    }
  },
  checked: {},
  disabled: {}
})((props: any) => <Checkbox color="default" {...props} />);

/**
 * FormControlLabel with height/margin changed
 */
const StyledFormControlLabel = withStyles({
  root: {
    height: 30,
    marginRight: 40
  }
})(FormControlLabel);

interface CheckFieldsLabelProps {
  label: string;
  error: boolean;
}

const CheckFieldsLabel = ({ label, error }: CheckFieldsLabelProps) => (
  <Typography color={error ? "error" : "textPrimary"} variant="body2">
    {label}
  </Typography>
);

const ApplicationOverview = (props) => {
  const { appInfo } = props;
  return (
    <Card className="application-overview" style={{ width: "1065px" }}>
      <div id="application-overview-header">
        <CardHeader title="Application Overview">Application Overview</CardHeader>
        <Button
          id="app-ovr-view"
          color="primary"
          style={{ color: "white" }}
          variant="contained"
          component={RouterLink}
          to={`${routes.casesApplications.path}/${appInfo?.id}/edit`}
        >
          View
        </Button>
      </div>

      <hr id="ao-line" />
      <CardContent>
        <Grid container direction="row" spacing={1} style={{ position: "relative" }}>
          <Grid item container direction="column" spacing={5} xs={6} style={{ width: "303px", flexBasis: "auto" }}>
            <Grid item>
              <Typography variant="body1" className="application-overview-label">
                <b>Citizenship</b>
              </Typography>
              <Typography variant="body1" className="application-overview-value">
                {appInfo?.citizenship}
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="body1" className="application-overview-label">
                <b>Employement Status</b>
              </Typography>
              <Typography variant="body1" className="application-overview-value">
                {appInfo?.employmentStatus}
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="body1" className="application-overview-label">
                <b>Amount Requested</b>
              </Typography>
              <Typography variant="body1" className="application-overview-value">
                ${appInfo?.loanAmountRequested}
              </Typography>
            </Grid>
          </Grid>
          <Grid item container direction="column" spacing={5} xs={6} style={{ width: "225px", flexBasis: "auto" }}>
            <Grid item>
              <Typography variant="body1" className="application-overview-label">
                <b>Loan Type</b>
              </Typography>
              <Typography variant="body1" className="application-overview-value">
                {appInfo?.loanType}
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="body1" className="application-overview-label">
                <b>Guarantor</b>
              </Typography>
              <Typography variant="body1" className="application-overview-value">
                {appInfo?.guarantor.hasGuarantor ? "Yes" : "No"}
              </Typography>
            </Grid>
          </Grid>
          <Grid
            item
            container
            direction="column"
            spacing={5}
            xs={6}
            id="application-overview-acknowledgements"
            style={{ width: "650px", flexBasis: "auto" }}
          >
            <Grid item container direction="column" style={{ width: "650px" }}>
              <Typography variant="body1" className="citizenship-label">
                <b>Acknowledgments</b>
              </Typography>
              {appInfo?.acknowledgements && (
                <StyledFormControlLabel
                  control={<ColoredCheckbox checked={appInfo?.acknowledgements.loanPurpose} disabled={true} />}
                  label={
                    <CheckFieldsLabel
                      label="Loan purpose is for credit card, emergency,
                          or similar high interest bearing debt"
                      error={false}
                    />
                  }
                  classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                />
              )}
              {appInfo?.acknowledgements && (
                <StyledFormControlLabel
                  control={<ColoredCheckbox checked={appInfo?.acknowledgements.maxLoan} disabled={true} />}
                  label={<CheckFieldsLabel label="Maximum loan value of $4,000" error={false} />}
                  classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                />
              )}
              {appInfo?.acknowledgements && (
                <StyledFormControlLabel
                  control={<ColoredCheckbox checked={appInfo?.acknowledgements.repayment} disabled={true} />}
                  label={<CheckFieldsLabel label="Repayment within 12 months" error={false} />}
                  classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                />
              )}
              {appInfo?.acknowledgements && (
                <StyledFormControlLabel
                  control={<ColoredCheckbox checked={appInfo?.acknowledgements.residence} disabled={true} />}
                  label={<CheckFieldsLabel label="Residence in Canada" error={false} />}
                  classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                />
              )}
              {appInfo?.acknowledgements && (
                <StyledFormControlLabel
                  control={<ColoredCheckbox checked={appInfo?.acknowledgements.netPositiveIncome} disabled={true} />}
                  label={<CheckFieldsLabel label="Positive net income" error={false} />}
                  classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                />
              )}
              {appInfo?.acknowledgements && (
                <StyledFormControlLabel
                  control={<ColoredCheckbox checked={appInfo?.acknowledgements.guarantorConsent} disabled={true} />}
                  label={<CheckFieldsLabel label="Guarantor consent" error={false} />}
                  classes={{ root: "acknowledgement-StyledFormControlLabel" }}
                />
              )}
            </Grid>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default ApplicationOverview;
