import React, { forwardRef, useImperativeHandle, useRef } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import {
  InputAdornment,
  makeStyles,
  Grid,
  TextField,
  Card,
  CardHeader,
  CardContent,
  Typography,
  Divider,
  Button
} from "@material-ui/core";
import NumberFormat from "react-number-format";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import moment from "moment";

import "./CreateEditContract.scss";
import { FormikSubmitObject } from "../../../../../ApplicationsLayout";
import { apiApplications } from "../../../../../../../services/api/applications/apiApplications";
import jwtDecode from "jwt-decode";
import { useHistory, useLocation, useParams } from "react-router";
import { apiContracts } from "../../../../../../../services/api/contracts/apiContracts";
import { apiOfficers } from "../../../../../../../services/api/officers/apiOfficers";
import Dialogue from "../../../../../../common/Dialogue/Dialogue";

interface NumberFormatProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

const CurrencyFormat = (props: NumberFormatProps) => {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      thousandSeparator
      isNumericString
      isAllowed={(values) => {
        const { formattedValue, floatValue } = values;
        return Boolean(formattedValue === "" || (floatValue && floatValue <= 99999));
      }}
    />
  );
};

const errorUseStyles = makeStyles((theme) => ({
  root: {
    "&$error": {
      top: 55,
      position: "absolute"
    }
  },
  error: {}
}));

interface FormikValuesType {
  loanAmount: number | undefined;
  contractDate: Date | null;
  monthlyInstallment: number | undefined;
  finalInstallment: number | undefined;
  firstInstallmentDue: Date | null;
  lastInstallmentDue: Date | null;
}

interface CreateEditContractProps {}

const CreateEditContract = forwardRef<FormikSubmitObject, CreateEditContractProps>((_, ref) => {
  const token = localStorage.getItem("accessToken");
  const errorTextStyles = errorUseStyles();
  const initialValues: FormikValuesType = {
    loanAmount: undefined,
    contractDate: null,
    monthlyInstallment: undefined,
    finalInstallment: undefined,
    firstInstallmentDue: null,
    lastInstallmentDue: null
  };
  const formikRef = useRef<any>(null);

  useImperativeHandle(
    ref,
    () => ({
      handleSubmit: () => formikRef.current?.handleSubmit()
    }),
    []
  );

  const guarantorRef = useRef<string>("");
  const { id } = useParams<{ id: string }>();
  const history = useHistory();
  const location = useLocation();
  const [officers, setOfficers] = React.useState([]);
  const [currOfficer, setCurrOfficer] = React.useState("");
  const [confirmContractLengthModal, setConfirmContractLengthModal] = React.useState(false);
  const user = token && jwtDecode<any>(token).sub;

  const fetchOfficers = React.useCallback(() => apiOfficers.getAll({ officers: { populate: "user" } }), [token]);

  React.useEffect(() => {
    if (token) {
      apiApplications.getSingle(id).then((app) => {
        setCurrOfficer(app.officer);
        guarantorRef.current = app.guarantor.fullName;
      });
      fetchOfficers().then((res) => {
        setOfficers(res.results);
      });
    }
  }, [fetchOfficers, token, id]);

  const calcContractLength = (date1, date2) => {
    const secondsInDay = 86400000;
    return (date2._d.getTime() - date1._d.getTime()) / secondsInDay;
  };

  return (
    <Formik
      innerRef={formikRef}
      initialValues={initialValues}
      onSubmit={(values, { setSubmitting }) => {
        const convertDate = (date) => moment(date).format("yyyy/MM/DD");
        const formData = {
          approvedLoanAmount: values.loanAmount!,
          firstPaymentDue: convertDate(values.firstInstallmentDue!),
          finalPaymentDue: convertDate(values.lastInstallmentDue!),
          monthlyPayment: values.monthlyInstallment!,
          finalPayment: values.finalInstallment!,
          status: "Draft",
          contractStartDate: convertDate(values.contractDate!),
          application: id,
          createdBy: user
        };

        if (token) {
          apiContracts.post({ contracts: formData }).then(() => {
            history.push(location.pathname.split("/").slice(0, -1).join("/"));
          });
        }
      }}
      validationSchema={Yup.object().shape({
        loanAmount: Yup.number()
          .typeError("Loan amount is required")
          .test("Check empty input", "Loan amount is required", (value) => value !== undefined)
          .test("Check amount is positive", "The amount must be greater than 0", (value) => value! > 0),
        contractDate: Yup.date().nullable().typeError("Please enter a valid date").required("Contract date is required"),
        monthlyInstallment: Yup.number()
          .typeError("Monthly installment is required")
          .test("Check empty input", "Monthly installment is required", (value) => value !== undefined)
          .test(
            "Check monthly installment is positive",
            "Monthly installment must be greater than 0",
            (value) => value! > 0
          ),
        finalInstallment: Yup.number().test(
          "Check final installment is positive",
          "The final installment must be greater than 0",
          (value) => !value || value! > 0
        ),
        firstInstallmentDue: Yup.date()
          .nullable()
          .typeError("Please enter a valid date")
          .required("First installment due date is required"),
        lastInstallmentDue: Yup.date()
          .nullable()
          .typeError("Please enter a valid date")
          .required("Last installment due date is required")
      })}
    >
      {(props) => {
        const { values, touched, errors, handleSubmit, setFieldValue } = props;
        return (
          <form data-testid="contract-form" onSubmit={handleSubmit}>
            <Card className="create-edit-contract-card">
              <CardHeader title="Loan Information" />
              <Divider />

              {confirmContractLengthModal ? (
                <Dialogue
                  open={confirmContractLengthModal}
                  title="Warning"
                  text={`The contract date and first installment due date are ${calcContractLength(
                    values.contractDate,
                    values.firstInstallmentDue
                  )} day(s) apart. \nThis will set the payment cycle to be every ${calcContractLength(
                    values.contractDate,
                    values.firstInstallmentDue
                  )} day(s). \nAre you sure you want to proceed?`}
                  hasActions
                  onClose={() => {
                    setConfirmContractLengthModal(false);
                  }}
                  onConfirm={() => {
                    setConfirmContractLengthModal(false);
                    handleSubmit();
                  }}
                />
              ) : null}

              <CardContent>
                <MuiPickersUtilsProvider utils={MomentUtils}>
                  <Grid container direction="row" spacing={5} style={{ width: "850px", minWidth: "1000px" }}>
                    <Grid item container direction="column" spacing={5} xs={4}>
                      <Grid item>
                        <Typography variant="body2" className="field-label">
                          Approved Loan Amount
                        </Typography>
                        <TextField
                          inputProps={{ "data-testid": "loan-amount" }}
                          name="loanAmount"
                          variant="outlined"
                          fullWidth
                          value={values.loanAmount}
                          onChange={(value) => setFieldValue("loanAmount", parseInt(value.target.value))}
                          error={!!(errors.loanAmount && touched.loanAmount)}
                          helperText={errors.loanAmount && touched.loanAmount && errors.loanAmount}
                          FormHelperTextProps={{ classes: errorTextStyles }}
                          InputProps={{
                            startAdornment: <InputAdornment position="start">$</InputAdornment>,
                            inputComponent: CurrencyFormat as any
                          }}
                        />
                      </Grid>
                      <Grid item>
                        <Typography variant="body2" className="field-label">
                          First Installment Due
                        </Typography>
                        <KeyboardDatePicker
                          inputProps={{ "data-testid": "first-installment-due" }}
                          id="firstInstallmentDue"
                          inputVariant="outlined"
                          variant="inline"
                          disableToolbar
                          fullWidth
                          placeholder="mm/dd/yyyy"
                          format="MM/DD/yyyy"
                          value={values.firstInstallmentDue}
                          error={!!(errors.firstInstallmentDue && touched.firstInstallmentDue)}
                          helperText={
                            errors.firstInstallmentDue && touched.firstInstallmentDue && errors.firstInstallmentDue
                          }
                          onChange={(date) => setFieldValue("firstInstallmentDue", date)}
                          FormHelperTextProps={{ classes: errorTextStyles }}
                        />
                      </Grid>
                    </Grid>
                    <Grid item container direction="column" spacing={5} xs={4}>
                      <Grid item>
                        <Typography variant="body2" className="field-label">
                          Monthly Installment
                        </Typography>
                        <TextField
                          inputProps={{ "data-testid": "monthly-installment" }}
                          name="monthlyInstallment"
                          variant="outlined"
                          fullWidth
                          value={values.monthlyInstallment}
                          onChange={(value) => setFieldValue("monthlyInstallment", parseInt(value.target.value))}
                          error={!!(errors.monthlyInstallment && touched.monthlyInstallment)}
                          helperText={errors.monthlyInstallment && touched.monthlyInstallment && errors.monthlyInstallment}
                          FormHelperTextProps={{ classes: errorTextStyles }}
                          InputProps={{
                            startAdornment: <InputAdornment position="start">$</InputAdornment>,
                            inputComponent: CurrencyFormat as any
                          }}
                        />
                      </Grid>
                      <Grid item>
                        <Typography variant="body2" className="field-label">
                          Last Installment Due
                        </Typography>
                        <KeyboardDatePicker
                          inputProps={{ "data-testid": "last-installment-due" }}
                          id="lastInstallmentDue"
                          inputVariant="outlined"
                          variant="inline"
                          disableToolbar
                          fullWidth
                          format="MM/DD/yyyy"
                          placeholder="mm/dd/yyyy"
                          value={values.lastInstallmentDue}
                          error={!!(errors.lastInstallmentDue && touched.lastInstallmentDue)}
                          helperText={errors.lastInstallmentDue && touched.lastInstallmentDue && errors.lastInstallmentDue}
                          onChange={(date) => setFieldValue("lastInstallmentDue", date)}
                          FormHelperTextProps={{ classes: errorTextStyles }}
                        />
                      </Grid>
                    </Grid>
                    <Grid item container direction="column" spacing={5} xs={4}>
                      <Grid item>
                        <Typography variant="body2" className="field-label">
                          Final Installment (if applicable)
                        </Typography>
                        <TextField
                          inputProps={{ "data-testid": "final-installment" }}
                          name="finalInstallment"
                          variant="outlined"
                          fullWidth
                          value={values.finalInstallment}
                          onChange={(value) => setFieldValue("finalInstallment", parseInt(value.target.value))}
                          error={!!(errors.finalInstallment && touched.finalInstallment)}
                          helperText={errors.finalInstallment && touched.finalInstallment && errors.finalInstallment}
                          FormHelperTextProps={{ classes: errorTextStyles }}
                          InputProps={{
                            startAdornment: <InputAdornment position="start">$</InputAdornment>,
                            inputComponent: CurrencyFormat as any
                          }}
                        />
                      </Grid>
                      <Grid item>
                        <Typography variant="body2" className="field-label">
                          Contract Date
                        </Typography>
                        <KeyboardDatePicker
                          inputProps={{ "data-testid": "contract-date" }}
                          id="contractDate"
                          inputVariant="outlined"
                          variant="inline"
                          disableToolbar
                          fullWidth
                          format="MM/DD/yyyy"
                          placeholder="mm/dd/yyyy"
                          value={values.contractDate}
                          error={!!(errors.contractDate && touched.contractDate)}
                          helperText={errors.contractDate && touched.contractDate && errors.contractDate}
                          onChange={(date) => setFieldValue("contractDate", date)}
                          FormHelperTextProps={{ classes: errorTextStyles }}
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                </MuiPickersUtilsProvider>
              </CardContent>
            </Card>
            <Button
              color="primary"
              variant="contained"
              id="create-btn"
              style={{ float: "right", marginTop: "15px" }}
              onClick={() => {
                setConfirmContractLengthModal(true);
              }}
            >
              Submit
            </Button>
          </form>
        );
      }}
    </Formik>
  );
});

export default CreateEditContract;
