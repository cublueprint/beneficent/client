import React from "react";
import { CheckCircle as CheckIcon, Error as ErrorIcon } from "@material-ui/icons";
import "./Alert.scss";

interface AlertProps {
  variant?: "success" | "warning";
  icon?: boolean;
  coloredText?: boolean;
  children?: React.ReactNode;
}

const Alert: React.FC<AlertProps> = ({ variant = "success", icon = false, coloredText = false, children }) => {
  const renderIcon = () => {
    switch (variant) {
      case "success":
        return <CheckIcon className="alert-icon alert-text-success" />;
      case "warning":
        return <ErrorIcon className="alert-icon alert-text-warning" />;
      default:
        return null;
    }
  };

  return (
    <div className={`alert-container alert-bg-${variant}`}>
      {icon && renderIcon()}
      <span className={`alert-content ${coloredText ? `alert-text-${variant}` : ""}`}>{children}</span>
    </div>
  );
};

export default Alert;
