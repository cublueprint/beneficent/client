import React, { useState } from "react";
import { MuiThemeProvider, Button, Modal, Paper, createMuiTheme } from "@material-ui/core";
import { Close as CloseIcon } from "@material-ui/icons";
import axios from "axios";
import "./UploadContractModal.scss";
// import { apiContracts } from "../../../../../../../services/api/contracts/apiContracts";
import Alert from "./Alert/Alert";
import FileDropzone from "../../../../../../common/FileDropzone/FileDropzone";
import type { Contract } from "../ContractCard";

const MAX_FILE_SIZE = 15; // MB

// custom theme for black buttons
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#000000",
      contrastText: "#ffffff"
    }
  }
});

// object to keep track of each error state
interface Errors {
  fileUploaded?: boolean;
  fileSize?: number;
  fileType?: boolean;
  submitted?: boolean;
}

interface UploadContractModalProps {
  open?: boolean;
  contract: Contract;
  activeContractPresent: boolean;
  onClose: () => void;
}

const UploadContractModal: React.FC<UploadContractModalProps> = ({ open, contract, activeContractPresent, onClose }) => {
  const accessToken = localStorage.getItem("accessToken") || "";
  const [file, setFile] = useState<File | null>(null);
  const [errors, setErrors] = useState<Errors>({});
  const [confirm, setConfirm] = useState(false); // whether we reached the "confimr" screen
  const [success, setSuccess] = useState(false); // whether we reached the "successful" screen

  // helper to round file size to MB with 1 decimal place
  const roundFileSize = (size: number) => {
    return Math.round(size / 100000) / 10;
  };

  // handle when "upload" button is clicked
  const handleClickUpload = () => {
    const tempErrors: Errors = {
      submitted: false,
      fileUploaded: !file
    };
    if (Object.values(tempErrors).includes(true)) {
      setErrors({ ...errors, ...tempErrors });
      return;
    }
    setConfirm(true);
  };

  const handleClickConfirm = async () => {
    let response;
    try {
      // response = await apiContracts.upload(accessToken, {
      //   contracts: {
      //     file: file!
      //   }
      // });
      const formData = new FormData();
      formData.append("file", file!);
      response = await axios.post(
        `${process.env.REACT_APP_API_ENDPOINT || ""}/api/v1/contracts/${contract.id}/upload`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${accessToken}`
          }
        }
      );
    } catch (error) {
      // if any server error, return early
      console.log(error);
      setErrors({ ...errors, submitted: true });
      return;
    }
    // if good response, move onto success state
    if (response.status === 201) {
      setSuccess(true);
    }
  };

  // handle when dropzone receives a file
  const handleUpload = (file: File) => {
    // validate file then set it to state
    const tempErrors: Errors = {
      fileUploaded: false,
      fileSize: file.size > MAX_FILE_SIZE * 1000000 ? file.size : 0,
      fileType: file.type !== "application/pdf"
    };
    setErrors({ ...errors, ...tempErrors });
    // if any errors, return early and don't upload file
    if (tempErrors.fileSize || tempErrors.fileType) {
      return;
    }
    setFile(file);
  };

  // gets the correct dropzone variant based on errors/input
  const getVariant = () => {
    if (errors.fileSize || errors.fileType) return "error";
    if (file) return "success";
    return "info";
  };

  // gets the corresponding error message
  const getErrorMessage = () => {
    if (errors.fileSize) return "Your file exceeds the size limit.\nPlease resize and try again!";
    if (errors.fileType) return "Your file is invalid.\nPlease choose other file types and try again!";
    return "";
  };

  const BaseState = () => (
    <>
      <header className="modal-card-header">
        <h2 className="modal-card-title">Upload Signed Contract</h2>
        <Button className="modal-btn-x" aria-label="close" onClick={onClose}>
          <CloseIcon className="modal-btn-x-icon" />
        </Button>
      </header>
      <section className="modal-card-body">
        <FileDropzone
          variant={getVariant()}
          onUpload={handleUpload}
          accept="application/pdf"
          fileName={file ? file.name : ""}
          fileSize={file ? roundFileSize(file.size) : 0}
          error={getErrorMessage()}
        />
        <p className="modal-card-label alert-margin">Accepted File Types: .pdf</p>
        {activeContractPresent ? (
          <Alert variant="warning" icon>
            There is currently an active contract.
          </Alert>
        ) : (
          <Alert variant="success" icon>
            There is currently no active contract.
          </Alert>
        )}
        {errors.fileUploaded && <p className="modal-card-label modal-card-required">Please upload a file.</p>}
        {errors.submitted && (
          <p className="modal-card-label modal-card-required">There was an error while submitting. Please try again.</p>
        )}
      </section>
      <footer className="modal-card-footer">
        <Button className="modal-btn modal-btn-footer modal-btn-cancel" onClick={onClose}>
          Cancel
        </Button>
        <Button
          disableElevation
          className="modal-btn modal-btn-footer"
          color="primary"
          variant="contained"
          onClick={() => handleClickUpload()}
        >
          Upload
        </Button>
      </footer>
    </>
  );

  const ConfirmState = () => (
    <>
      <header className="modal-card-header">
        <h2 className="modal-card-title">Upload Signed Contract</h2>
        <Button className="modal-btn-x" aria-label="close" onClick={onClose}>
          <CloseIcon className="modal-btn-x-icon" />
        </Button>
      </header>
      <section className="modal-card-body">
        <p className="alert-margin">Are you sure you want to upload this signed contract?</p>
        <Alert variant="success" coloredText>
          {contract.status === "Active"
            ? "Once uploaded, this contract will be active and the existing active contract will be archived."
            : "Once uploaded, the contract status will become active."}
        </Alert>
      </section>
      <footer className="modal-card-footer">
        <Button className="modal-btn modal-btn-footer modal-btn-cancel" onClick={() => setConfirm(false)}>
          Back
        </Button>
        <Button
          disableElevation
          className="modal-btn modal-btn-footer"
          color="primary"
          variant="contained"
          onClick={() => handleClickConfirm()}
        >
          Confirm
        </Button>
      </footer>
    </>
  );

  const SuccessState = () => (
    <>
      <header className="modal-card-header">
        <h2 className="modal-card-title text-success">Successful!</h2>
      </header>
      <section className="modal-card-body">
        <p>The signed contract has been submitted.</p>
      </section>
      <footer className="modal-card-footer">
        <Button
          disableElevation
          className="modal-btn modal-btn-footer"
          color="primary"
          variant="contained"
          onClick={onClose}
        >
          Close
        </Button>
      </footer>
    </>
  );

  return (
    <MuiThemeProvider theme={theme}>
      <Modal open={!!open} onClose={onClose} className="modal-container">
        <Paper elevation={0} className="modal-card">
          {success ? <SuccessState /> : confirm ? <ConfirmState /> : <BaseState />}
        </Paper>
      </Modal>
    </MuiThemeProvider>
  );
};

export default UploadContractModal;
