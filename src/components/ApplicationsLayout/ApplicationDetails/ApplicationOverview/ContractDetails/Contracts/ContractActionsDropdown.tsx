import { FormControl, MenuItem, Select, InputBase, withStyles } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { ExpandMore } from "@material-ui/icons";
import Dialogue from "../../../../../common/Dialogue/Dialogue";
import { apiContracts } from "../../../../../../services/api/contracts/apiContracts";

//TODO: adjust as needed for modals
interface ContractActionDropdownProps {
  applicationId: string;
  contractId: string;
  contractFileName: string;
  actions: string[];
  actionMap?: { [key: string]: Function };
  onChange: Function;
}
/**
 * Customized Drop Down Input for Action field
 */
const StatusInput = withStyles(() => ({
  input: {
    height: 16,
    minWidth: 170,
    border: "1px solid rgb(0, 0, 0, 0.12)",
    borderRadius: 4,
    fontSize: 16,
    color: "black",
    paddingTop: 6,
    paddingLeft: 10,
    paddingBottom: 10
  }
}))(InputBase);

const archiveContract = async (contractId: string) => {
  const token = localStorage.getItem("accessToken");
  try {
    const data = await apiContracts.patch(contractId, { contracts: { status: "Archived" } });
  } catch (e: any) {
    console.error(e.message);
  }
};

const deleteContract = async (contractId: string) => {
  const token = localStorage.getItem("accessToken");
  try {
    await apiContracts.remove(contractId);
  } catch (e: any) {
    console.error(e.message);
  }
};

/**
 * Dropdown for Contract Actions
 * @param props component props
 * @returns React Component
 */
const ContractActionsDropdown = (props: ContractActionDropdownProps) => {
  const [action, setAction] = useState("");
  const actions = props.actions;
  const actionMap = props.actionMap;
  const contractId = props.contractId;
  const contractFileName = props.contractFileName;

  const [openTextModal, setOpenTextModal] = useState(false);
  const [moreActionsModalProps, setMoreActionsModalProps] = useState({
    title: "",
    text: "",
    open: openTextModal,
    hasActions: true,
    onConfirm: () => {},
    onClose: () => {}
  });

  const moreActionProps = {
    archive: {
      open: openTextModal,
      title: "Confirm Your Action",
      text: "Are you sure you want to archive Contract ",
      hasActions: true,
      onConfirm: async () => {
        await archiveContract(contractId);
        props.onChange(); //triggers rerender of contracts
      },
      onClose: () => {
        setOpenTextModal(false);
      }
    },

    delete: {
      open: openTextModal,
      title: "Confirm Your Action",
      text: "Are you sure you want to delete Contract ",
      hasActions: true,
      onConfirm: async () => {
        await deleteContract(contractId);
        props.onChange(); //triggers rerender of contracts
      },
      onClose: () => {
        setOpenTextModal(false);
      }
    }
  };

  useEffect(() => {
    const actionsRequireTextWarning = ["Archive", "Delete"]; //TODO: update as needed
    if (actionsRequireTextWarning.includes(action)) {
      setOpenTextModal(true);
    }
  }, [action]);

  useEffect(() => {
    if (openTextModal && action) {
      moreActionProps[action.toLowerCase()].text += `${contractFileName}?`;
      setMoreActionsModalProps({ ...moreActionProps[action.toLowerCase()] });
    }
  }, [openTextModal]);

  const onChangeSelect = async (event) => {
    const newAction = event.target.value;
    setAction(newAction); //upload more documents? (Vinh), archive, delete
    if (actionMap && actionMap[newAction]) {
      actionMap[newAction]();
    }
  };

  //sets actions
  const actionOptions = (appAction) => (
    <MenuItem value={appAction} key={appAction}>
      <p className="status-menu-item">{appAction}</p>
    </MenuItem>
  );

  return (
    <FormControl fullWidth>
      <Select
        labelId="status-select-label"
        id="status-select"
        displayEmpty
        value={action}
        input={<StatusInput />}
        onClick={onChangeSelect}
        inputProps={{
          "data-testid": "select",
          classes: {
            icon: "dropdown-icon"
          }
        }}
        IconComponent={ExpandMore}
      >
        <MenuItem disabled value="">
          <p className="status-menu-item">More Action</p>
        </MenuItem>
        {actions.map((action) => actionOptions(action))}
      </Select>
      {openTextModal ? <Dialogue data-testid="modal" {...moreActionsModalProps} open={openTextModal} /> : null}
    </FormControl>
  );
};

export default ContractActionsDropdown;
