import React, { useEffect, useState } from "react";
import MyTable, { ColumnType } from "../../../../../common/Table/Table";
import { apiContracts } from "../../../../../../services/api/contracts/apiContracts";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import { Button, Card, CardContent, CardHeader, Grid, IconButton } from "@material-ui/core";
import { Add as AddIcon } from "@material-ui/icons";
import DownloadIcon from "@material-ui/icons/GetApp";
import moment from "moment";
import ContractActionsDropdown from "./ContractActionsDropdown";
import { useHistory } from "react-router";
import UploadContractModal from "./UploadContractModal/UploadContractModal";
import Dialogue from "../../../../../common/Dialogue/Dialogue";

import "./ContractCard.scss";
import { apiUsers } from "../../../../../../services/api/users/apiUsers";

interface ContractProps {
  applicationId: string;
}

export type Contract = {
  id: string;
  status: string;
  contractFileName: string;
  createdAt: string;
  createdBy: {
    firstName: string;
    lastName: string;
  };
  updatedAt: string;
  approvedLoanAmount: Number;
  firstPaymentDue: string;
  finalPaymentDue: string;
  contractStartDate: string;
  client: string;
  monthlyPayment: Number;
  finalPayment: Number;
};

/** Service Functions (Interacts with Server)*/

/**
 * calls API to download current contract
 * @param contractId id of contract
 */
const getFile = (contractId: string) => {
  return apiContracts.download(contractId);
};

/**
 *
 * @param applicationId id of the current application displayed
 * @returns all contracts associated with application
 */
const getAllContracts = async (applicationId: string, page: number, limit: number, populate: string): Promise<any> => {
  const data = await apiContracts.getAll({
    contracts: {
      application: applicationId,
      page: page,
      limit: limit,
      populate: populate
    }
  });
  if (data?.code && data.code !== 200) {
    return { results: [], totalResults: 0 };
  }
  return data;
};

/**
 *
 * @param applicationId id of the current application displayed
 * @returns all active contracts associated with application
 */
const getAllActiveContracts = async (applicationId: string) => {
  const data = await apiContracts.getAll({
    contracts: {
      application: applicationId,
      status: "Active"
    }
  });
  if (data?.code && data.code !== 200) {
    return { results: [], totalResults: 0 };
  }
  return data;
};
/**
 * Displays all contracts for specific user and allows actions to be taken
 * @param props Props Passed to React Component
 * @returns All Application Contracts
 */
const ContractCard = (props: ContractProps) => {
  const history = useHistory();
  const [contracts, setContracts] = useState<Contract[]>([]);
  const [selectedContract, setSelectedContract] = useState<Contract | null>(null);
  const [showUploadModal, setShowUploadModal] = useState(false);
  const [activeContractExists, setActiveContractExists] = useState(false);
  const [page, setPage] = useState(1);
  const [totalRows, setTotalRows] = useState(0);
  const [isViewMore, setIsViewMore] = useState(false);
  const perPage = 5;
  const applicationId: string = props.applicationId;
  const populate: string = "createdBy";

  /**
   * retrieves file from server and forces download to user's drive
   * @param id contract id
   * @param name name of file
   */
  const downloadContract = async (id: string, name: string) => {
    const file = await getFile(id);
    const url = window.URL.createObjectURL(new Blob([file.data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", `${name}`);
    document.body.appendChild(link);
    link.click();
  };

  /** retrieve all current contracts on render */
  useEffect(() => {
    if (localStorage.getItem("accessToken")) {
      getAllContracts(applicationId, page, perPage, populate).then((data) => {
        setContracts(data.results);
        setTotalRows(data.totalResults);
      });
    }
  }, [applicationId, page]);

  useEffect(() => {
    if (localStorage.getItem("accessToken")) {
      getAllActiveContracts(applicationId).then((data) => {
        if (data.results.length > 0) {
          setActiveContractExists(true);
        } else {
          setActiveContractExists(false);
        }
      });
    }
  }, [applicationId]);

  const onPaginate = (newPage) => {
    setPage(newPage);
  };

  const handleOpenUploadModal = (contract: Contract) => {
    setSelectedContract(contract);
    setShowUploadModal(true);
  };

  /**
   * when action dropdown changes, should retrigger getAll to to be consistent
   */
  const onDropdownChange = () => {
    getAllContracts(applicationId, page, perPage, populate).then((data) => {
      setContracts(data.results);
      setTotalRows(data.totalResults);
    });

    getAllActiveContracts(applicationId).then((data) => {
      if (data.results.length > 0) {
        setActiveContractExists(true);
      } else {
        setActiveContractExists(false);
      }
    });
  };

  const handleViewMore = (event) => {
    setIsViewMore(!isViewMore);
    // TODO: update on backend database
  };

  /** Table Columns */
  const columns: ColumnType<Contract>[] = [
    {
      title: "Name",
      render: (record) => (
        <div data-testid="file-name" id="file-name">
          {record.contractFileName}
        </div>
      )
    },
    {
      title: "Uploaded Time",
      render: (record) => (
        <div data-testid="uploaded-time" id="uploaded-time">
          {moment(record.createdAt).format("YYYY-MM-DD h:m A")}
        </div>
      )
    },
    {
      title: "Status",
      render: (record) => (
        <div className="inline-component">
          <span data-testid="status" className={record.status === "Active" ? "active" : "inactive"}>
            {record.status}
          </span>
          {record.status === "Active" ? (
            <CheckCircleIcon data-testid="checkmark-icon" style={{ color: "green" }} className="activeIcon" />
          ) : null}
        </div>
      )
    },
    {
      title: "Actions",
      render: (record) => (
        <div className="inline-component">
          <ContractActionsDropdown
            actions={["Archive", "Delete", "Upload Signed Contract"]}
            actionMap={{ "Upload Signed Contract": () => handleOpenUploadModal(record) }}
            applicationId={applicationId}
            contractId={record.id}
            contractFileName={record.contractFileName}
            onChange={() => onDropdownChange()}
          />
          <IconButton data-testid="download-button" onClick={() => downloadContract(record.id, record.contractFileName)}>
            <DownloadIcon color="primary" id="download-icon" />
          </IconButton>
        </div>
      )
    },
    {
      title: "",
      render: (record) => (
        <Button color="primary" id="view-more" variant="text" onClick={() => handleViewMore(isViewMore)}>
          View More
          {isViewMore ? (
            <Dialogue
              title={"Other Details"}
              text={
                "Creator: " +
                `${record.createdBy.firstName} ${record.createdBy.lastName}` +
                "\nCreated at: " +
                moment(record.createdAt as string).format("YYYY-MM-DD h:m A") +
                "\nLast updated at: " +
                moment(record.updatedAt as string).format("YYYY-MM-DD h:m A")
              }
              hasClose={true}
              open={isViewMore}
            />
          ) : null}
        </Button>
      )
    }
  ];

  return (
    <>
      <Card id="contract-card-box" elevation={1} style={{ width: "1065px" }}>
        <div className="contracts-card-header">
          <CardHeader title="Contracts">Contracts</CardHeader>
          <Button
            id="create-contract-button"
            color="primary"
            variant="contained"
            onClick={() => history.push(props.applicationId + "/create-contract")}
          >
            <AddIcon style={{ marginRight: "5px" }} />
            Create Contract
          </Button>
        </div>

        <Grid container>
          <Grid item md={12}>
            <CardContent id="contract-card-display-contracts">
              <MyTable
                data={contracts}
                columns={columns}
                pagination={{
                  perPage,
                  onPaginate,
                  totalRows,
                  page
                }}
              />
            </CardContent>
          </Grid>
        </Grid>
      </Card>
      {showUploadModal && (
        <UploadContractModal
          open={showUploadModal}
          contract={selectedContract!}
          activeContractPresent={activeContractExists}
          onClose={() => {
            setShowUploadModal(false);
            onDropdownChange();
          }}
        />
      )}
    </>
  );
};
export default ContractCard;
