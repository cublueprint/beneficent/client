import React, { useState } from "react";
import "./DocTypeDropdown.scss";
import { FormControl, MenuItem, Select, withStyles, InputBase, Divider } from "@material-ui/core";
import { ExpandMore as ExpandMoreIcon, ExpandLess as ExpandLessIcon } from "@material-ui/icons";
import axios from "axios";
import { apiDocuments } from "../../../../../services/api/documents/apiDocuments";

/**
 * Customized Drop Down Input for Status field
 */
const DocTypeInput = withStyles(() => ({
  input: {
    width: 127,
    height: 16,
    border: "1px solid rgb(0, 0, 0, 0.12)",
    borderRadius: 4,
    fontSize: 16,
    color: "black",
    paddingTop: 6,
    paddingLeft: 22,
    paddingBottom: 10
  }
}))(InputBase);

type DocTypeDropdownProps = {
  value: string;
  applicationId: string;
  updatedBy: string;
};

const DocTypeDropdown: React.FC<DocTypeDropdownProps> = ({ value, applicationId, updatedBy }) => {
  const options = ["Identification", "Proof of Debt", "Proof of Income", "Other"];
  const [docType, setDocType] = useState(value);

  const handleChangeDocType = async (event) => {
    const newDocType = event.target.value;
    setDocType(newDocType);
    apiDocuments
      .patch(applicationId, {
        documents: {
          docType: newDocType,
          updatedBy
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const renderItem = (docType) => (
    <MenuItem value={docType} key={docType}>
      <p className="doctype-menu-item">{docType}</p>
    </MenuItem>
  );
  return (
    <FormControl variant="outlined">
      <Select value={docType} onChange={handleChangeDocType} input={<DocTypeInput />} IconComponent={ExpandMoreIcon}>
        <div className="doctype-dropdown">
          <p>Document Type</p>
          <ExpandLessIcon className="icon" />
        </div>
        <Divider />
        {options.map((docType) => renderItem(docType))}
      </Select>
    </FormControl>
  );
};

export default DocTypeDropdown;
