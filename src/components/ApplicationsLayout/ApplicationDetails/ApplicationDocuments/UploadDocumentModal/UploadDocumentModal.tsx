import React, { useState } from "react";
import {
  createMuiTheme,
  MuiThemeProvider,
  withStyles,
  Button,
  Modal,
  Paper,
  Menu,
  MenuProps,
  ListItem,
  ListItemText,
  Divider
} from "@material-ui/core";
import { Close as CloseIcon, ExpandMore as ExpandMoreIcon, ExpandLess as ExpandLessIcon } from "@material-ui/icons";
import { apiDocuments } from "../../../../../services/api/documents/apiDocuments";
import "./UploadDocumentModal.scss";
import FileDropzone from "../../../../common/FileDropzone/FileDropzone";

const MAX_FILE_SIZE = 15; // MB

// custom theme for black buttons
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#000000",
      contrastText: "#ffffff"
    }
  }
});

// dropdown menu overlay that matches button size
const SizedMenu = withStyles({
  paper: {
    width: 435
  }
})((props: MenuProps) => (
  <Menu
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "left"
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "left"
    }}
    {...props}
  />
));

// object to keep track of each error state
interface Errors {
  fileUploaded?: boolean;
  fileSize?: number;
  fileType?: boolean;
  documentType?: boolean;
  submitted?: boolean;
}

interface UploadDocumentModalProps {
  open?: boolean;
  applicationId: string;
  uploadedBy: string;
  onClose: () => void;
}

const UploadDocumentModal: React.FC<UploadDocumentModalProps> = ({ open, applicationId, uploadedBy, onClose }) => {
  const accessToken = localStorage.getItem("accessToken") || "";
  const [file, setFile] = useState<File | null>(null);
  const [errors, setErrors] = useState<Errors>({});
  const [documentType, setDocumentType] = useState("");
  const [menuOpen, setMenuOpen] = useState(false); // dropdown menu open state
  const [success, setSuccess] = useState(false); // whether we reached the "successful" screen

  // helper to round file size to MB with 1 decimal place
  const roundFileSize = (size: number) => {
    return Math.round(size / 100000) / 10;
  };

  // handle when "upload" button is clicked
  const handleSubmit = async () => {
    // check for errors and return early if there are any
    const tempErrors: Errors = {
      submitted: false,
      fileUploaded: !file,
      documentType: !documentType
    };
    if (Object.values(tempErrors).includes(true)) {
      setErrors({ ...errors, ...tempErrors });
      return;
    }

    // request API to upload file
    let response;
    try {
      response = await apiDocuments.upload({
        documents: {
          docType: documentType,
          application: applicationId,
          file: file!,
          uploadedBy
        }
      });
    } catch (error) {
      // if any server error, return early
      console.log(error);
      setErrors({ ...errors, submitted: true });
      return;
    }
    // if good response, move onto success state
    if (response.status === 201) {
      setSuccess(true);
    }
  };

  // handle when dropzone receives a file
  const handleUpload = (file: File) => {
    // validate file then set it to state
    const validFileTypes = new Set(["application/pdf", "image/jpeg", "image/png"]);

    const tempErrors: Errors = {
      fileUploaded: false,
      fileSize: file.size > MAX_FILE_SIZE * 1000000 ? file.size : 0,
      fileType: !validFileTypes.has(file.type)
    };
    setErrors({ ...errors, ...tempErrors });
    // if any errors, return early and don't upload file
    if (tempErrors.fileSize || tempErrors.fileType) {
      return;
    }

    setFile(file);
  };

  // handle document type dropdown menu option selection
  const handleSelectOption = (option) => {
    setErrors({ ...errors, documentType: false });
    setDocumentType(option);
    setMenuOpen(false);
  };

  // gets the correct dropzone variant based on errors/input
  const getVariant = () => {
    if (errors.fileSize || errors.fileType) return "error";
    if (file) return "success";
    return "info";
  };

  // gets the corresponding error message
  const getErrorMessage = () => {
    if (errors.fileSize) return "Your file exceeds the size limit.\nPlease resize and try again!";
    if (errors.fileType) return "Your file is invalid.\nPlease choose other file types and try again!";
    return "";
  };

  // renders each item in the document type dropdown menu
  const renderMenuItems = () => {
    const items: React.ReactNode[] = [];
    const options = ["Identification", "Proof of Debt", "Proof of Income", "Other"];
    for (let i = 0; i < options.length; i++) {
      items.push(
        <ListItem button key={`item-${i}`} onClick={() => handleSelectOption(options[i])}>
          <ListItemText primary={options[i]} />
        </ListItem>
      );
      if (i !== options.length - 1) {
        items.push(<Divider key={`divider-${i}`} />);
      }
    }
    return items;
  };

  return (
    <MuiThemeProvider theme={theme}>
      <Modal open={!!open} onClose={onClose} className="modal-container">
        <Paper elevation={0} className="modal-card">
          {success ? (
            <>
              <header className="modal-card-header">
                <h2 className="modal-card-title text-success">Successful!</h2>
              </header>
              <section className="modal-card-body">
                <p>The document has been successfully uploaded.</p>
              </section>
              <footer className="modal-card-footer">
                <Button
                  disableElevation
                  className="modal-btn modal-btn-footer"
                  color="primary"
                  variant="contained"
                  onClick={onClose}
                >
                  Back
                </Button>
              </footer>
            </>
          ) : (
            <>
              <header className="modal-card-header">
                <h2 className="modal-card-title">Upload Document</h2>
                <Button className="modal-btn-x" aria-label="close" onClick={onClose}>
                  <CloseIcon className="modal-btn-x-icon" />
                </Button>
              </header>
              <section className="modal-card-body">
                <FileDropzone
                  variant={getVariant()}
                  onUpload={handleUpload}
                  fileName={file ? file.name : ""}
                  fileSize={file ? roundFileSize(file.size) : 0}
                  error={getErrorMessage()}
                />
                <p className="modal-card-label">Accepted File Types: .png, .jpeg, .jpg, .pdf</p>
                {errors.fileUploaded && <p className="modal-card-label modal-card-required">Please upload a file.</p>}
                <h3 className="modal-card-subtitle">
                  Document Type <span className="modal-card-required">*</span>
                </h3>
                <Button className="modal-btn modal-select-btn" variant="outlined" onClick={() => setMenuOpen(true)}>
                  {documentType === "" ? "Unselect" : documentType}
                  {menuOpen ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                </Button>
                {errors.documentType && (
                  <p className="modal-card-label modal-card-required">A document type must be selected.</p>
                )}
                {errors.submitted && (
                  <p className="modal-card-label modal-card-required">
                    There was an error while submitting. Please try again.
                  </p>
                )}
                <SizedMenu
                  anchorEl={document.querySelector(".modal-select-btn")}
                  open={menuOpen}
                  onClose={() => setMenuOpen(false)}
                >
                  {renderMenuItems()}
                </SizedMenu>
              </section>
              <footer className="modal-card-footer">
                <Button className="modal-btn modal-btn-footer modal-btn-cancel" onClick={onClose}>
                  Cancel
                </Button>
                <Button
                  disableElevation
                  className="modal-btn modal-btn-footer"
                  color="primary"
                  variant="contained"
                  onClick={() => handleSubmit()}
                >
                  Upload
                </Button>
              </footer>
            </>
          )}
        </Paper>
      </Modal>
    </MuiThemeProvider>
  );
};

export default UploadDocumentModal;
