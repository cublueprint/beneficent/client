import React, { useState, useEffect, useCallback } from "react";
import moment from "moment";
import axios from "axios";
import { Button, Card, CardHeader } from "@material-ui/core";
import { Add as AddIcon, GetApp as GetAppIcon } from "@material-ui/icons";
import "./ApplicationDocuments.scss";
import { apiDocuments } from "../../../../services/api/documents/apiDocuments";
import MyTable, { ColumnType } from "../../../common/Table/Table";
import UploadDocumentModal from "./UploadDocumentModal/UploadDocumentModal";
import DocTypeDropdown from "./DocTypeDropdown/DocTypeDropdown";
import { Application } from "../../ApplicationsLayout";
import Dialog from "../../../common/Dialogue/Dialogue";

type Document = {
  id: string;
  originalName: string;
  mimeType: string;
  size: number;
  docType: string;
  application: string;
  createdAt: string;
  updatedAt: string;
  uploadedBy: Application;
};

interface DocumentsRequest {
  totalRows: number;
  documents: Document[];
}

interface ApplicationDocumentsProps {
  applicationId: string;
  userId: string;
}

const ApplicationDocuments: React.FC<ApplicationDocumentsProps> = ({ applicationId, userId }) => {
  const accessToken = localStorage.getItem("accessToken") || "";
  const [documents, setDocuments] = useState<Document[]>([]);
  const [deletedDocument, setDeletedDocument] = useState<Document>();
  const [uploadModalOpen, setUploadModalOpen] = useState(false);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [page, setPage] = useState(1);
  const [totalRows, setTotalRows] = useState(0);
  const perPage = 10;

  const fetchDocuments = useCallback(async (): Promise<DocumentsRequest | undefined> => {
    const response: any = await apiDocuments.getAll({
      documents: {
        application: applicationId,
        limit: perPage.toString(),
        page: page.toString(),
        populate: "uploadedBy"
      }
    });
    if (response instanceof Error) {
      console.error(response);
      return;
    }
    if (response.totalResults && response.results) {
      return {
        totalRows: response.totalResults,
        documents: response.results
      };
    }
  }, [accessToken, perPage, page]);

  useEffect(() => {
    const onLoad = async () => {
      const response = await fetchDocuments();
      if (response) {
        setTotalRows(response.totalRows);
        setDocuments(response.documents);
      }
    };
    onLoad();
  }, [fetchDocuments]);

  const onPaginate = (newPage) => {
    setPage(newPage);
  };

  const handleUploadModal = async () => {
    setUploadModalOpen(false);
    const response = await fetchDocuments();
    if (response) {
      setTotalRows(response.totalRows);
      setDocuments(response.documents);
    }
  };

  const handleDeleteButton = (doc: Document) => {
    setDeletedDocument(doc);
    setDeleteModalOpen(true);
  };

  const handleDelete = async () => {
    const response = await apiDocuments.remove(deletedDocument!.id);
    if (response instanceof Error) {
      console.error(response);
      return;
    }
    const newDocuments = documents.filter((d) => d.id !== deletedDocument!.id);
    setDocuments(newDocuments);
    setDeleteModalOpen(false);
  };

  const handleDownload = async (doc: Document) => {
    const response = await apiDocuments.download(doc.id);
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.setAttribute("href", url);
    link.setAttribute("download", doc.originalName);
    link.click();
  };

  const columns: ColumnType<any>[] = [
    {
      title: "Name",
      render: (record) => record.originalName
    },
    {
      title: "Document Type",
      render: (record) => <DocTypeDropdown value={record.docType} applicationId={record.id} updatedBy={userId} />
    },
    {
      title: "Uploaded Time",
      render: (record) => moment(record.createdAt as string).format("YYYY-MM-DD h:mm A")
    },
    {
      title: "Uploaded By",
      render: (record) => record.uploadedBy.firstName + " " + record.uploadedBy.lastName
    },
    {
      title: "Actions",
      render: (record) => (
        <div className="documents-card-table-row">
          <button className="documents-card-delete-button" onClick={() => handleDeleteButton(record)}>
            Delete
          </button>
          <button className="documents-card-download-button" onClick={() => handleDownload(record)}>
            <GetAppIcon />
          </button>
        </div>
      )
    }
  ];

  return (
    <>
      <Card className="documents-card-container" style={{ width: "1065px" }}>
        <div className="documents-card-header">
          <CardHeader title="Documents" />
          <Button
            className="documents-card-upload-button"
            variant="contained"
            color="primary"
            onClick={() => setUploadModalOpen(true)}
          >
            <AddIcon style={{ marginRight: "5px" }} /> Upload
          </Button>
        </div>

        <MyTable
          data={documents}
          columns={columns}
          pagination={{
            perPage,
            onPaginate,
            totalRows,
            page: page
          }}
        />
        {uploadModalOpen && (
          <UploadDocumentModal
            applicationId={applicationId}
            uploadedBy={userId}
            open={uploadModalOpen}
            onClose={handleUploadModal}
          />
        )}
      </Card>
      {deleteModalOpen && (
        <Dialog
          open={deleteModalOpen}
          title="Confirm Delete Document"
          text={`Please confirm you want to delete document ${deletedDocument?.originalName}`}
          hasActions
          onClose={() => setDeleteModalOpen(false)}
          onConfirm={() => handleDelete()}
        />
      )}
    </>
  );
};

export default ApplicationDocuments;
