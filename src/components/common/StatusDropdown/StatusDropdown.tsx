import React from "react";
import "./StatusDropdown.scss";
import { FormControl, MenuItem, Select, withStyles, InputBase, Divider } from "@material-ui/core";
import { ExpandMore, ExpandLess } from "@material-ui/icons";
import Dialogue from "../Dialogue/Dialogue";

/**
 * Customized Drop Down Input for Status field
 */
const StatusInput = withStyles(() => ({
  input: {
    width: "auto",
    height: 16,
    border: "1px solid rgb(0, 0, 0, 0.12)",
    borderRadius: 4,
    fontSize: 16,
    color: "black",
    paddingTop: 6,
    paddingLeft: 22,
    paddingBottom: 10
  }
}))(InputBase);

type StatusDropdownProps = {
  application?: any;
  value: string;
  onChange: (status: string) => void;
};

/**
 * Renders Application Status dropdown to update the status of an
 *  application
 */
function StatusDropdown(props: StatusDropdownProps) {
  const { onChange, application, value: status } = props;

  const { validStatuses: statuses, firstName, lastName } = application;

  const [modalOpen, setModalOpen] = React.useState("");

  const renderItem = (appStatus) => (
    <MenuItem
      value={appStatus}
      key={appStatus}
      onClick={() => {
        if (status === appStatus) return;

        if (appStatus === "Active Client") {
          setModalOpen("Active Client");
        } else if (["Accepted", "Rejected", "Waitlist"].includes(appStatus)) {
          setModalOpen(appStatus);
        } else {
          onChange(appStatus);
        }
      }}
    >
      <p className="status-menu-item">{appStatus}</p>
    </MenuItem>
  );

  const renderDialogue = () => {
    switch (modalOpen) {
      case "Active Client":
        return (
          <Dialogue
            title="Change Status to Active Client"
            text={`Are you sure you would like ${firstName} ${lastName} to become an Active Client?\nThis change is irreversible.`}
            open={!!modalOpen}
            hasActions
            onConfirm={() => onChange("Active Client")}
            onClose={() => setModalOpen("")}
          />
        );
      case "Accepted":
      case "Rejected":
      case "Waitlist":
        return (
          <Dialogue
            title="Reminder"
            text="Please remove any confidential documents associated with this applicant."
            open={!!modalOpen}
            hasActions
            onConfirm={() => onChange(modalOpen)}
            onClose={() => setModalOpen("")}
          />
        );
      default:
        return null;
    }
  };

  return (
    <>
      <FormControl variant="outlined">
        <Select
          labelId="status-select-label"
          id="status-select"
          value={status}
          input={<StatusInput />}
          inputProps={{
            classes: {
              icon: "dropdown-icon"
            }
          }}
          IconComponent={ExpandMore}
        >
          <div className="status-dropdown">
            <p>Status</p>
            <ExpandLess className="icon" />
          </div>
          <Divider />
          {statuses && statuses.map((appStatus) => renderItem(appStatus))}
        </Select>
      </FormControl>
      {renderDialogue()}
    </>
  );
}

export default StatusDropdown;
