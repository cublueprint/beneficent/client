import {
  SvgIconTypeMap,
  TextField,
  Grid,
  InputAdornment,
  IconButton,
  Button,
  Menu,
  ListItem,
  Checkbox,
  ListItemText,
  Avatar,
  withStyles,
  CheckboxProps,
  MenuProps,
  Container
} from "@material-ui/core";

import { OverridableComponent } from "@material-ui/core/OverridableComponent";
import {
  Search as SearchIcon,
  ExpandMore as ExpandMoreIcon,
  ExpandLess as ExpandLessIcon,
  SwapVert as SwapVertIcon
} from "@material-ui/icons";
import React, { useState } from "react";
import "./TableFilters.scss";
import { KeyboardDatePicker } from "@material-ui/pickers";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import moment from "moment";
import { FilterCheckboxSelectionType, FilterChooseSelectionType, FilterSections, LoanOfficerType } from "../CommonTypes";

/**
 * The Date Picker Input that is currently focused
 */
enum Focused {
  NONE,
  START,
  END
}

const itemSelectedColor = "rgba(30, 105, 255, 0.08)";

/**
 * Checkbox styled with the "Beneficent" blue
 */
const ColoredCheckbox = withStyles({
  root: {
    "&:hover": {
      backgroundColor: "transparent"
    },
    "&$checked": {
      color: "#DA6E5D"
    }
  },
  checked: {}
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

/**
 * This is used to show the small vertical line that represents
 * the busyness of a Loan Officer
 */
export const BusyLevelIndicator: React.FC<{ level: 1 | 2 | 3 }> = ({ level }) => {
  let color: string;
  switch (level) {
    case 1:
      color = "#70D19C";
      break;
    case 2:
      color = "#FFB946";
      break;
    case 3:
      color = "#F7685B";
      break;
    default:
      color = "#70D19C";
      break;
  }

  return (
    <div
      className="busy-level-indicator"
      style={{
        backgroundColor: color
      }}
    />
  );
};

/**
 * Menu with width equals to the button for each section
 */
const SizedMenu = withStyles({
  paper: {
    width: 190
  }
})((props: MenuProps) => (
  <Menu
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "left"
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "left"
    }}
    {...props}
  />
));

/**
 * SizedMenu but with added width
 */
const DateMenu = withStyles({
  paper: {
    width: 400
  }
})((props: MenuProps) => (
  <Menu
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "left"
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "left"
    }}
    {...props}
  />
));

const ColoredMenuItem = withStyles({
  root: {
    "&$selected, &$selected:hover": {
      backgroundColor: itemSelectedColor
    }
  },
  selected: {}
})(ListItem);

/**
 * The type of each filter selection in the corresponsing state
 */
type FilterSectionType = {
  title: string;
  anchorEl: null | (EventTarget & HTMLElement);
  icon: OverridableComponent<SvgIconTypeMap<{}, "svg">>;
};

/**
 * Props type of the main component TableFilters
 */
interface TableFiltersProp {
  statusSelections: Array<FilterCheckboxSelectionType>;
  handleToggleStatus: (title: string) => void;
  assignedToSelections: Array<LoanOfficerType>;
  handleToggleChooseAssigned: (name: string) => void;
  sortBySelections?: FilterChooseSelectionType;
  handleChooseSortType?: (index: number) => void;
  startDate: MaterialUiPickersDate | null;
  endDate: MaterialUiPickersDate | null;
  setStartDate: (date: MaterialUiPickersDate) => void;
  setEndDate: (date: MaterialUiPickersDate) => void;
  handleApply: () => void;
  sorting: Boolean;
  setSearchValue: (search: string) => void;
  setPage: (page: number) => void;
  disableFuture: boolean;
}

/**
 * The search and filter section of Reusable Tables
 * @param param0 TableFiltersProp
 */
const TableFilters = (props: TableFiltersProp) => {
  const {
    statusSelections,
    handleToggleStatus,
    assignedToSelections,
    handleToggleChooseAssigned,
    sortBySelections,
    handleChooseSortType,
    setStartDate,
    startDate,
    setEndDate,
    endDate,
    handleApply,
    sorting,
    setSearchValue,
    setPage,
    disableFuture
  } = props;

  const [focused, setFocused] = useState<Focused>(Focused.NONE);

  const [filterSections, setFilterSections] = useState<Array<FilterSectionType>>([
    {
      title: FilterSections.STATUS,
      anchorEl: null,
      icon: ExpandMoreIcon
    },
    {
      title: FilterSections.ASSIGNED_TO,
      anchorEl: null,
      icon: ExpandMoreIcon
    },
    {
      title: "Date",
      anchorEl: null,
      icon: ExpandMoreIcon
    },
    ...(sorting
      ? [
          {
            title: FilterSections.SORT_BY,
            anchorEl: null,
            icon: SwapVertIcon
          }
        ]
      : [])
  ]);

  /**
   * Handle open a section when user click on one
   * @param title the title of the section
   * @param event the HTML event element
   */
  const handleOpenSection = (title: string, event: React.MouseEvent<HTMLElement>) => {
    setFilterSections(
      filterSections.map((section) => {
        if (section.title !== title) {
          return section;
        }
        return {
          ...section,
          anchorEl: event.currentTarget,
          icon: title === FilterSections.SORT_BY ? SwapVertIcon : ExpandLessIcon
        };
      })
    );
  };

  // TODO Maybe validate the filter in this function (After closing the menu)
  /**
   * Handle close a section when needed
   */
  const handleCloseSection = () => {
    setFilterSections(
      filterSections.map((section) => {
        return {
          ...section,
          anchorEl: null,
          icon: section.title === FilterSections.SORT_BY ? SwapVertIcon : ExpandMoreIcon
        };
      })
    );
  };

  /**
   * Handle blur of Date Picker
   */
  const handleBlur = () => {
    setFocused(Focused.NONE);
  };

  /**
   * Render menu items based on the clicked section
   * @param title the title of a filter section
   */
  const renderMenuItems = (title: string) => {
    switch (title) {
      case FilterSections.STATUS:
        return statusSelections.map((selection, index) => (
          <ColoredMenuItem key={index} button onClick={() => handleToggleStatus(selection.title)}>
            <ListItemText primary={selection.title} />
            <ColoredCheckbox disableRipple checked={selection.checked} />
          </ColoredMenuItem>
        ));

      case FilterSections.ASSIGNED_TO:
        return assignedToSelections.map((selection, index) => (
          <ColoredMenuItem
            key={index}
            button
            style={{ marginBottom: 8 }}
            onClick={() => handleToggleChooseAssigned(selection.name)}
          >
            <ListItemText primary={selection.name} />
            <ColoredCheckbox disableRipple checked={selection.selected} />
          </ColoredMenuItem>
        ));

      case "Date":
        return (
          <Container style={{ paddingTop: "10px" }}>
            <Grid container>
              <Grid container spacing={0}>
                <Grid item xs={6}>
                  <div className="date-picker-left">
                    <KeyboardDatePicker
                      TextFieldComponent={(myProps) => {
                        return <TextField {...myProps} style={{ width: 176 }} />;
                      }}
                      autoOk
                      variant="inline"
                      inputVariant="outlined"
                      label="Start Date"
                      format="MM/DD/yyyy"
                      disableFuture={disableFuture}
                      maxDate={disableFuture ? endDate || moment() : endDate || undefined}
                      value={startDate}
                      onChange={(date) => setStartDate(date)}
                      KeyboardButtonProps={{ style: { padding: 0 } }}
                      InputAdornmentProps={{ position: "start" }}
                      onFocus={() => setFocused(Focused.START)}
                      autoFocus={focused === Focused.START}
                      onBlur={handleBlur}
                      ToolbarComponent={() => (
                        <Grid container>
                          <Grid item xs={6}>
                            <Button onClick={() => setStartDate(moment())}>Today</Button>
                          </Grid>
                          <Grid item xs={6}>
                            <Button style={{ float: "right" }} onClick={() => setStartDate(null)}>
                              None
                            </Button>
                          </Grid>
                        </Grid>
                      )}
                    />
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div className="date-picker-right">
                    <KeyboardDatePicker
                      TextFieldComponent={(myProps) => {
                        return <TextField {...myProps} style={{ width: 176 }} />;
                      }}
                      autoOk
                      variant="inline"
                      inputVariant="outlined"
                      label="End Date"
                      format="MM/DD/yyyy"
                      value={endDate}
                      minDate={startDate || moment(0)}
                      disableFuture={disableFuture}
                      onChange={(date) => setEndDate(date)}
                      KeyboardButtonProps={{ style: { padding: 0 } }}
                      InputAdornmentProps={{ position: "start" }}
                      onFocus={() => setFocused(Focused.END)}
                      autoFocus={focused === Focused.END}
                      onBlur={handleBlur}
                      ToolbarComponent={() => (
                        <Grid container>
                          <Grid item xs={6}>
                            <Button onClick={() => setEndDate(moment())}>Today</Button>
                          </Grid>
                          <Grid item xs={6}>
                            <Button style={{ float: "right" }} onClick={() => setEndDate(null)}>
                              None
                            </Button>
                          </Grid>
                        </Grid>
                      )}
                    />
                  </div>
                </Grid>
              </Grid>
            </Grid>
            <Button
              style={{ marginTop: "10px", marginBottom: "10px", float: "right" }}
              color="primary"
              variant="contained"
              disabled={!(startDate || endDate)}
              onClick={() => handleApply()}
            >
              Apply
            </Button>
          </Container>
        );

      case FilterSections.SORT_BY:
        return sortBySelections?.selections.map((selection, index) => (
          <ColoredMenuItem
            key={index}
            button
            selected={index === sortBySelections?.selected}
            onClick={() => handleChooseSortType?.(index)}
          >
            <ListItemText primary={selection} />
          </ColoredMenuItem>
        ));

      default:
        return;
    }
  };

  return (
    <Grid className="table-filters-container" container direction="row" spacing={6}>
      <Grid item xs={5}>
        <TextField
          onChange={(event) => {
            setSearchValue(event.target.value);
            setPage(1);
          }}
          variant="outlined"
          placeholder="Search by name or email"
          className="search-field"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton>
                  <SearchIcon color="action" />
                </IconButton>
              </InputAdornment>
            )
          }}
        />
      </Grid>

      <Grid item container xs={7} spacing={3}>
        {filterSections.map((section, index) => (
          <Grid key={index} item>
            <Button
              className="filter-select"
              variant="outlined"
              onClick={(event) => handleOpenSection(section.title, event)}
            >
              {section.title}
              <section.icon color="action" />
            </Button>
            {section.title === "Date" ? (
              <DateMenu anchorEl={section.anchorEl} open={Boolean(section.anchorEl)} onClose={handleCloseSection}>
                {renderMenuItems(section.title)}
              </DateMenu>
            ) : (
              <SizedMenu anchorEl={section.anchorEl} open={Boolean(section.anchorEl)} onClose={handleCloseSection}>
                {renderMenuItems(section.title)}
              </SizedMenu>
            )}
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};

export default TableFilters;
