import React, { useCallback } from "react";
import { createMuiTheme, MuiThemeProvider, Button } from "@material-ui/core";
import { useDropzone } from "react-dropzone";
import "./FileDropzone.scss";

// custom theme for black buttons
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#000000",
      contrastText: "#ffffff"
    }
  }
});

// containing the icon svgs
const CloudUploadIcon = () => (
  <svg width="99" height="100" viewBox="0 0 99 100" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M65.6663 66.6667L48.9997 50L32.333 66.6667"
      stroke="#C4C4C4"
      strokeWidth="5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M49 50V87.5" stroke="#C4C4C4" strokeWidth="5" strokeLinecap="round" strokeLinejoin="round" />
    <path
      d="M83.9584 76.6248C88.0223 74.4092 91.2327 70.9035 93.0829 66.6607C94.9331 62.418 95.3176 57.6799 94.1759 53.1943C93.0342 48.7087 90.4313 44.7311 86.7779 41.8891C83.1245 39.0472 78.6287 37.5028 74.0001 37.4998H68.7501C67.4889 32.6216 65.1383 28.0928 61.8749 24.2539C58.6115 20.415 54.5203 17.3658 49.9088 15.3356C45.2974 13.3055 40.2857 12.3471 35.2506 12.5326C30.2154 12.7181 25.2879 14.0427 20.8383 16.4067C16.3888 18.7707 12.533 22.1127 9.561 26.1814C6.58896 30.25 4.57795 34.9395 3.67916 39.8972C2.78037 44.855 3.01718 49.952 4.3718 54.805C5.72642 59.6581 8.16359 64.1409 11.5001 67.9164"
      stroke="#C4C4C4"
      strokeWidth="5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M65.6663 66.6667L48.9997 50L32.333 66.6667"
      stroke="#C4C4C4"
      strokeWidth="5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

const FileIcon = () => (
  <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M35 5H15C13.6739 5 12.4021 5.52678 11.4645 6.46447C10.5268 7.40215 10 8.67392 10 10V50C10 51.3261 10.5268 52.5979 11.4645 53.5355C12.4021 54.4732 13.6739 55 15 55H45C46.3261 55 47.5978 54.4732 48.5355 53.5355C49.4732 52.5979 50 51.3261 50 50V20L35 5Z"
      stroke="#2189EA"
      strokeWidth="4"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M35 5V20H50" stroke="#2189EA" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M40 32.5H20" stroke="#2189EA" strokeWidth="4" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M40 42.5H20" stroke="#2189EA" strokeWidth="4" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M25 22.5H22.5H20" stroke="#2189EA" strokeWidth="4" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);

const AlertIcon = () => (
  <svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M27 49.5C39.4264 49.5 49.5 39.4264 49.5 27C49.5 14.5736 39.4264 4.5 27 4.5C14.5736 4.5 4.5 14.5736 4.5 27C4.5 39.4264 14.5736 49.5 27 49.5Z"
      stroke="#F7685B"
      strokeWidth="4"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M27 18V27" stroke="#F7685B" strokeWidth="4" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M27 36H27.0225" stroke="#F7685B" strokeWidth="4" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);

interface FileDropzoneProps {
  variant: "success" | "error" | "info";
  onUpload: (file: File) => void;
  accept?: string;
  fileName?: string;
  fileSize?: number;
  error?: string;
}

const FileDropzone: React.FC<FileDropzoneProps> = ({ variant, onUpload, accept, fileName, fileSize, error }) => {
  // method used by dropzone to upload files
  const onDrop = useCallback(
    (acceptedFiles) => {
      if (acceptedFiles.length > 0) {
        onUpload(acceptedFiles[0]);
      }
    },
    [onUpload]
  );
  // hook for dropzone, including an accept parameter.
  // any invalid file types do not make it past to the "acceptedFiles" array
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: accept || "image/png, image/jpeg, application/pdf"
  });

  // get the right icon based on variant
  const getIcon = () => {
    switch (variant) {
      case "success":
        return <FileIcon />;
      case "error":
        return <AlertIcon />;
      default:
        return <CloudUploadIcon />;
    }
  };

  // get the right body content based on variant
  const getBody = () => {
    switch (variant) {
      case "success":
        return (
          <>
            <h4 className="dropzone-title">{fileName}</h4>
            <p className="dropzone-body text-label">{fileSize} MB</p>
          </>
        );
      case "error":
        return (
          <>
            <h4 className="dropzone-title text-error">Uploading Failed</h4>
            <p className="dropzone-body">{error}</p>
          </>
        );
      default:
        return (
          <>
            <h4 className="dropzone-title">{isDragActive ? "Drop your files here..." : "Drag and drop your file here"}</h4>
            <div className="upload-divider-container">
              <span className="divider-bar" />
              <span className="divider-text">Or</span>
              <span className="divider-bar" />
            </div>
          </>
        );
    }
  };

  return (
    <MuiThemeProvider theme={theme}>
      <div className={`dropzone-container dropzone-${variant}`} {...getRootProps()}>
        {getIcon()}
        {getBody()}
        <label htmlFor="input-upload">
          <input hidden {...getInputProps()} />
          <Button disableElevation className="modal-btn" color="primary" variant="contained">
            Browse File
          </Button>
        </label>
        <p className="upload-label">Maximum file size is 15MB</p>
      </div>
    </MuiThemeProvider>
  );
};

export default FileDropzone;
