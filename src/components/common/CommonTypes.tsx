/**
 * Type of selection in Status section
 */
export type FilterCheckboxSelectionType = {
  title: string;
  checked: boolean;
};

/**
 * Type of one-per-time filter section
 */
export type FilterChooseSelectionType = {
  selected: number;
  selections: Array<string>;
  translations: Array<string>;
};

// TODO Please make change to these if it's not correct
export type LoanOfficerType = {
  name: string;
  avatar?: string; // The url to the avatar
  busyLevel?: 1 | 2 | 3; // TODO change this if needed: 1, 2, 3 corresponding to green, yellow, red
  selected: boolean;
  id: string;
};

/**
 * Indicates all of filter sections
 */
export enum FilterSections {
  STATUS = "Status",
  ASSIGNED_TO = "Assigned To",
  BEFORE = "Before",
  AFTER = "After",
  SORT_BY = "Sort By"
}
