import React, { ReactNode, useState } from "react";
import "./Table.scss";
import {
  createMuiTheme,
  IconButton,
  MuiThemeProvider,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  withStyles,
  TableSortLabel
} from "@material-ui/core";
import PropTypes from "prop-types";
import { KeyboardArrowLeft, KeyboardArrowRight } from "@material-ui/icons";

/**
 * Props for table configuration
 * @template T
 * @typedef {object} TableProps<T>
 * @property {ColumnType<T>[]} columns - column definitions
 * @property {T[]} data - data to be displayed
 * @property {{ onPaginate: (direction: number) => void, totalRows: number, perPage: number, page: number,}} pagination - pagination definition
 * @property {{ defaultIndex?: number, defaultDirection?: "asc" | "desc" }} [sort] - sorting definition
 */
export type TableProps<T> = {
  columns: ColumnType<T>[];
  data: T[];
  pagination?: {
    onPaginate: (direction: number) => void;
    totalRows: number;
    perPage: number;
    page: number;
  };
  sort?: {
    defaultIndex?: number;
    defaultDirection?: "asc" | "desc";
  };
};

/**
 * Definition of a single column
 * @template T
 * @typedef {object} ColumnType
 * @property {string} title - title of the column
 * @property {(direction: "asc" | "desc") => void} [onSort] - sort handler, receives direction
 * @property {(record: T, index?: number) => ReactNode | string} render - customer render for cells of this column
 */
export type ColumnType<T> = {
  title: string;
  onSort?: (direction: "asc" | "desc") => void;
  render: (record: T, index: number) => ReactNode | string;
};

/**
 * Custom MUI Theme for component
 */
const theme = createMuiTheme({
  typography: {
    fontFamily: "Lato",
    fontSize: 16
  }
});

/**
 * Customized Table header row
 */
const TableHeaderCell = withStyles(() => ({
  root: {
    paddingLeft: 43,
    paddingRight: 43,
    fontWeight: 700,
    letterSpacing: 0.5
  }
}))(TableCell);

/**
 * Renders Applications Table Pagination widget
 */
function TablePaginationActions(props) {
  const { page, onPageChange, count, rowsPerPage } = props;

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  return (
    <div className="page-buttons">
      <IconButton
        onClick={handleBackButtonClick}
        aria-label="previous-page"
        disabled={page === 0}
        className="arrow"
        data-testid="prevPage"
      >
        <KeyboardArrowLeft className="icon" />
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        aria-label="Next-page"
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        className="arrow"
        data-testid="nextPage"
      >
        <KeyboardArrowRight className="icon" />
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired
};

/**
 * Custom table component
 * @param {TableProps} props - Table props
 * @returns {ReactNode} Table Component
 */
function MyTable<T extends object = any>(props: TableProps<T>) {
  const [sortIndex, setSortIndex] = useState(props.sort?.defaultIndex || 0);
  const [sortDirection, setSortDirection] = useState(props.sort?.defaultDirection || "desc");
  return (
    <MuiThemeProvider theme={theme}>
      <div className="container">
        <TableContainer>
          <Table className="table" aria-label="table" data-testid="table">
            <TableHead>
              <TableRow className="row" data-testid="header">
                {props.columns.map(({ title, onSort }, index) => {
                  const isActive = sortIndex === index;
                  return (
                    <TableHeaderCell
                      className="header-cell"
                      key={title + index}
                      sortDirection={isActive ? sortDirection : false}
                    >
                      {onSort ? (
                        <TableSortLabel
                          active={isActive}
                          direction={isActive ? sortDirection : "asc"}
                          onClick={() => {
                            let newDirection = props.sort?.defaultDirection || "desc";
                            if (isActive) {
                              newDirection = sortDirection === "asc" ? "desc" : "asc";
                            }
                            onSort(newDirection);
                            setSortDirection(newDirection);
                            setSortIndex(index);
                          }}
                        >
                          {title}
                        </TableSortLabel>
                      ) : (
                        title
                      )}
                    </TableHeaderCell>
                  );
                })}
              </TableRow>
            </TableHead>
            <TableBody>
              {props.data?.map((row, index) => (
                <TableRow className="row" hover key={index}>
                  {props.columns.map(({ render }, i) => (
                    <TableCell className="cell" key={`${index}-${i}`}>
                      {render(row, index)}
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        {props.pagination ? (
          <table className="page-row" data-testid="pagination">
            <tbody>
              <tr>
                <TablePagination
                  className="pagination"
                  count={props.pagination.totalRows}
                  rowsPerPage={props.pagination.perPage}
                  labelRowsPerPage=""
                  rowsPerPageOptions={[]}
                  page={props.pagination.page - 1}
                  onPageChange={(_, page) => props.pagination?.onPaginate(page + 1)}
                  ActionsComponent={TablePaginationActions}
                />
              </tr>
            </tbody>
          </table>
        ) : null}
      </div>
    </MuiThemeProvider>
  );
}

export default MyTable;
