import React from "react";

const ProgressBar = (props) => {
  const { bgcolor, completed, secondCompleted } = props;

  const containerStyles = {
    height: 10,
    width: "100%",
    backgroundColor: "#FFFFFF",
    margin: 50
  };

  const fillerStyles = {
    height: "100%",
    width: `${secondCompleted}%`,
    backgroundColor: "#6C6C6C"
  };

  const labelStyles = {
    padding: 5,
    color: "white",
    fontWeight: "bold"
  };

  return (
    <div style={containerStyles}>
      <div style={fillerStyles}></div>
      <div
        style={{ height: "100%", width: `${completed}%`, backgroundColor: bgcolor, bottom: "10px", position: "relative" }}
      ></div>
    </div>
  );
};

export default ProgressBar;
