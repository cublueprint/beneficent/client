import React from "react";
import { Dialog, DialogTitle, DialogContent, DialogContentText, IconButton, DialogActions, Button } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import "./Dialogue.scss";

const Title = (props) => {
  const { title } = props;
  const onClose = () => {
    props.onClose();
  };

  return (
    <DialogTitle id="simple-dialog-title">
      {title}
      {onClose ? (
        <IconButton aria-label="close" onClick={onClose} style={{ position: "absolute", right: 8, top: 8 }}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

interface DialoguePropsType {
  open: boolean;
  title: string;
  text: string;
  hasActions?: boolean;
  hasClose?: boolean;
  onClose?: Function;
  onConfirm?: Function;
}

export default function Dialogue({
  title,
  text,
  open,
  hasActions = false,
  hasClose = false,
  onClose,
  onConfirm
}: DialoguePropsType) {
  const [isOpen, setOpen] = React.useState<boolean>(open);

  React.useEffect(() => {
    setOpen(open);
  }, [open]);

  const handleClose = () => {
    setOpen(false);
    if (onClose) {
      onClose();
    }
  };

  // TODO: Check if it works
  const handleConfirm = () => {
    handleClose();
    if (onConfirm) {
      onConfirm();
    }
  };

  return (
    <Dialog className="dialog" onClose={handleClose} aria-labelledby="simple-dialog-title" open={isOpen} maxWidth={false}>
      <Title title={title} onClose={handleClose} />
      <DialogContent className="content">
        <DialogContentText style={{ whiteSpace: "pre", width: "650px", height: "30px" }}>{text}</DialogContentText>
      </DialogContent>
      {hasActions ? (
        <DialogActions>
          <Button className="button" onClick={handleClose} style={{ backgroundColor: "#FFFFFF", color: "#272727" }}>
            Cancel
          </Button>
          <Button className="button" onClick={handleConfirm} style={{ backgroundColor: "#272727", color: "#FFFFFF" }}>
            Confirm
          </Button>
        </DialogActions>
      ) : null}
      {hasClose ? (
        <DialogActions>
          <Button className="button" onClick={handleClose} style={{ backgroundColor: "#272727", color: "#FFFFFF" }}>
            Close
          </Button>
        </DialogActions>
      ) : null}
    </Dialog>
  );
}
