import { Avatar, Divider, FormControl, InputBase, MenuItem, Select, withStyles } from "@material-ui/core";
import "./LoanOfficerDropdown.scss";
import { ExpandMore } from "@material-ui/icons";
import React from "react";

/**
 * Props for loan officer dropdown
 */
type LoanOfficerDropdownProps = {
  officers: {
    workload: number;
    user: {
      role: string;
      isEmailVerified: boolean;
      firstName: string;
      lastName: string;
      phoneNumber: string;
      address: string;
      country: string;
      email: string;
      createdAt: string;
      updatedAt: string;
      id: string;
    };
    createdAt: string;
    updatedAt: string;
    id: string;
  }[];
  value: string;
  currentUser?: string;
  onChange: (loanOfficerId: string) => void;
  disabled?: boolean;
};

/**
 * Customized Drop Down Input for AssignedTo and Status fields
 */
const AssignedToInput = withStyles((theme) => ({
  input: {
    width: 127,
    height: 16,
    border: "1px solid rgb(0, 0, 0, 0.12)",
    borderRadius: 4,
    fontSize: 16,
    color: "black",
    paddingLeft: 11,
    paddingBottom: 10
  }
}))(InputBase);

const StyledAvatar = withStyles(() => ({
  root: {
    height: "24px",
    width: "24px",
    fontSize: "14px"
  }
}))(Avatar);
/**
 * Renders Loan Officer dropdown to assign a loan officer to an
 *  application
 */
const LoanOfficerDropdown: React.FC<LoanOfficerDropdownProps> = (props) => {
  const { officers: loanOfficers, onChange, value, currentUser, disabled } = props;

  const convertBusyness = (busyness) => {
    if (busyness === "Unassigned") return "#C4C4C4";

    if (busyness < 33) return "#568558";

    if (busyness < 66) return "#D79746";

    return "#DA6E5D";
  };

  const me = props.officers?.find((officer) => officer.user.id === currentUser);
  if (me) me.user.firstName = "Me";

  const renderContent = (firstName, workload) => (
    <div className="profile">
      <span data-testid={firstName}>{firstName}</span>
      <StyledAvatar style={{ backgroundColor: convertBusyness(workload) }}>{Math.round(workload)}</StyledAvatar>
    </div>
  );

  const renderItem = (loanOfficer) => (
    <MenuItem
      style={{ height: "52px", width: 172 }}
      value={loanOfficer.id}
      data-testid={loanOfficer.id}
      onClick={() => onChange(loanOfficer.id)}
      key={loanOfficer.id}
    >
      {renderContent(loanOfficer.user.firstName, loanOfficer.workload)}
    </MenuItem>
  );

  return (
    <FormControl variant="outlined">
      <Select
        data-testid="lo-select"
        value={value}
        input={<AssignedToInput />}
        inputProps={{
          classes: {
            icon: "dropdown-icon"
          }
        }}
        displayEmpty
        autoWidth
        renderValue={value === "" ? () => renderContent("Unassigned", "Unassigned") : undefined}
        IconComponent={ExpandMore}
        disabled={disabled}
      >
        {me && renderItem(me)}
        {loanOfficers
          ? loanOfficers.map((loanOfficer) => loanOfficer.user.id !== currentUser && renderItem(loanOfficer))
          : null}
      </Select>
    </FormControl>
  );
};

export default LoanOfficerDropdown;
