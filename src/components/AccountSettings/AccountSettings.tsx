import React from "react";

import { Grid, Paper } from "@material-ui/core";

import "./AccountSettings.scss";
import ProfileBox from "./ProfileBox/ProfileBox";
import PreferencesBox from "./PreferencesBox/PreferencesBox";
import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { TitleTypography } from "../HomePage/AdminHomePage/AdminHomePage";
import { apiUsers } from "../../services/api/users/apiUsers";

/**
 * Preferences Box Form values type
 */
type PreferencesBoxValues = {
  email: string;
  password: string;
  newPassword: string;
  newPasswordConfirmation: string;
};

/**
 * Profile Box Form values type
 */
type ProfileBoxValues = {
  firstName: string;
  lastName: string;
  address: string;
  phoneNumber: string;
};

/**
 * AccountSettings Component to display profile, preferences, contact details
 * @param props AccountSettingsProps
 */
const AccountSettings = () => {
  const accessToken = localStorage.getItem("accessToken");
  const userId = localStorage.getItem("id");

  const [profileInitValues, setProfileInitValues] = React.useState({
    firstName: "",
    lastName: "",
    address: "",
    phoneNumber: ""
  });

  const [preferencesInitValues, setPreferencesInitValues] = React.useState({
    email: "",
    password: "",
    newPassword: "",
    newPasswordConfirmation: ""
  });

  // const history = useHistory();

  // retrieve user data using access token
  React.useEffect(() => {
    if (accessToken) {
      // if access token is valid, get data
      const options: AxiosRequestConfig = {
        // TODO: Change this url to the GET for user profile data, get avatar
        url: `${process.env.REACT_APP_API_ENDPOINT || ""}/api/v1/users/${userId}`,
        method: "get",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${accessToken}`
        }
      };
      axios(options)
        .then((res) => {
          const data = res.data;
          // TODO change these field based on the response of the API call
          setProfileInitValues({
            firstName: data.firstName,
            lastName: data.lastName,
            address: data.address,
            phoneNumber: data.phoneNumber
          });

          setPreferencesInitValues({
            email: data.email,
            password: "supersecret",
            newPassword: "",
            newPasswordConfirmation: ""
          });
          console.log("set");
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [accessToken]);

  // TODO change these 2 functions and their signatures in the boxes based on API endpoints
  const handleUpdateProfile = (values: ProfileBoxValues) => {
    return new Promise<AxiosResponse>((resolve) => {
      setTimeout(() => {
        apiUsers.patch(userId!, {
          users: values
        });
      }, 1000);
    });
  };

  const handleUpdatePreferences = (values: PreferencesBoxValues) => {
    return new Promise<AxiosResponse>((resolve) => {
      setTimeout(() => {
        apiUsers.patch(userId!, {
          users: {
            currentPassword: values.password,
            password: values.newPassword
          }
        });
      }, 1000);
    });
  };

  return (
    <Grid className="account-settings-container" container direction="column">
      <Grid item>
        <TitleTypography variant="h4" color="textPrimary" className="header">
          Account Settings
        </TitleTypography>
      </Grid>
      <Grid spacing={3} container item direction="row">
        <Grid item container alignItems="center" direction="column" lg={8} spacing={3}>
          <Grid item className="box">
            <ProfileBox initialValues={profileInitValues} handleUpdateProfile={handleUpdateProfile} />
          </Grid>
          <Grid item className="box">
            <PreferencesBox initialValues={preferencesInitValues} handleUpdatePreferences={handleUpdatePreferences} />
          </Grid>
        </Grid>
        <Grid item className="box" lg>
          {/* TODO Replace the following Paper by the <NotificationsBox /> component */}
          <Paper style={{ width: "100%", height: 250 }}>Notification Box</Paper>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default AccountSettings;
