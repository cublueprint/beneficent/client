import {
  Card,
  CardContent,
  CardHeader,
  FilledInput,
  InputAdornment,
  Button,
  createMuiTheme,
  ThemeProvider,
  CircularProgress,
  FormHelperText,
  IconButton
} from "@material-ui/core";
import { useState } from "react";
import { AlternateEmail, Lock, Visibility, VisibilityOff } from "@material-ui/icons";
import { Formik, FormikErrors, FormikTouched } from "formik";
import * as Yup from "yup";
import * as React from "react";
import "./PreferencesBox.scss";
import { AxiosResponse } from "axios";

/**
 * Enum contains different type of new password field's error
 */
enum PasswordFieldErrors {
  EIGHT_CHARACTERS = "8 characters",
  LETTER = "letter",
  NUMBER = "number"
}

/**
 * Theme to remove underline and add padding to inputs,
 * include custom primary color to buttons
 */
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#2189EA"
    }
  },
  overrides: {
    MuiFilledInput: {
      input: {
        padding: "20px 0"
      }
    },
    MuiOutlinedInput: {
      input: {
        padding: "20px 0"
      }
    },
    MuiButton: {
      outlined: {
        color: "#2189EA"
      }
    },
    MuiCard: {
      root: {
        padding: "12px"
      }
    }
  }
});

/**
 * Preferences Box Form values type
 */
type PreferencesBoxValues = {
  email: string;
  password: string;
  newPassword: string;
  newPasswordConfirmation: string;
};

/**
 * Props for profile box
 */
type PreferencesBoxProps = {
  /**
   * Initial values for fields
   */
  initialValues: PreferencesBoxValues;

  /**
   * Function to handle profile save, returns promise
   */
  handleUpdatePreferences: (values: PreferencesBoxValues) => Promise<AxiosResponse>;
};

/**
 * Profile Box Component: Form to update profile fields
 * @param param0 ProfileBoxProps
 */
const PreferencesBox: React.FC<PreferencesBoxProps> = ({ initialValues, handleUpdatePreferences }) => {
  const [editing, setEditing] = useState(false);
  const [showingPassword, setShowingPassword] = useState({
    current: false,
    new: false,
    confirmNew: false
  });

  /**
   * Get an array of error messages of a text field as "path"
   * @param err an object of type Yup.ValidationError that contain validation errors of the password fields
   * @param path a string represent the name of the field
   */
  const getValidationErrorMessages = (err: Yup.ValidationError, path: string) => {
    return err.inner
      .filter((error: Yup.ValidationError) => error.path === path)
      .map((error: Yup.ValidationError) => error.errors)
      .map((message: Array<String>) => message[0]);
  };

  /**
   * The form validation schema for new password field
   */
  const schema = Yup.object().shape({
    email: Yup.string().email("Email must be valid email").required("Email is required"),
    password: Yup.string().required("Current password is required"),
    newPassword: Yup.string()
      .required("New password is required")
      .min(8, PasswordFieldErrors.EIGHT_CHARACTERS)
      .matches(/^(?=.*[a-zA-Z])/, PasswordFieldErrors.LETTER)
      .matches(/^(?=.*\d)/, PasswordFieldErrors.NUMBER),
    newPasswordConfirmation: Yup.string()
      .required("New password confirmation is required")
      .oneOf([Yup.ref("newPassword"), ""], "Passwords do not match")
  });

  /**
   * Validate the schema and return errors if there's any
   * @param values the values of the form's fields
   */
  const validateSchema = (values: PreferencesBoxValues): Yup.InferType<typeof schema> => {
    return schema
      .validate(values, {
        abortEarly: false
      })
      .then((res) => {
        return;
      })
      .catch((err) => {
        return {
          email: getValidationErrorMessages(err, "email"),
          password: getValidationErrorMessages(err, "password"),
          newPassword: getValidationErrorMessages(err, "newPassword"),
          newPasswordConfirmation: getValidationErrorMessages(err, "newPasswordConfirmation")
        };
      });
  };

  /**
   * The array of the basic requirements for new password
   */
  const newPasswordRequirements = [
    {
      errorType: PasswordFieldErrors.EIGHT_CHARACTERS,
      message: "Must be at least 8 characters long\n"
    },
    {
      errorType: PasswordFieldErrors.LETTER,
      message: "Must contain at least one letter\n"
    },
    {
      errorType: PasswordFieldErrors.NUMBER,
      message: "Must contain at least one number\n"
    }
  ];

  /**
   * Render the helper texts for new password field
   */
  const renderNewPasswordRequirement = (
    errors: FormikErrors<PreferencesBoxValues>,
    touched: FormikTouched<PreferencesBoxValues>
  ) => {
    return newPasswordRequirements.map((requirement, index) => (
      <FormHelperText
        key={index}
        error={touched.newPassword && errors.newPassword?.includes(requirement.errorType)}
        style={{ marginTop: -5 }}
      >
        {requirement.message}
      </FormHelperText>
    ));
  };

  return (
    <ThemeProvider theme={theme}>
      <Card>
        <CardHeader
          titleTypographyProps={{
            variant: "h6",
            style: {
              fontWeight: 700
            }
          }}
          title="Preferences"
          subheader="Edit your preference details here"
        />
        <CardContent>
          <Formik
            onSubmit={(values, { setSubmitting, setFieldValue }) => {
              handleUpdatePreferences(values).then((res) => {
                setFieldValue("password", values.newPassword);
                setFieldValue("newPassword", "");
                setFieldValue("newPasswordConfirmation", "");
                setSubmitting(false);
                setEditing(false);
                setShowingPassword({ ...showingPassword, current: false });
              });
            }}
            enableReinitialize
            initialValues={{
              ...initialValues,
              newPassword: "",
              newPasswordConfirmation: ""
            }}
            validate={validateSchema}
            validateOnChange={false}
          >
            {(props) => {
              const {
                values,
                touched,
                errors,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit,
                submitForm,
                setFieldValue
              } = props;

              return (
                <form onSubmit={handleSubmit}>
                  <FilledInput
                    startAdornment={
                      <InputAdornment position="start">
                        <AlternateEmail />
                      </InputAdornment>
                    }
                    name="email"
                    value={values.email}
                    fullWidth
                    disabled={!editing}
                    placeholder="Email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={!!(touched.email && errors.email)}
                  />
                  <FilledInput
                    startAdornment={
                      <InputAdornment position="start">
                        <Lock />
                      </InputAdornment>
                    }
                    endAdornment={
                      editing ? (
                        <InputAdornment position="start">
                          <IconButton
                            onClick={() => setShowingPassword({ ...showingPassword, current: !showingPassword.current })}
                            edge="end"
                          >
                            {showingPassword.current ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      ) : null
                    }
                    name="password"
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={!!(touched.password && errors.password && errors.password.length > 0)}
                    fullWidth
                    type={showingPassword.current ? "text" : "password"}
                    placeholder="Password"
                    disabled={!editing}
                  />
                  {editing ? (
                    <>
                      <FilledInput
                        startAdornment={
                          <InputAdornment position="start">
                            <Lock />
                          </InputAdornment>
                        }
                        endAdornment={
                          <InputAdornment position="start">
                            <IconButton
                              onClick={() => setShowingPassword({ ...showingPassword, new: !showingPassword.new })}
                              edge="end"
                            >
                              {showingPassword.new ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                          </InputAdornment>
                        }
                        name="newPassword"
                        type={showingPassword.new ? "text" : "password"}
                        value={values.newPassword}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={!!(touched.newPassword && errors.newPassword && errors.newPassword.length > 0)}
                        fullWidth
                        placeholder="New Password"
                        disabled={!editing}
                      />

                      <div data-testid="helper-text-field" className="new-password-requirements">
                        {renderNewPasswordRequirement(errors, touched)}
                      </div>

                      <FilledInput
                        startAdornment={
                          <InputAdornment position="start">
                            <Lock />
                          </InputAdornment>
                        }
                        endAdornment={
                          <InputAdornment position="start">
                            <IconButton
                              onClick={() => {
                                setShowingPassword({ ...showingPassword, confirmNew: !showingPassword.confirmNew });
                              }}
                              edge="end"
                            >
                              {showingPassword.confirmNew ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                          </InputAdornment>
                        }
                        name="newPasswordConfirmation"
                        type={showingPassword.confirmNew ? "text" : "password"}
                        value={values.newPasswordConfirmation}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={
                          !!(
                            touched.newPasswordConfirmation &&
                            errors.newPasswordConfirmation &&
                            errors.newPasswordConfirmation.length > 0
                          )
                        }
                        fullWidth
                        placeholder="New Password Confirmation"
                        disabled={!editing}
                      />

                      <FormHelperText
                        error={
                          !!(
                            touched.newPasswordConfirmation &&
                            errors.newPasswordConfirmation &&
                            errors.newPasswordConfirmation.length > 0
                          )
                        }
                        style={{ position: "absolute" }}
                      >
                        {touched.newPasswordConfirmation && errors.newPasswordConfirmation
                          ? errors.newPasswordConfirmation[0]
                          : ""}
                      </FormHelperText>
                    </>
                  ) : null}

                  {editing && (
                          <Button
                          variant = {"contained"}
                          onClick = {() => (setEditing(false))}
                          color = {"primary"}
                          style={{
                            boxShadow: "none",
                            float: "right",
                            margin: "16px 32px"
                          }}
                          >
                            Cancel
                          </Button>
                    )}

                  <Button // change button depending on if currently editing
                    variant={editing ? "contained" : "outlined"}
                    onClick={() => {
                      if (editing) {
                        
                        setEditing(false);
                        submitForm();
                      } else {
                        
                        setEditing(true);
                        setFieldValue("password", "");
                      }
                    }}
                    color={editing ? "primary" : "default"}
                    style={{
                      boxShadow: "none",
                      float: "right",
                      margin: "16px 32px"
                    }}
                    
                  >
                    {editing ? "Save" : "Edit"}
                  </Button>
                </form>
              );
            }}
          </Formik>
        </CardContent>
      </Card>
    </ThemeProvider>
  );
};

export default PreferencesBox;
