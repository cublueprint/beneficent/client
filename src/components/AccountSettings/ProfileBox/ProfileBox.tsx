import {
  Avatar,
  Card,
  CardContent,
  CardHeader,
  FilledInput,
  InputAdornment,
  MenuItem,
  Select,
  Button,
  createMuiTheme,
  ThemeProvider,
  CircularProgress
} from "@material-ui/core";
import { AccountCircle, Map, Phone, Room } from "@material-ui/icons";
import { Formik } from "formik";
import * as Yup from "yup";
import * as React from "react";
import "./ProfileBox.scss";
import { AxiosResponse } from "axios";

/**
 * Theme to remove underline and add padding to inputs,
 * include custom primary color to buttons
 */
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#2189EA"
    }
  },
  overrides: {
    MuiFilledInput: {
      input: {
        padding: "20px 0"
      }
    },
    MuiOutlinedInput: {
      input: {
        padding: "20px 0"
      }
    },
    MuiButton: {
      outlined: {
        color: "#2189EA"
      }
    },
    MuiCard: {
      root: {
        padding: "12px"
      }
    }
  }
});

/**
 * Profile Box Form values type
 */
type ProfileBoxValues = {
  firstName: string;
  lastName: string;
  address: string;
  phoneNumber: string;
};

/**
 * Props for profile box
 */
type ProfileBoxProps = {
  /**
   * Initial values for fields
   */
  initialValues: ProfileBoxValues;
  /**
   * Avatar url/path
   */
  avatar?: string;
  /**
   * Function to handle profile save, returns promise
   */
  handleUpdateProfile: (values: ProfileBoxValues) => Promise<AxiosResponse>;
};

const schema = Yup.object().shape({
  firstName: Yup.string().required("First name is required"),
  lastName: Yup.string().required("Last name is required"),
  address: Yup.string().required("Address is required"),
  phoneNumber: Yup.string()
    .matches(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/g, "Please enter a valid phone number")
    .required("Phone number is required")
});

const getValidationErrorMessages = (err: Yup.ValidationError, path: string) => {
  return err.inner
    .filter((error: Yup.ValidationError) => error.path === path)
    .map((error: Yup.ValidationError) => error.errors)
    .map((message: Array<String>) => message[0]);
};

/**
 * Profile Box Component: Form to update profile fields
 * @param param0 ProfileBoxProps
 */
const ProfileBox: React.FC<ProfileBoxProps> = ({ initialValues, avatar, handleUpdateProfile }) => {
  const [editing, setEditing] = React.useState(false);

  return (
    // apply custom theme
    <ThemeProvider theme={theme}>
      <Card>
        <CardHeader
          avatar={
            <Avatar src={avatar} alt="Your Avatar">
              {avatar && "NA"}
            </Avatar>
          }
          titleTypographyProps={{
            variant: "h6",
            style: {
              fontWeight: 700
            }
          }}
          title="Profile"
          subheader="Edit your profile details here"
        />
        <CardContent>
          <Formik
            onSubmit={(values, { setSubmitting }) => {
              handleUpdateProfile(values).then((res) => {
                setSubmitting(false);
                setEditing(false);
              });
            }}
            enableReinitialize
            initialValues={initialValues}
            validationSchema={schema}
            validateOnChange = {false}
          >
            {(props) => {
              const { values, touched, errors, isSubmitting, handleChange, handleBlur, handleSubmit, submitForm } = props;

              return (
                <form onSubmit={handleSubmit}>
                  <FilledInput
                    startAdornment={
                      <InputAdornment position="start">
                        <AccountCircle />
                      </InputAdornment>
                    }
                    name="firstName"
                    value={values.firstName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={!!(touched.firstName && errors.firstName)}
                    fullWidth
                    placeholder="First Name"
                    disableUnderline={!(touched.firstName && errors.firstName)}
                    disabled={!editing}
                  />
                  <FilledInput
                    startAdornment={
                      <InputAdornment position="start">
                        <AccountCircle />
                      </InputAdornment>
                    }
                    name="lastName"
                    value={values.lastName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={!!(touched.lastName && errors.lastName)}
                    fullWidth
                    placeholder="Last Name"
                    disableUnderline={!(touched.lastName && errors.lastName)}
                    disabled={!editing}
                  />
                  <FilledInput
                    startAdornment={
                      <InputAdornment position="start">
                        <Room />
                      </InputAdornment>
                    }
                    name="address"
                    value={values.address}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={!!(touched.address && errors.address)}
                    fullWidth
                    placeholder="Address"
                    disableUnderline={!(touched.address && errors.address)}
                    disabled={!editing}
                  />
                  <FilledInput
                    startAdornment={
                      <InputAdornment position="start">
                        <Phone />
                      </InputAdornment>
                    }
                    name="phoneNumber"
                    value={values.phoneNumber}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={!!(touched.phoneNumber && errors.phoneNumber)}
                    fullWidth
                    placeholder="Phone Number"
                    disableUnderline={!(touched.phoneNumber && errors.phoneNumber)}
                    disabled={!editing}
                  />
                  {editing && (
                    <Button
                      variant={"contained"}
                      onClick={() => setEditing(false)}
                      color={"primary"}
                      style={{
                        boxShadow: "none",
                        float: "right",
                        margin: "16px 32px"
                      }}
                    >
                      Cancel
                    </Button>
                  )}
                  <Button // change button depending on if currently editing
                    variant={editing ? "contained" : "outlined"}
                    onClick={() => {
                      if (editing) {
                        setEditing(false);
                        handleUpdateProfile(values).then((res) => {
                          
                          
                        });

                      } else {
                        
                        setEditing(true);
                      }
                    }}
                    color={editing ? "primary" : "default"}
                    style={{
                      boxShadow: "none",
                      float: "right",
                      margin: "16px 32px"
                    }}
                    
                  >
                    {editing ? "Save" : "Edit"}
                  </Button>
                </form>
              );
            }}
          </Formik>
        </CardContent>
      </Card>
    </ThemeProvider>
  );
};

export default ProfileBox;
