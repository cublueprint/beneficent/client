import * as React from "react";
import { useState, useEffect } from "react";
import {
  Grid,
  Card,
  CardHeader,
  CardContent,
  Typography,
  TextField,
  RadioGroup,
  Radio,
  FormControlLabel,
  Select,
  MenuItem,
  Button,
  withStyles,
  makeStyles,
  FormHelperText
} from "@material-ui/core";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { Formik } from "formik";
import * as Yup from "yup";
import NumberFormat from "react-number-format";
import "../../ApplicationsLayout/CreateApplication/CreateApplication.scss";
import Dialogue from "../../common/Dialogue/Dialogue";
import MomentUtils from "@date-io/moment";
import { apiUsers } from "../../../services/api/users/apiUsers";
import { useHistory } from "react-router-dom";
/*
import ROUTES from "../../../routes";
import { useHistory } from "react-router-dom";
*/

/**
 * Props of FieldTitle
 */
interface FieldTitleProps {
  text: string;
  required?: boolean;
  bold?: boolean;
}

/**
 * Smaller title for small fields
 */
const FieldTitle = ({ text, required = false, bold = false }: FieldTitleProps) => (
  <Typography
    style={{ marginBottom: 7, fontWeight: bold ? "bold" : 400 }}
    className={`field-title ${required ? "required" : ""}`}
  >
    {text}
  </Typography>
);

/**
 * FormControlLabel with height/margin changed
 */
const StyledFormControlLabel = withStyles({
  root: {
    height: 30,
    marginRight: 40
  }
})(FormControlLabel);

/**
 * Radio button colored as design
 */
const ColoredRadio = withStyles({
  root: {
    "&$checked": {
      color: "#DA6E5D",
      "&$disabled": {
        color: "rgba(30, 105, 255, 0.38)"
      }
    }
  },
  checked: {},
  disabled: {}
})((props: any) => <Radio color="default" {...props} />);

/**
 * Props of StyledSelect
 */
interface StyledSelectProps {
  name: string;
  error?: boolean;
  maxWidth?: number;
  currentValue: string | undefined | null;
  selections: Array<string>;
  disabled?: boolean;
  maxItemShowed?: number;
  onChange: (event: React.ChangeEvent<{ value: unknown }>) => void;
  onBlur: (event: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
}

/**
 * A styled Select component with custom width
 * @param param0 StyledSelectProps
 */
const StyledSelect = ({
  name,
  error,
  currentValue,
  selections,
  disabled,
  maxItemShowed = 6,
  onChange,
  onBlur
}: StyledSelectProps) => {
  const itemHeight = 55;

  return (
    <Select
      fullWidth
      name={name}
      error={error}
      style={{ maxHeight: 56, padding: 0 }}
      disabled={disabled}
      value={currentValue}
      variant="outlined"
      onChange={onChange}
      onBlur={onBlur}
      MenuProps={{
        getContentAnchorEl: null,
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "left"
        },
        PaperProps: {
          style: {
            maxHeight: maxItemShowed * itemHeight,
            padding: 0
          }
        },
        MenuListProps: { disablePadding: true }
      }}
    >
      {selections.map((selection, index) => (
        <MenuItem
          style={{ height: itemHeight, borderBottom: "1px solid rgba(33, 33, 33, 0.08)" }}
          key={index}
          value={selection}
        >
          {selection}
        </MenuItem>
      ))}
    </Select>
  );
};

interface CheckFieldsLabelProps {
  label: string;
  error: boolean;
}

const CheckFieldsLabel = ({ label, error }: CheckFieldsLabelProps) => (
  <Typography color={error ? "error" : "textPrimary"} variant="body2">
    {label}
  </Typography>
);

interface NumberFormatProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

function PhoneFormat(props: NumberFormatProps) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      format="(###)-###-####"
      placeholder="(___)-___-____"
      mask={["_", "_", "_", "_", "_", "_", "_", "_", "_", "_"]}
      isNumericString
    />
  );
}

interface ErrorTextProps {
  error: string | undefined;
}

const ErrorText = ({ error }: ErrorTextProps) => (
  <FormHelperText error={Boolean(error)} className={error ? "error-text" : "hidden"}>
    {error}
  </FormHelperText>
);

/**
 * Style override TextField
 */
const useStyle = makeStyles({
  textField: {
    "&$textFieldDisabled": {
      color: "rgba(0,0,0,0.85)"
    }
  },
  textFieldDisabled: {}
});

/**
 * Mockup type of full info
 */
type DetailsInfoType = {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  role: string;
  isLoanOfficer: boolean | null;
};

interface CreateAccountProps {
  /**
   * An optional prop that specifies if the page is for view only or for applying
   */
  permission?: boolean;
  viewOnly?: boolean;
  handleSubmitAccount?: Function;
}

// TODO This may change based on the design
const roleSelections = ["Admin", "User"];

/**
 * **Full** detail page of an application
 */
const CreateAccount: React.FC<CreateAccountProps> = (props) => {
  const accessToken = localStorage.getItem("accessToken");
  const classes = useStyle();
  const { viewOnly, permission } = props;

  const emptyInfo: DetailsInfoType = {
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    role: "",
    isLoanOfficer: null
  };

  const [successModal, setSuccessModal] = React.useState(false);
  const [confirmModal, setConfirmModal] = React.useState(false);
  const [errorModal, setErrorModal] = React.useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [duplicateEmail, setDuplicateEmail] = React.useState(false);
  let history = useHistory();

  useEffect(() => {
    if (!permission) {
      handleRedirect();
    }
  });

  const handleRedirect = () => {
    history.push("/manage-accounts");
  };
  const handleEmailChange = () => {
    setDuplicateEmail(false);
  };

  // POST user
  const postAccount = React.useCallback(
    (values) => {
      let payload = {
        firstName: values.firstName,
        lastName: values.lastName,
        email: values.email,
        phoneNumber: values.phone,
        role: `pending${values.role}`,
        isLoanOfficer: values.isLoanOfficer,
        address: null,
        country: null,
        password: null
      };

      apiUsers
        .post({
          users: payload
        })
        .then((res) => {
          if (res.code === undefined) {
            setSuccessModal(true);
          } else if (res.code === 400) {
            if (res.message === "Email already taken") {
              setDuplicateEmail(true);
              setErrorMessage("Email already taken");
            } else {
              setErrorMessage(res.message);
            }
            setErrorModal(true);
          }
        });
    },
    [accessToken]
  );

  return (
    <Formik
      onSubmit={(values, submitProps) => {
        setConfirmModal(true);
      }}
      enableReinitialize
      validateOnBlur={false}
      validateOnChange={false}
      initialValues={emptyInfo}
      validationSchema={Yup.object().shape({
        firstName: Yup.string().required("First name is required"),
        lastName: Yup.string().required("Last name is required"),
        phone: Yup.string().required("Phone number is required"),
        email: Yup.string().email("Email must be valid email").required("Email is required"),
        role: Yup.string().required("Permission is required"),
        isLoanOfficer: Yup.boolean().nullable().required("Is loan officer is required")
      })}
    >
      {(props) => {
        const { values, touched, errors, resetForm, setFieldValue, handleChange, handleBlur, handleSubmit } = props;

        return (
          <MuiPickersUtilsProvider utils={MomentUtils}>
            <form data-testid="form" className="create-application-form" onSubmit={handleSubmit}>
              <Card className="form-card">
                <div id="application-overview-header">
                  <CardHeader title={"User details"} />
                  <div>
                    <Button
                      id="app-ovr-view"
                      color="primary"
                      style={{ color: "white" }}
                      variant="contained"
                      type="submit"
                      onClick={() => {}}
                    >
                      Save
                    </Button>

                    {confirmModal ? (
                      <Dialogue
                        open={confirmModal}
                        title="Confirm Create New Account"
                        text={`Upon creating a new account, verification instructions will be sent to the user's email.`}
                        hasActions
                        onClose={() => {
                          setConfirmModal(false);
                        }}
                        onConfirm={() => {
                          setConfirmModal(false);
                          postAccount(values);
                        }}
                      />
                    ) : null}

                    {successModal ? (
                      <Dialogue
                        title={"Successful!"}
                        text={"Your account has been created."}
                        open={successModal}
                        hasClose
                        onClose={() => {
                          setSuccessModal(false);
                          setDuplicateEmail(false);
                          resetForm();
                        }}
                      />
                    ) : null}

                    {errorModal ? (
                      <Dialogue
                        title={"Error"}
                        text={`${errorMessage}`}
                        open={errorModal}
                        hasClose
                        onClose={() => {
                          setErrorModal(false);
                        }}
                      />
                    ) : null}
                  </div>
                </div>

                <CardContent>
                  <Grid container spacing={4}>
                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="First Name" required />
                        <TextField
                          data-testid="firstName"
                          name="firstName"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.firstName)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          InputProps={{
                            classes: {
                              root: classes.textField,
                              disabled: classes.textFieldDisabled
                            }
                          }}
                          value={values.firstName}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.firstName} />
                      </Grid>

                      <Grid item xs={4}>
                        <FieldTitle text="Last Name" required />
                        <TextField
                          data-testid="lastName"
                          name="lastName"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.lastName)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          InputProps={{
                            classes: {
                              root: classes.textField,
                              disabled: classes.textFieldDisabled
                            }
                          }}
                          value={values.lastName}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.lastName} />
                      </Grid>

                      <Grid item xs={2}></Grid>
                    </Grid>

                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="Phone Number" required />
                        <TextField
                          data-testid="phone"
                          type="tel"
                          name="phone"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.phone)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          InputProps={{
                            classes: {
                              root: classes.textField,
                              disabled: classes.textFieldDisabled
                            },
                            inputComponent: PhoneFormat as any
                          }}
                          value={values.phone.match(/\d/g)}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.phone} />
                      </Grid>

                      <Grid item xs={4}>
                        <FieldTitle text="Email" required />
                        <TextField
                          data-testid="email"
                          name="email"
                          variant="outlined"
                          fullWidth
                          error={Boolean(errors.email)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          onInput={handleEmailChange}
                          InputProps={{
                            classes: {
                              root: classes.textField,
                              disabled: classes.textFieldDisabled
                            }
                          }}
                          value={values.email}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.email} />
                        {duplicateEmail ? <ErrorText error={"The Email Address is already taken"} /> : null}
                      </Grid>

                      <Grid item xs={2}></Grid>
                    </Grid>

                    <Grid item container xs={12} justify="space-between">
                      <Grid item xs={4}>
                        <FieldTitle text="Permission" required />
                        <StyledSelect
                          name="role"
                          error={Boolean(errors.role)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          currentValue={values.role}
                          selections={roleSelections}
                          disabled={viewOnly}
                        />
                        <ErrorText error={errors.role} />
                      </Grid>

                      <Grid item xs={7} style={{ marginTop: "28px" }}>
                        <FieldTitle text="Add as loan officer?" required bold />
                        <RadioGroup row value={values.isLoanOfficer}>
                          <StyledFormControlLabel
                            name="isLoanOfficer"
                            value={true}
                            disabled={viewOnly}
                            control={<ColoredRadio onChange={() => setFieldValue("isLoanOfficer", true)} />}
                            label={<CheckFieldsLabel label="Yes" error={Boolean(errors.isLoanOfficer)} />}
                          />
                          <StyledFormControlLabel
                            name="isLoanOfficer"
                            value={false}
                            disabled={viewOnly}
                            control={<ColoredRadio onChange={() => setFieldValue("isLoanOfficer", false)} />}
                            label={<CheckFieldsLabel label="No" error={!!touched.isLoanOfficer && !!errors.isLoanOfficer} />}
                          />
                        </RadioGroup>
                        <ErrorText error={errors.isLoanOfficer} />
                      </Grid>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </form>
          </MuiPickersUtilsProvider>
        );
      }}
    </Formik>
  );
};

export default CreateAccount;
