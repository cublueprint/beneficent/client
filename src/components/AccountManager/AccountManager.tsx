import React from "react";
import { Button, Paper, InputAdornment, IconButton, TextField, Grid } from "@material-ui/core";
import { apiUsers } from "../../services/api/users/apiUsers";
import MyTable, { ColumnType } from "../common/Table/Table";
import { Add, Search as SearchIcon } from "@material-ui/icons";
import { TitleTypography } from "../HomePage/AdminHomePage/AdminHomePage";
import { Link as RouterLink, useHistory, Route, Switch, useRouteMatch } from "react-router-dom";
import CreateAccount from "./CreateAccount/CreateAccount";
import EditAccount from "./EditAccount/EditAccount";
import jwtDecode from "jwt-decode";
import Dialogue from "../common/Dialogue/Dialogue";

import "./AccountManager.scss";

type User = {
  role: string;
  isEmailVerified: boolean;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  address: string;
  country: string;
  email: string;
  createdAt: string;
  updatedAt: string;
  id: string;
};

const AccountManager = (props) => {
  const { path } = useRouteMatch();
  const accessToken = localStorage.getItem("accessToken");
  //Query for all users - admin, loan officers
  const [totalRows, setTotalRows] = React.useState<number>(0);
  const [data, setData] = React.useState<User[] | undefined>([]);
  const [page, setPage] = React.useState<number>(1);
  const [searchValue, setSearchValue] = React.useState<string>("");
  const [permission, setPermission] = React.useState(false);
  const currentUser = accessToken && jwtDecode<any>(accessToken).sub;
  let history = useHistory();
  const perPage = 5;

  const convertDateToString = (date) => {
    if (date !== undefined) {
      let monthMap = new Map();

      monthMap.set("01", "January");
      monthMap.set("02", "February");
      monthMap.set("03", "March");
      monthMap.set("04", "April");
      monthMap.set("05", "May");
      monthMap.set("06", "June");
      monthMap.set("07", "July");
      monthMap.set("08", "August");
      monthMap.set("09", "September");
      monthMap.set("10", "October");
      monthMap.set("11", "November");
      monthMap.set("12", "December");

      const extractedMonth = date.substring(5, 7);
      const extractedDay = date.substring(8, 10);
      const extractedYear = date.substring(0, 4);

      return monthMap.get(extractedMonth) + " " + extractedDay + ", " + extractedYear;
    }
    return "";
  };

  const onPaginate = (newPage) => {
    setPage(newPage);
  };

  React.useEffect(() => {
    apiUsers
      .getSingle(currentUser)
      .then((res) => {
        if (res.code === undefined) {
          setPermission(res.role === "admin");
        } else if (res.code === 404) {
          console.log("not found");
        }
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, [currentUser, accessToken]);

  React.useEffect(() => {
    apiUsers
      .getAll({
        users: {
          ...(searchValue && { nameOrEmail: searchValue }),
          limit: perPage,
          page: page
        }
      })
      .then((res) => {
        if (res.totalResults > 0) {
          setData(res.results);
          setTotalRows(res.totalResults);
        } else {
          setData(undefined);
          setTotalRows(0);
        }
      })
      .catch((err) => console.log(err));
  }, [searchValue, page]);

  const columns: ColumnType<any>[] = [
    {
      title: "Name",
      render: (record) => (
        <RouterLink to={path + "/" + record.id} className="link">
          {record.firstName + " " + record.lastName}
        </RouterLink>
      )
    },
    {
      title: "Email",
      render: (record) => record.email
    },
    {
      title: "Date Added",
      render: (record) => convertDateToString(record.createdAt)
    }
  ];

  const handleCreateUserClick = () => {
    if (permission) {
      history.push(`${path}/create`);
    }
  };

  return (
    <div>
      <Switch>
        <Route exact path={path}>
          <div id="account-manager-header">
            <TitleTypography variant="h4" color="textPrimary">
              Account Manager
            </TitleTypography>

            {permission && (
              <Button
                color="primary"
                style={{ color: "white" }}
                variant="contained"
                id="add-btn"
                onClick={() => {
                  handleCreateUserClick();
                }}
              >
                <Add style={{ marginRight: "5px" }} />
                Create User
              </Button>
            )}
          </div>
        </Route>
        <Route exact path={path + "/create"}>
          <TitleTypography variant="h4" color="textPrimary" className="header">
            Create New Account
          </TitleTypography>
        </Route>
      </Switch>

      <Switch>
        <Route exact path={path}>
          <Paper className="account-manager-container" elevation={0} square>
            <Grid className="table-filters-container" container direction="row" spacing={6}>
              <Grid item xs={5}>
                <div className="search-field-container">
                  <TextField
                    onChange={(event) => {
                      setSearchValue(event.target.value);
                      setPage(1);
                    }}
                    variant="outlined"
                    placeholder="Search by name or email"
                    className="search-field"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton>
                            <SearchIcon color="action" />
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                  />
                </div>
              </Grid>
            </Grid>
            <div className="account-manager-content">
              <MyTable
                data={data!}
                columns={columns}
                pagination={{
                  perPage,
                  onPaginate,
                  totalRows,
                  page: page
                }}
              />
            </div>
          </Paper>
        </Route>
        <Route exact path={path + "/create"}>
          <CreateAccount permission={permission} />
        </Route>
        <Route exact path={path + "/:id"}>
          <EditAccount />
        </Route>
      </Switch>
    </div>
  );
};

export default AccountManager;
