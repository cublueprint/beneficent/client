import * as React from "react";
import { useEffect, useState } from "react";
import {
  Grid,
  Card,
  CardHeader,
  CardContent,
  Typography,
  TextField,
  RadioGroup,
  Radio,
  FormControlLabel,
  Select,
  MenuItem,
  Button,
  withStyles,
  makeStyles,
  FormHelperText
} from "@material-ui/core";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { Formik } from "formik";
import * as Yup from "yup";
import NumberFormat from "react-number-format";
import "../../ApplicationsLayout/CreateApplication/CreateApplication.scss";
import Dialogue from "../../common/Dialogue/Dialogue";
import MomentUtils from "@date-io/moment";
import { apiUsers } from "../../../services/api/users/apiUsers";
import { useHistory, useParams } from "react-router-dom";
import jwtDecode from "jwt-decode";
import axios, { AxiosRequestConfig } from "axios";
import { TitleTypography } from "../../HomePage/AdminHomePage/AdminHomePage";
import "../AccountManager.scss";

/**
 * Props of FieldTitle
 */
interface FieldTitleProps {
  text: string;
  required?: boolean;
  bold?: boolean;
}

/**
 * Smaller title for small fields
 */
const FieldTitle = ({ text, required = false, bold = false }: FieldTitleProps) => (
  <Typography
    style={{ marginBottom: 7, fontWeight: bold ? "bold" : 400 }}
    className={`field-title ${required ? "required" : ""}`}
  >
    {text}
  </Typography>
);

/**
 * FormControlLabel with height/margin changed
 */
const StyledFormControlLabel = withStyles({
  root: {
    height: 30,
    marginRight: 40
  }
})(FormControlLabel);

/**
 * Radio button colored as design
 */
const ColoredRadio = withStyles({
  root: {
    "&$checked": {
      color: "#DA6E5D",
      "&$disabled": {
        color: "rgba(30, 105, 255, 0.38)"
      }
    }
  },
  checked: {},
  disabled: {}
})((props: any) => <Radio color="default" {...props} />);

/**
 * Props of StyledSelect
 */
interface StyledSelectProps {
  name: string;
  error?: boolean;
  maxWidth?: number;
  currentValue: string | undefined | null;
  selections: Array<string>;
  disabled?: boolean;
  maxItemShowed?: number;
  onChange: (event: React.ChangeEvent<{ value: unknown }>) => void;
  onBlur: (event: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
}

/**
 * A styled Select component with custom width
 * @param param0 StyledSelectProps
 */
const StyledSelect = ({
  name,
  error,
  currentValue,
  selections,
  disabled,
  maxItemShowed = 6,
  onChange,
  onBlur
}: StyledSelectProps) => {
  const itemHeight = 55;

  return (
    <Select
      fullWidth
      name={name}
      error={error}
      style={{ maxHeight: 56, padding: 0 }}
      disabled={disabled}
      value={currentValue}
      variant="outlined"
      onChange={onChange}
      onBlur={onBlur}
      MenuProps={{
        getContentAnchorEl: null,
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "left"
        },
        PaperProps: {
          style: {
            maxHeight: maxItemShowed * itemHeight,
            padding: 0
          }
        },
        MenuListProps: { disablePadding: true }
      }}
    >
      {selections.map((selection, index) => (
        <MenuItem
          style={{ height: itemHeight, borderBottom: "1px solid rgba(33, 33, 33, 0.08)" }}
          key={index}
          value={selection}
        >
          {selection}
        </MenuItem>
      ))}
    </Select>
  );
};

interface CheckFieldsLabelProps {
  label: string;
  error: boolean;
}

const CheckFieldsLabel = ({ label, error }: CheckFieldsLabelProps) => (
  <Typography color={error ? "error" : "textPrimary"} variant="body2">
    {label}
  </Typography>
);

interface NumberFormatProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

function PhoneFormat(props: NumberFormatProps) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      format="(###)-###-####"
      placeholder="(___)-___-____"
      mask={["_", "_", "_", "_", "_", "_", "_", "_", "_", "_"]}
      isNumericString
    />
  );
}

interface ErrorTextProps {
  error: string | undefined;
}

const ErrorText = ({ error }: ErrorTextProps) => (
  <FormHelperText error={Boolean(error)} className={error ? "error-text" : "hidden"}>
    {error}
  </FormHelperText>
);

/**
 * Mockup type of full info
 */
type DetailsInfoType = {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  role: string;
  isLoanOfficer: boolean | null;
};

interface EditAccountProps {
  /**
   * An optional prop that specifies if the page is for view only or for applying
   */
  viewOnly?: boolean;
  handleSubmitAccount?: Function;
}

// TODO This may change based on the design
const roleSelections = ["Admin", "User", "Pending User", "Pending Admin"];

/**
 * **Full** detail page of an application
 */
const EditAccount: React.FC<EditAccountProps> = (props) => {
  const accessToken = localStorage.getItem("accessToken");
  const { id } = useParams<{ id: string }>();
  const [viewOnly, setViewOnly] = useState(true);
  const [successfulDeleteModal, setSuccessfulDeleteModal] = useState(false);
  const [confirmDeleteModal, setConfirmDeleteModal] = useState(false);
  const [sentSuccessfully, setSentSuccessfully] = useState(false);
  const [sendEmail, setSendEmail] = useState(false);
  const [userFound, setUserFound] = useState(true);
  const [duplicateEmail, setDuplicateEmail] = useState(false);
  const [permission, setPermission] = useState(false);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const currentUser = accessToken && jwtDecode<any>(accessToken).sub;
  let history = useHistory();

  const emptyInfo: DetailsInfoType = {
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    role: "",
    isLoanOfficer: null
  };

  const [userInformation, setuserInformation] = useState(emptyInfo);
  const [defaultuserInformation, setDefaultuserInformation] = useState(emptyInfo);

  useEffect(() => {
    apiUsers
      .getSingle(currentUser)
      .then((res) => {
        if (res.code === undefined) {
          setPermission(res.role === "admin");
        } else if (res.code === 404) {
          console.log("not found");
        }
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, [currentUser, accessToken]);

  useEffect(() => {
    apiUsers
      .getSingle(id)
      .then((res) => {
        if (res.code === undefined) {
          setuserInformation(res);
          setDefaultuserInformation(res);
        } else if (res.code === 404) {
          console.log("not found");
          setUserFound(false);
        }
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, [id, accessToken]);

  const changeViewOnly = () => {
    setViewOnly(!viewOnly);
  };

  const cancelUpdate = () => {
    setuserInformation(defaultuserInformation);
    setViewOnly(!viewOnly);
  };

  const handleValueChange = (e) => {
    setDuplicateEmail(false);
    setuserInformation({ ...userInformation, [e.target.name]: e.target.value });
  };

  const handleDeleteButtonPress = (e) => {
    setConfirmDeleteModal(true);
  };

  const deleteUser = () => {
    apiUsers.remove(id).then((res) => {
      if (res.code === undefined) {
        setSuccessfulDeleteModal(true);
      } else {
        setError(true);
        setErrorMessage("An error occured while deleting this user. Please try again.");
      }
    });
  };

  const handleRedirect = () => {
    history.push("/manage-accounts");
    history.go(0);
  };

  const handleSendEmail = () => {
    setSendEmail(true);
  };

  const sendVerificationEmail = () => {
    let payload = {
      email: userInformation.email
    };

    const options: AxiosRequestConfig = {
      url: `${process.env.REACT_APP_API_ENDPOINT || ""}/api/v1/auth/send-verification-email`,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`
      },
      data: payload
    };

    axios(options)
      .then((res) => {
        console.log(res);
        setSentSuccessfully(true);
        setSendEmail(false);
      })
      .catch((err) => {
        console.log(err.message);
        setError(true);
        setSendEmail(false);
        setErrorMessage(
          "An error occured while sending the verification email.\nPlease make sure the email address is valid and try again."
        );
      });
  };

  const updateUser = () => {
    let roleString = "";
    if (userInformation.role === "Admin") {
      roleString = "admin";
    } else if (userInformation.role === "User") {
      roleString = "user";
    } else if (userInformation.role === "Pending Admin") {
      roleString = "pendingAdmin";
    } else if (userInformation.role === "Pending User") {
      roleString = "pendingUser";
    } else {
      roleString = userInformation.role;
    }

    let payload = {
      firstName: userInformation.firstName,
      lastName: userInformation.lastName,
      email: userInformation.email,
      phoneNumber: userInformation.phoneNumber,
      isLoanOfficer: userInformation.isLoanOfficer,
      role: roleString
    };

    apiUsers
      .patch(id, {
        users: payload
      })
      .then((res) => {
        if (res.code == undefined) {
          setDefaultuserInformation(userInformation);
          setViewOnly(!viewOnly);
        } else if (res.message) {
          setError(true);
          setErrorMessage(
            "An error occured while updating this user.\nPlease make sure the user information is valid and try again."
          );
          if (res.message === "Email already taken") {
            setErrorMessage("This email address is already taken.\nPlease use a different email address.");
            setDuplicateEmail(true);
          }
        }
      });
  };

  if (userFound) {
    return (
      <Formik
        onSubmit={(values, submitProps) => {}}
        enableReinitialize
        validateOnBlur={false}
        validateOnChange={false}
        initialValues={userInformation}
        validationSchema={Yup.object().shape({
          firstName: Yup.string().required("First name is required"),
          lastName: Yup.string().required("Last name is required"),
          phoneNumber: Yup.string()
            .matches(
              /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
              "Phone number must be a valid phone number"
            )
            .required("Phone number is required"),
          email: Yup.string().email("Email must be valid email").required("Email is required"),
          role: Yup.string().required("Permission is required"),
          isLoanOfficer: Yup.boolean().nullable().required("Is loan officer is required")
        })}
      >
        {(props) => {
          const { values, touched, errors, resetForm, setFieldValue, handleChange, handleBlur, handleSubmit } = props;

          return (
            <MuiPickersUtilsProvider utils={MomentUtils}>
              <div id="edit-account-header">
                <TitleTypography variant="h4" color="textPrimary" className="header">
                  Account Details
                </TitleTypography>

                {permission && (
                  <Button
                    color="secondary"
                    style={{ color: "#da6e5d", textTransform: "none", marginLeft: "25px" }}
                    variant="outlined"
                    onClick={handleSendEmail}
                  >
                    Resend Verification Email
                  </Button>
                )}
              </div>
              <form data-testid="form" className="create-application-form" onSubmit={handleSubmit}>
                <Card className="form-card">
                  <div id="application-overview-header">
                    <CardHeader title={"User details"} />
                    <div>
                      {viewOnly ? (
                        <>
                          {permission && (
                            <Button
                              id="app-ovr-view"
                              color="primary"
                              style={{ backgroundColor: "white", boxShadow: "none", color: "#da6e5d", marginRight: "30px" }}
                              variant="contained"
                              onClick={handleDeleteButtonPress}
                            >
                              Delete
                            </Button>
                          )}

                          {permission && (
                            <Button
                              id="app-ovr-view"
                              color="primary"
                              style={{ color: "white" }}
                              variant="contained"
                              onClick={changeViewOnly}
                            >
                              Edit
                            </Button>
                          )}
                        </>
                      ) : (
                        <>
                          <Button
                            id="app-ovr-view"
                            color="primary"
                            style={{ color: "white", marginRight: "30px" }}
                            variant="contained"
                            onClick={cancelUpdate}
                          >
                            Cancel
                          </Button>
                          <Button
                            id="app-ovr-view"
                            color="primary"
                            style={{ color: "white" }}
                            variant="contained"
                            type="submit"
                            onClick={updateUser}
                          >
                            Save
                          </Button>
                        </>
                      )}

                      {confirmDeleteModal && permission ? (
                        <Dialogue
                          open={confirmDeleteModal}
                          title="Confirm Delete"
                          text={`Are you sure you want to delete this user?`}
                          hasActions
                          onClose={() => {
                            setConfirmDeleteModal(false);
                          }}
                          onConfirm={() => {
                            setConfirmDeleteModal(false);
                            deleteUser();
                          }}
                        />
                      ) : null}

                      {sendEmail && permission ? (
                        <Dialogue
                          title={"Confirm Resend Verification Email"}
                          text={"Are you sure you want to resend a verification email to this user?"}
                          open={sendEmail && permission}
                          hasActions
                          onClose={() => {
                            setSendEmail(false);
                          }}
                          onConfirm={() => {
                            sendVerificationEmail();
                          }}
                        />
                      ) : null}

                      {successfulDeleteModal ? (
                        <Dialogue
                          title={"Successful!"}
                          text={"The user has been deleted."}
                          open={successfulDeleteModal}
                          hasClose
                          onClose={() => {
                            setSuccessfulDeleteModal(false);
                            handleRedirect();
                          }}
                        />
                      ) : null}

                      {sentSuccessfully ? (
                        <Dialogue
                          title={"Successful!"}
                          text={"Verification email sent successfully."}
                          open={sentSuccessfully}
                          hasClose
                          onClose={() => {
                            setSentSuccessfully(false);
                          }}
                        />
                      ) : null}

                      {error ? (
                        <Dialogue
                          title={"Error"}
                          text={errorMessage}
                          open={error}
                          hasClose
                          onClose={() => {
                            setError(false);
                            setErrorMessage("");
                          }}
                        />
                      ) : null}
                    </div>
                  </div>

                  <CardContent>
                    <Grid container spacing={4}>
                      <Grid item container xs={12} justify="space-between">
                        <Grid item xs={4}>
                          <FieldTitle text="First Name" required />
                          <TextField
                            data-testid="firstName"
                            name="firstName"
                            variant="outlined"
                            fullWidth
                            error={Boolean(errors.firstName)}
                            onChange={handleValueChange}
                            onBlur={handleBlur}
                            value={userInformation.firstName}
                            disabled={viewOnly}
                          />
                          <ErrorText error={errors.firstName} />
                        </Grid>

                        <Grid item xs={4}>
                          <FieldTitle text="Last Name" required />
                          <TextField
                            data-testid="lastName"
                            name="lastName"
                            variant="outlined"
                            fullWidth
                            error={Boolean(errors.lastName)}
                            onChange={handleValueChange}
                            onBlur={handleBlur}
                            value={userInformation.lastName}
                            disabled={viewOnly}
                          />
                          <ErrorText error={errors.lastName} />
                        </Grid>

                        <Grid item xs={2}></Grid>
                      </Grid>

                      <Grid item container xs={12} justify="space-between">
                        <Grid item xs={4}>
                          <FieldTitle text="Phone Number" required />
                          <TextField
                            data-testid="phoneNumber"
                            type="tel"
                            name="phoneNumber"
                            variant="outlined"
                            fullWidth
                            error={Boolean(errors.phoneNumber)}
                            onChange={handleValueChange}
                            onBlur={handleBlur}
                            InputProps={{
                              inputComponent: PhoneFormat as any
                            }}
                            value={userInformation.phoneNumber.match(/\d/g)}
                            disabled={viewOnly}
                          />
                          <ErrorText error={errors.phoneNumber} />
                        </Grid>

                        <Grid item xs={4}>
                          <FieldTitle text="Email" required />
                          <TextField
                            data-testid="email"
                            name="email"
                            variant="outlined"
                            fullWidth
                            error={Boolean(errors.email)}
                            onChange={handleValueChange}
                            onBlur={handleBlur}
                            value={userInformation.email}
                            disabled={viewOnly}
                          />
                          <ErrorText error={errors.email} />
                          {duplicateEmail ? <ErrorText error={"The Email Address is already taken"} /> : null}
                        </Grid>

                        <Grid item xs={2}></Grid>
                      </Grid>

                      <Grid item container xs={12} justify="space-between">
                        <Grid item xs={4}>
                          <FieldTitle text="Permission" required />
                          <StyledSelect
                            name="role"
                            error={Boolean(errors.role)}
                            onChange={handleValueChange}
                            onBlur={handleBlur}
                            currentValue={
                              userInformation.role.toLowerCase().includes("pending")
                                ? userInformation.role.toLowerCase().includes("admin")
                                  ? "Pending Admin"
                                  : "Pending User"
                                : userInformation.role.toLowerCase().includes("admin")
                                ? "Admin"
                                : "User"
                            }
                            selections={
                              userInformation.role.toLowerCase().includes("pending")
                                ? roleSelections.slice(2)
                                : roleSelections.slice(0, 2)
                            }
                            disabled={viewOnly}
                          />
                          <ErrorText error={errors.role} />
                        </Grid>

                        <Grid item xs={7} style={{ marginTop: "28px" }}>
                          <FieldTitle text="Add as loan officer?" required bold />
                          <RadioGroup row value={userInformation.isLoanOfficer}>
                            <StyledFormControlLabel
                              name="isLoanOfficer"
                              value={true}
                              disabled={viewOnly}
                              control={
                                <ColoredRadio
                                  onChange={() => {
                                    setFieldValue("isLoanOfficer", true);
                                    userInformation.isLoanOfficer = true;
                                  }}
                                />
                              }
                              label={<CheckFieldsLabel label="Yes" error={Boolean(errors.isLoanOfficer)} />}
                            />
                            <StyledFormControlLabel
                              name="isLoanOfficer"
                              value={false}
                              disabled={viewOnly}
                              control={
                                <ColoredRadio
                                  onChange={() => {
                                    setFieldValue("isLoanOfficer", false);
                                    userInformation.isLoanOfficer = false;
                                  }}
                                />
                              }
                              label={
                                <CheckFieldsLabel label="No" error={!!touched.isLoanOfficer && !!errors.isLoanOfficer} />
                              }
                            />
                          </RadioGroup>
                          <ErrorText error={errors.isLoanOfficer} />
                        </Grid>
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </form>
            </MuiPickersUtilsProvider>
          );
        }}
      </Formik>
    );
  } else {
    return <h1>User not found</h1>;
  }
};

export default EditAccount;
