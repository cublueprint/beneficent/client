import React, { useState, useEffect } from "react";
import "./AdminHomePage.scss";
import { withStyles, useTheme } from "@material-ui/core/styles";

import {
  Grid,
  AppBar,
  Toolbar,
  Typography,
  Drawer,
  Divider,
  List,
  ListItem,
  IconButton,
  Hidden,
  Avatar,
  Button,
  Menu
} from "@material-ui/core";

import {
  Menu as MenuIcon,
  Search as SearchIcon,
  Notifications as NotificationsIcon,
  ExpandMore as ExpandMoreIcon
} from "@material-ui/icons/";
import { Link, Route, Switch, useLocation, useRouteMatch, useHistory } from "react-router-dom";

// import ROUTES from "../../routes";
import logo from "../../../assets/beneficient-logo.png";
import useWindowSize from "../../../hooks/useWindowSize";
import AccountSettings from "../../AccountSettings/AccountSettings";
import routes from "../../../routes";
import ApplicationsLayout from "../../ApplicationsLayout/ApplicationsLayout";
import MyClients from "../../Clients/Clients";
import AccountManager from "../../AccountManager/AccountManager";

const textColor = "#192A3E";
const sideNavItemSelectedColor = "rgba(30, 105, 255, 0.08)";

/**
 * A styled typography component for the side nav's menu items
 */
const SideNavSelectionTypography = withStyles({
  root: {
    color: textColor,
    fontSize: 20
  }
})(Typography);

const BlackIconButton = withStyles({
  root: {
    color: "black"
  }
})(IconButton);

/**
 * A styled ListItem component for the side nav's menu items
 */
const SideNavItem = withStyles({
  root: {
    "&$selected, &$selected:hover": {
      backgroundColor: sideNavItemSelectedColor
    }
  },
  selected: {},
  button: {}
})(ListItem);

/**
 * A styled typography component for the titles
 */
export const TitleTypography = withStyles({
  h4: {
    paddingLeft: 15,
    fontWeight: 400,
    marginBottom: 32,
    fontSize: 32
  },
  h6: {
    paddingLeft: 15,
    fontWeight: 700
  }
})(Typography);

const sideNavItems = [
  {
    test_id: "dashboard-selection",
    path: "",
    text: "Dashboard"
  },
  {
    test_id: "cases-and-applications-selection",
    path: routes.casesApplications.path,
    text: "Applications"
  },
  {
    test_id: "my-clients-selection",
    path: routes.myClients.path,
    text: "Clients"
  },
  {
    test_id: "account-manager-selection",
    path: routes.accountManager.path,
    text: "Account Manager"
  }
];

interface AdminHomePageProps {
  /**
   * Function handling logout the current user
   */
  handleLogout(): void;
  validRefresh(): Promise<boolean>;
}

/**
 * The home page skeleton of admin accounts
 * @param { handleLogout, accessToken }: AdminHomePageProps
 */
const HomePage: React.FC<AdminHomePageProps> = ({ handleLogout, validRefresh }: AdminHomePageProps) => {
  const [width] = useWindowSize();
  const [mobile, setMobile] = useState(false);
  const [sideNavOpen, setSideNavOpen] = useState(false);
  const [authenticating, setAuthenticating] = useState(false);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const theme = useTheme();
  let location = useLocation();
  const history = useHistory();
  let { path } = useRouteMatch();

  useEffect(() => {
    setAuthenticating(true);

    const isAuthenticated = async (): Promise<void> => {
      if (!(await validRefresh())) {
        history.push(routes.homepage.path);
      }
    };

    isAuthenticated();
    setAuthenticating(false);
  }, [history, validRefresh]);

  const removeTrailingSlash = (path) => {
    return path.endsWith("/") ? path.slice(0, path.length - 1) : path;
  };
  path = removeTrailingSlash(path);
  location.pathname = removeTrailingSlash(location.pathname);
  /**
   * Handle changes of the window's width
   */
  useEffect(() => {
    if (width < theme.breakpoints.width("md")) {
      setMobile(true);
    } else {
      setMobile(false);
    }
  }, [width, theme.breakpoints]);

  const handleCloseSideNav = () => {
    setSideNavOpen(false);
  };

  const handleClickUser = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorEl(null);
  };

  return !authenticating ? (
    <Grid className="homepage-container" container direction="row">
      <Grid item>
        <Drawer
          className="drawer"
          variant={!mobile ? "permanent" : "temporary"}
          anchor="left"
          open={!mobile ? true : sideNavOpen}
          onClose={handleCloseSideNav}
          PaperProps={{
            elevation: 3
          }}
          classes={{
            paper: "sidenav-drawer"
          }}
        >
          <Grid container direction="column">
            <Grid item className="home-logo-box">
              <img className="home-beneficent-logo" src={logo} alt="Beneficent logo" />
            </Grid>
            <Divider variant="fullWidth" />
            <Grid item className="drawer-menu">
              <List component="nav">
                {sideNavItems.map((item) => {
                  return (
                    <SideNavItem
                      key={item.test_id}
                      data-testid={item.test_id}
                      selected={location.pathname === path + item.path}
                      className="side-nav-item"
                      onClick={() => {
                        history.push(path + item.path);
                        history.go(0);
                      }}
                      button
                    >
                      <SideNavSelectionTypography variant="h6">{item.text}</SideNavSelectionTypography>
                    </SideNavItem>
                  );
                })}
              </List>
            </Grid>
          </Grid>
        </Drawer>
      </Grid>
      <Grid direction="column" item container className="app-bar-and-content">
        <Grid item>
          <AppBar position="fixed" className="app-bar">
            <Toolbar className="app-bar">
              <Hidden mdUp>
                <BlackIconButton onClick={() => setSideNavOpen(!sideNavOpen)}>
                  <MenuIcon />
                </BlackIconButton>
              </Hidden>
              <div style={{ flexGrow: 1 }} />

              {/* TODO Add function to this search button */}
              <BlackIconButton>
                <SearchIcon />
              </BlackIconButton>

              {/* TODO Add function to this notification button */}
              <BlackIconButton>
                <NotificationsIcon />
              </BlackIconButton>

              {/* TODO Change this to custom avatar and name based on each user */}
              <Button onClick={handleClickUser} className="user-button">
                <Avatar className="user-avatar" alt="Avatar" style={{ backgroundColor: "#DA6E5D" }}>
                  A
                </Avatar>
                <Typography>Loan Officer</Typography>
                <ExpandMoreIcon />
              </Button>
              <Menu
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={handleCloseUserMenu}
                keepMounted
                getContentAnchorEl={null}
                classes={{
                  paper: "user-menu"
                }}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "center"
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "center"
                }}
              >
                {/* TODO Change this when the design team has the finalized version */}
                <div className="user-menu-content">
                  <Link to={path + routes.accountSettings.path}>
                    <Button variant="outlined">Account Settings</Button>
                  </Link>
                  <Button onClick={handleLogout} variant="outlined">
                    Logout
                  </Button>
                </div>
              </Menu>
            </Toolbar>
          </AppBar>
        </Grid>

        <Grid item className="main-content">
          <Switch>
            <Route path={path + routes.casesApplications.path} render={() => <ApplicationsLayout />} />
            <Route path={path + routes.myClients.path} render={() => <MyClients />} />
            <Route path={path + routes.accountManager.path} render={() => <AccountManager />} />
            <Route path={path + routes.accountSettings.path} render={() => <AccountSettings />} />
            <Route
              render={() => (
                <TitleTypography variant="h4" color="textPrimary">
                  Dashboard
                </TitleTypography>
              )}
            />
          </Switch>
        </Grid>
      </Grid>
    </Grid>
  ) : (
    <Typography>Authenticating...</Typography>
  );
};

export default HomePage;
