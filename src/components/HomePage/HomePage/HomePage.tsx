import * as React from "react";
import "./HomePage.scss";
import FormCardLayout from "../../common/FormCardLayout/FormCardLayout";
import ROUTES from "../../../routes";

import { makeStyles, createStyles, MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { Typography, TextField, InputAdornment, Button, Grid } from "@material-ui/core/";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import Link from "@material-ui/core/Link";
import { useHistory } from "react-router-dom";
import { getParsedCommandLineOfConfigFile } from "typescript";

/**
 * Custom theme for the buttons
 */
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "rgba(0, 0, 0, 0.87);"
    },
    secondary: {
      main: "#DA6E5D"
    }
  },
  typography: {
    fontFamily: "Lato"
  }
});

/**
 * Custom theme for the buttons
 */
const useStyles = makeStyles({
  submit: {
    marginTop: 15,

    primary: {
      main: "#FFFFFF"
    },
    "&.MuiButton-outlinedSecondary": {
      border: "1px #DA6E5D"
    }
  },
  typography: {
    fontFamily: "Lato"
  }
});

/**
 * Home page component for the application.
 */
const HomePage = () => {
  const classes = useStyles();

  return (
    <MuiThemeProvider theme={theme}>
      <FormCardLayout>
        <div className="homepage">
          <Grid direction="column" spacing={4}>
            <Grid item>
              <Link href={ROUTES.apply.path}>
                <Button style={{ color: "#FFFFFF", backgroundColor: "#DA6E5D", width: 202, height: 84 }}>
                  Apply for a loan
                </Button>
              </Link>
            </Grid>
            <Grid item>
              <Link href={ROUTES.login.path}>
                <Button
                  type="submit"
                  variant="outlined"
                  color="secondary"
                  className={classes.submit}
                  style={{ width: 202, height: 84 }}
                >
                  Log in
                </Button>
              </Link>
            </Grid>
          </Grid>
        </div>
      </FormCardLayout>
    </MuiThemeProvider>
  );
};

export default HomePage;
