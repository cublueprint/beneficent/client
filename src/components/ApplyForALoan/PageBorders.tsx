import React, { useState, useEffect } from "react";
import "./PageBorders.scss";
import { withStyles, useTheme } from "@material-ui/core/styles";

import { Grid, AppBar, Typography, Drawer, Divider } from "@material-ui/core";

// import ROUTES from "../../routes";
import logo from "../../assets/beneficient-logo.png";
import useWindowSize from "../../hooks/useWindowSize";

/**
 * A styled typography component for the titles
 */
export const TitleTypography = withStyles({
  h4: {
    paddingLeft: 15,
    fontWeight: 400,
    marginBottom: 32,
    fontSize: 32
  },
  h6: {
    paddingLeft: 15,
    fontWeight: 700
  }
})(Typography);

/**
 * The home page skeleton
 */
const PageBorders = (props: any) => {
  const [width] = useWindowSize();
  const [mobile, setMobile] = useState(false);
  const [sideNavOpen, setSideNavOpen] = useState(false);
  const theme = useTheme();

  /**
   * Handle changes of the window's width
   */
  useEffect(() => {
    if (width < theme.breakpoints.width("md")) {
      setMobile(true);
    } else {
      setMobile(false);
    }
  }, [width, theme.breakpoints]);

  const handleCloseSideNav = () => {
    setSideNavOpen(false);
  };

  return (
    <Grid className="homepage-container" container direction="row">
      <Grid item>
        <Drawer
          className="drawer"
          variant={!mobile ? "permanent" : "temporary"}
          anchor="left"
          open={!mobile ? true : sideNavOpen}
          onClose={handleCloseSideNav}
          PaperProps={{
            elevation: 3
          }}
          classes={{
            paper: "sidenav-drawer"
          }}
        >
          <Grid container direction="column">
            <Grid item className="home-logo-box">
              <img className="home-beneficent-logo" src={logo} alt="Beneficent logo" />
            </Grid>
            <Divider variant="fullWidth" />
          </Grid>
        </Drawer>
      </Grid>
      <Grid direction="column" item container className="app-bar-and-content">
        <Grid item>
          <AppBar position="fixed" className="app-bar" />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default PageBorders;
