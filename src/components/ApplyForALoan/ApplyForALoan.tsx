import React from "react";
import CreateApplication from "../ApplicationsLayout/CreateApplication/CreateApplication";
import PageBorders, { TitleTypography } from "./PageBorders";

const ApplyForALoan = () => {
  return (
    <div>
      <PageBorders />
      <div className="application-form">
        <TitleTypography variant="h4" color="textPrimary">
          Apply for a Loan
        </TitleTypography>
        <CreateApplication />
      </div>
    </div>
  );
};

export default ApplyForALoan;
