import { cleanup, render, act, fireEvent } from "@testing-library/react";
import * as React from "react";
import { MemoryRouter } from "react-router-dom";
import routes from "../routes";
import CreateEditContract from "../components/ApplicationsLayout/ApplicationDetails/ApplicationOverview/ContractDetails/Contracts/CreateEditContract/CreateEditContract.tsx";

afterEach(cleanup);

const mockProps = {
  guarantor: "guarantor",
  officerId: "officerId",
  userId: "userId"
};

describe("Create Edit Contract Form", () => {
  describe("Changing text fields", () => {
    it("should correctly change amount fields", () => {
      const { getByTestId } = render(
        <MemoryRouter initialEntries={[routes.casesApplications.path + routes.createContract.path]}>
          <CreateEditContract {...mockProps} />
        </MemoryRouter>
      );
      const loanAmountInput: any = getByTestId("loan-amount");
      const monthlyInstallmentInput: any = getByTestId("monthly-installment");
      const finalInstallmentInput: any = getByTestId("final-installment");

      act(() => {
        fireEvent.change(loanAmountInput, {
          target: { value: 10 }
        });
        fireEvent.change(monthlyInstallmentInput, {
          target: { value: 20 }
        });
        fireEvent.change(finalInstallmentInput, {
          target: { value: 30 }
        });
      });

      expect(loanAmountInput.value).toBe("10");
      expect(monthlyInstallmentInput.value).toBe("20");
      expect(finalInstallmentInput.value).toBe("30");
    });

    it("should correctly change contract date field", () => {
      const { getByTestId } = render(
        <MemoryRouter initialEntries={[routes.casesApplications.path + routes.createContract.path]}>
          <CreateEditContract {...mockProps} />
        </MemoryRouter>
      );
      const contractDateInput: any = getByTestId("contract-date");
      const firstInstallmentDueInput: any = getByTestId("first-installment-due");
      const lastInstallmentDueInput: any = getByTestId("last-installment-due");

      act(() => {
        fireEvent.change(contractDateInput, {
          target: { value: "01012000" } // Input in number for date
        });
        fireEvent.change(firstInstallmentDueInput, {
          target: { value: "0e2f0g1h2a0b0c0d" } // Input has letters
        });
        fireEvent.change(lastInstallmentDueInput, {
          target: { value: "0%a3/0^12!0@0#0/" } // Input has special numbers
        });
      });

      expect(contractDateInput.value).toBe("01/01/2000");
      expect(firstInstallmentDueInput.value).toBe("02/01/2000");
      expect(lastInstallmentDueInput.value).toBe("03/01/2000");
    });
  });

  describe("form validation", () => {
    it("should show error for submitting improper dates", async () => {
      const { getByTestId } = render(
        <MemoryRouter initialEntries={[routes.casesApplications.path + routes.createContract.path]}>
          <CreateEditContract {...mockProps} />
        </MemoryRouter>
      );
      const contractDateInput: any = getByTestId("contract-date");
      const firstInstallmentDueInput: any = getByTestId("first-installment-due");
      const lastInstallmentDueInput: any = getByTestId("last-installment-due");
      const contractForm = getByTestId("contract-form");

      await act(async () => {
        await fireEvent.change(contractDateInput, {
          target: { value: "" }
        });

        await fireEvent.change(firstInstallmentDueInput, {
          target: { value: "2000/12/" } // Input has special numbers
        });

        await fireEvent.change(lastInstallmentDueInput, {
          target: { value: "notadate" } // Input has special numbers
        });

        await fireEvent.submit(contractForm);
      });

      expect(contractDateInput).toHaveAttribute("aria-invalid", "true");
      expect(firstInstallmentDueInput).toHaveAttribute("aria-invalid", "true");
      expect(lastInstallmentDueInput).toHaveAttribute("aria-invalid", "true");
    });

    it("should show error for submitting empty loan fields", async () => {
      const { getByTestId } = render(
        <MemoryRouter initialEntries={[routes.casesApplications.path + routes.createContract.path]}>
          <CreateEditContract {...mockProps} />
        </MemoryRouter>
      );
      const loanAmountInput: any = getByTestId("loan-amount");
      const monthlyInstallmentInput: any = getByTestId("monthly-installment");
      const finalInstallmentInput: any = getByTestId("final-installment");
      const contractForm = getByTestId("contract-form");

      await act(async () => {
        await fireEvent.submit(contractForm);
      });

      expect(loanAmountInput).toHaveAttribute("aria-invalid", "true");
      expect(monthlyInstallmentInput).toHaveAttribute("aria-invalid", "true");
      expect(finalInstallmentInput).toHaveAttribute("aria-invalid", "false");
    });
  });
});
