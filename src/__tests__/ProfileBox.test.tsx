import { cleanup, fireEvent, render, wait } from "@testing-library/react";
import { AxiosResponse } from "axios";
import * as React from "react";
import ProfileBox from "../components/AccountSettings/ProfileBox/ProfileBox";

afterEach(cleanup);

const emptyInit = {
  firstName: "",
  lastName: "",
  address: "",
  phoneNumber: "",
  
};

const filledInit = {
  firstName: "Tom",
  lastName: "Zhu",
  address: "123 Sesame Street",
  phoneNumber: "1-800-267-2001",
  
};

const otherProps = {
  handleUpdateProfile: () => new Promise<AxiosResponse>((resolve) => resolve())
};

describe("Profile Box", () => {
  it("renders properly", () => {
    const { getByPlaceholderText } = render(<ProfileBox initialValues={filledInit} {...otherProps} />);

    const firstNameInput: any = getByPlaceholderText("First Name");
    const lastNameInput: any = getByPlaceholderText("Last Name");
    const phoneNumInput: any = getByPlaceholderText("Phone Number");
    const addressInput: any = getByPlaceholderText("Address");

    expect(firstNameInput.value).toBe(filledInit.firstName);

    expect(lastNameInput.value).toBe(filledInit.lastName);

    expect(addressInput.value).toBe(filledInit.address);

    expect(phoneNumInput.value).toBe(filledInit.phoneNumber);
  });

  describe("handles validation correctly", () => {
    it("handles valid inputs", async () => {
      const { getByPlaceholderText, getByText } = render(<ProfileBox initialValues={emptyInit} {...otherProps} />);

      const firstNameInput = getByPlaceholderText("First Name");
      const phoneNumInput = getByPlaceholderText("Phone Number");
      const lastNameInput = getByPlaceholderText("Last Name");
      const addressInput = getByPlaceholderText("Address");

      getByText("Edit").click();

      await wait(() => {
        fireEvent.change(firstNameInput, { target: { value: "John" } });
        firstNameInput.blur();

        fireEvent.change(phoneNumInput, { target: { value: "+1 (613) 762-3313" } });
        phoneNumInput.blur();

        fireEvent.change(lastNameInput, { target: { value: "Doe" } });
        lastNameInput.blur();

        fireEvent.change(addressInput, { target: { value: "Street" } });
        addressInput.blur();
      });

      expect(firstNameInput).toHaveAttribute("aria-invalid", "false");
      expect(lastNameInput).toHaveAttribute("aria-invalid", "false");
      expect(addressInput).toHaveAttribute("aria-invalid", "false");
      expect(phoneNumInput).toHaveAttribute("aria-invalid", "false");
    });

    it("handles invalid inputs", async () => {
      const { getByPlaceholderText, getByText } = render(<ProfileBox initialValues={emptyInit} {...otherProps} />);

      const firstNameInput = getByPlaceholderText("First Name");
      const phoneNumInput = getByPlaceholderText("Phone Number");
      const lastNameInput = getByPlaceholderText("Last Name");
      const addressInput = getByPlaceholderText("Address");

      getByText("Edit").click();

      await wait(() => {
        firstNameInput.focus();
        fireEvent.change(firstNameInput, { target: { value: "" } });
        firstNameInput.blur();

        phoneNumInput.focus();
        fireEvent.change(phoneNumInput, { target: { value: "abcdefg" } });
        phoneNumInput.blur();

        lastNameInput.focus();
        fireEvent.change(lastNameInput, { target: { value: "" } });
        lastNameInput.blur();

        addressInput.focus();
        fireEvent.change(addressInput, { target: { value: "" } });
        addressInput.blur();
      });

      expect(firstNameInput).toHaveAttribute("aria-invalid", "true");
      expect(lastNameInput).toHaveAttribute("aria-invalid", "true");
      expect(addressInput).toHaveAttribute("aria-invalid", "true");
      expect(phoneNumInput).toHaveAttribute("aria-invalid", "true");
    });
  });

  it("removes disabled on click edit button", async () => {
    const { getByText } = render(<ProfileBox {...otherProps} initialValues={filledInit} />);

    const editBtn = getByText("Edit");

    await wait(() => {
      fireEvent.click(editBtn);
    });

    expect(editBtn.firstChild?.textContent).toBe("Save");
  });
});
