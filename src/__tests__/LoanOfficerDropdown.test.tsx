import { cleanup, fireEvent, render } from "@testing-library/react";
import * as React from "react";
import LoanOfficerDropdown from "../components/common/LoanOfficerDropdown/LoanOfficerDropdown";
import avatar from "../assets/avatar.png";

afterEach(cleanup);

const mockProps = {
  officers: [
    {
      workload: 36.666666666666664,
      user: {
        role: "officer",
        isEmailVerified: false,
        firstName: "Carley",
        lastName: "Wyman",
        phoneNumber: "988-787-8880",
        address: "9229 Ernser Corners",
        country: "Cayman Islands",
        email: "maggie.bergnaum@gmail.com",
        createdAt: "2021-09-26T00:57:53.693Z",
        updatedAt: "2021-09-26T00:57:53.945Z",
        id: "614fc591b172ae06d2ba1e33"
      },
      createdAt: "2021-09-26T00:57:53.948Z",
      updatedAt: "2021-09-30T04:15:34.278Z",
      id: "614fc591b172ae06d2ba1e3a"
    },
    {
      workload: 83.33333333333334,
      user: {
        role: "officer",
        isEmailVerified: false,
        firstName: "Kaylah",
        lastName: "Heathcote",
        phoneNumber: "254-760-2055",
        address: "49767 Keegan Terrace",
        country: "Republic of Korea",
        email: "sonny_raynor@hotmail.com",
        createdAt: "2021-09-26T00:57:53.693Z",
        updatedAt: "2021-09-26T00:57:53.954Z",
        id: "614fc591b172ae06d2ba1e32"
      },
      createdAt: "2021-09-26T00:57:53.958Z",
      updatedAt: "2021-09-30T04:15:34.288Z",
      id: "614fc591b172ae06d2ba1e3b"
    },
    {
      workload: 0,
      user: {
        role: "admin",
        isEmailVerified: false,
        firstName: "fake first name",
        lastName: "fake last name",
        phoneNumber: "+16132332892",
        address: "'123 Melon Street, Ottawa, K1L6L3'",
        country: "Canada",
        email: "fake@example.com",
        createdAt: "2021-09-26T00:57:53.628Z",
        updatedAt: "2021-09-26T00:57:53.935Z",
        id: "614fc591b172ae06d2ba1e31"
      },
      createdAt: "2021-09-30T03:03:54.892Z",
      updatedAt: "2021-09-30T04:15:34.293Z",
      id: "6155291ac70e381d667d17b2"
    }
  ],
  onChange: jest.fn(),
  value: "",
  currentUser: "nonexistent"
};

describe("renders properly", () => {
  it("shows unassigned when empty", () => {
    const { getByTestId } = render(<LoanOfficerDropdown {...mockProps} />);

    expect(getByTestId("Unassigned")).toHaveTextContent("Unassigned");
  });

  it("shows name of current officer", () => {
    const { getByTestId } = render(
      <LoanOfficerDropdown {...mockProps} value={"614fc591b172ae06d2ba1e3b"}></LoanOfficerDropdown>
    );

    expect(getByTestId("lo-select")).toHaveTextContent("Kaylah");
  });

  it("shows current user as me", () => {
    const { getByTestId } = render(
      <LoanOfficerDropdown
        {...mockProps}
        value={mockProps.officers[2].id}
        currentUser={mockProps.officers[2].user.id}
      ></LoanOfficerDropdown>
    );

    expect(getByTestId("lo-select")).toHaveTextContent("Me");
  });

  it("renders officer options", () => {
    const { getByText, getByRole } = render(
      <LoanOfficerDropdown {...mockProps} currentUser={mockProps.officers[2].user.id} />
    );

    fireEvent.mouseDown(getByRole("button"));

    expect(getByText("Me")).toBeInTheDocument();
    expect(getByText("Carley")).toBeInTheDocument();
  });
});

describe("handles interaction", () => {
  it("triggers onChange event with value", () => {
    const { getByText, getByRole } = render(<LoanOfficerDropdown {...mockProps} />);

    fireEvent.mouseDown(getByRole("button"));

    fireEvent.click(getByText("Carley").parentElement?.parentElement!);
    expect(mockProps.onChange).toHaveBeenCalledWith("614fc591b172ae06d2ba1e3a");
  });

  it("triggers onChange if assigned to self", () => {
    const { getByTestId, getByRole } = render(<LoanOfficerDropdown {...mockProps} currentUser="614fc591b172ae06d2ba1e33" />);

    fireEvent.mouseDown(getByRole("button"));

    fireEvent.click(getByTestId("614fc591b172ae06d2ba1e3a"));
    expect(mockProps.onChange).toHaveBeenCalledWith("614fc591b172ae06d2ba1e3a");
  });
});
