import { cleanup, render } from "@testing-library/react";
import * as React from "react";
import ClientDetails from "../components/Clients/ClientDetails/ClientDetails";
import { MemoryRouter } from "react-router-dom";
import routes from "../routes";
import { FormikSubmitObject } from "../components/ApplicationsLayout/ApplicationsLayout";

afterEach(cleanup);

describe("Client Details", () => {
  it("renders correctly", () => {
    const { queryByTestId } = render(
      <MemoryRouter initialEntries={[routes.myClients.path + "/:id"]}>
        <ClientDetails />
      </MemoryRouter>
    );

    expect(queryByTestId("skeleton-container")).toHaveClass("MuiGrid-container MuiGrid-spacing-xs-3");
  });
});
