import { cleanup, render } from "@testing-library/react";
import * as React from "react";
import { MemoryRouter } from "react-router-dom";
import routes from "../routes";
import ApplicationsLayout, { breadcrumbMap } from "../components/ApplicationsLayout/ApplicationsLayout";

const accessToken = "token";

afterEach(cleanup);

describe("Applications Layout", () => {
  it("renders default", () => {
    const { getByText } = render(
      <MemoryRouter initialEntries={[routes.casesApplications.path]}>
        <ApplicationsLayout />
      </MemoryRouter>
    );

    expect(getByText(breadcrumbMap[routes.casesApplications.path]).textContent).toBe(
      breadcrumbMap[routes.casesApplications.path]
    );
  });

  describe("has breadcrumb change", () => {
    it("works for detail", () => {
      const { getByText } = render(
        <MemoryRouter initialEntries={[routes.casesApplications.path + routes.detail.path]}>
          <ApplicationsLayout />
        </MemoryRouter>
      );

      expect(getByText("Back").parentElement?.getAttribute("href")).toBe(routes.casesApplications.path);
      expect(getByText(breadcrumbMap[routes.detail.path]).textContent).toBe(breadcrumbMap[routes.detail.path]);
    });

    it("works for create", () => {
      const { getByText } = render(
        <MemoryRouter initialEntries={[routes.casesApplications.path + routes.create.path]}>
          <ApplicationsLayout />
        </MemoryRouter>
      );

      expect(getByText("Back").parentElement?.getAttribute("href")).toBe(routes.casesApplications.path);
      expect(getByText(breadcrumbMap[routes.create.path]).textContent).toBe(breadcrumbMap[routes.create.path]);
    });
  });
});
