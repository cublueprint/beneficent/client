import * as React from "react";
import { cleanup, render } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import AccountSettings from "../components/AccountSettings/AccountSettings";

afterEach(cleanup);

const props = {
  accessToken: "token"
};

describe("Account Settings", function () {
  it("render correctly", () => {
    const { getByText } = render(
      <BrowserRouter>
        <AccountSettings {...props} />
      </BrowserRouter>
    );
    expect(getByText("Account Settings")).toHaveClass("MuiTypography-root");
    expect(getByText("Account Settings")).toHaveClass("MuiTypography-h4");
  });
});
