import { cleanup, fireEvent, render, wait } from "@testing-library/react";
import { AxiosResponse } from "axios";
import * as React from "react";
import PreferencesBox from "../components/AccountSettings/PreferencesBox/PreferencesBox";

afterEach(cleanup);

const emptyInit = {
  email: "",
  password: "",
  newPassword: "",
  newPasswordConfirmation: ""
};

const validFilledInit = {
  email: "test@test.com",
  password: "OldPassword123!",
  newPassword: "NewPassword123!",
  newPasswordConfirmation: "NewPassword123!"
};

const inValidFilledInit = {
  email: "test@test.com",
  password: "",
  newPassword: "!!!",
  newPasswordConfirmation: "newpassword!"
};

const otherProps = {
  handleUpdatePreferences: () => new Promise<AxiosResponse>((resolve) => resolve())
};

describe("Preferences Box", () => {
  it("renders properly", () => {
    const { getByPlaceholderText } = render(<PreferencesBox initialValues={validFilledInit} {...otherProps} />);

    const emailInput: any = getByPlaceholderText("Email");
    const passwordInput: any = getByPlaceholderText("Password");

    expect(emailInput).toHaveAttribute("value", validFilledInit.email);
    expect(passwordInput).toHaveAttribute("value", validFilledInit.password);
  });

  describe("handles validation correctly", () => {
    it("handles valid inputs", async () => {
      const { getByPlaceholderText, getByText } = render(<PreferencesBox initialValues={emptyInit} {...otherProps} />);

      fireEvent.click(getByText("Edit"));

      const passwordInput: any = getByPlaceholderText("Password");
      const newPasswordInput: any = getByPlaceholderText("New Password");
      const newPasswordConfirmationInput: any = getByPlaceholderText("New Password Confirmation");

      await wait(() => {
        passwordInput.focus();
        fireEvent.change(passwordInput, { target: { value: validFilledInit.password } });
        passwordInput.blur();

        newPasswordInput.focus();
        fireEvent.change(newPasswordInput, { target: { value: validFilledInit.newPassword } });
        newPasswordInput.blur();

        newPasswordConfirmationInput.focus();
        fireEvent.change(newPasswordConfirmationInput, { target: { value: validFilledInit.newPassword } });
        newPasswordConfirmationInput.blur();
      });

      expect(passwordInput).toHaveAttribute("aria-invalid", "false");
      // expect(newPasswordInput).toBe("aria-invalid");
      expect(newPasswordInput).toHaveAttribute("aria-invalid", "false");
      // expect(newPasswordConfirmationInput).toBe("aria-invalid");
      expect(newPasswordConfirmationInput).toHaveAttribute("aria-invalid", "false");
    });

    it("handles invalid inputs", async () => {
      const { getByPlaceholderText, getByText } = render(<PreferencesBox initialValues={emptyInit} {...otherProps} />);

      fireEvent.click(getByText("Edit"));

      const passwordInput: any = getByPlaceholderText("Password");
      const newPasswordInput: any = getByPlaceholderText("New Password");
      const newPasswordConfirmationInput: any = getByPlaceholderText("New Password Confirmation");

      await wait(() => {
        passwordInput.focus();
        fireEvent.change(passwordInput, { target: { value: inValidFilledInit.password } });
        passwordInput.blur();

        newPasswordInput.focus();
        fireEvent.change(newPasswordInput, { target: { value: inValidFilledInit.newPassword } });
        newPasswordInput.blur();

        newPasswordConfirmationInput.focus();
        fireEvent.change(newPasswordConfirmationInput, { target: { value: inValidFilledInit.newPasswordConfirmation } });
        newPasswordConfirmationInput.blur();
      });

      expect(passwordInput).toHaveAttribute("aria-invalid", "true");
      expect(newPasswordInput).toHaveAttribute("aria-invalid", "true");
      expect(newPasswordConfirmationInput).toHaveAttribute("aria-invalid", "true");
      expect(getByText("Must be at least 8 characters long")).toHaveClass("Mui-error");
      expect(getByText("Must contain at least one letter")).toHaveClass("Mui-error");
      expect(getByText("Must contain at least one number")).toHaveClass("Mui-error");
    });
  });

  it("removes disabled on click edit button", async () => {
    const { getByText } = render(<PreferencesBox {...otherProps} initialValues={validFilledInit} />);

    const editBtn = getByText("Edit");

    await wait(() => {
      fireEvent.click(editBtn);
    });

    expect(editBtn.firstChild?.textContent).toBe("Save");
  });
});
