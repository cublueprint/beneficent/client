import * as React from "react";
import { cleanup, render, act, fireEvent, getByTitle, getByLabelText, getByRole, getByTestId } from "@testing-library/react";

import PersonalInformation from "../components/ApplicationsLayout/ApplicationDetails/PersonalInformation/PersonalInformation";

const mockProps = {
  officers: [
    {
      workload: 10,
      user: {
        role: "officer",
        isEmailVerified: true,
        firstName: "Fake",
        lastName: "officer",
        phoneNumber: "6131234567",
        address: "Fake Street",
        country: "Canada",
        email: "fake@example.com",
        id: "fakeid",
        updatedAt: "2021-10-02T01:32:00.000Z",
        createdAt: "2021-10-02T01:32:00.000Z"
      },
      createdAt: "2021-10-02T01:32:00.000Z",
      updatedAt: "2021-10-02T01:32:00.000Z",
      id: "fakeid"
    }
  ],

  appInfo: {
    id: "61773f712728c837f8b77e00",
    firstName: "Fake",
    lastName: "Application",
    email: "fake@example.com",
    phoneNumber: "1234567891",
    preferredLanguage: "English",
    maritalStatus: "Separated",
    employmentStatus: "Employed Full Time",
    address: "Fake Address Drive",
    city: "Ottawa",
    province: "Ontario",
    zipCode: "A1B 2C3",
    sex: "Male",
    dateOfBirth: "2021-10-02T01:32:00.000Z",
    citizenship: "Canadian",
    loanAmountRequested: 4000,
    loanType: "credit",
    debtCircumstances: "Please help",
    guarantor: {
      hasGuarantor: true,
      fullName: "",
      phoneNumber: "",
      email: ""
    },
    status: "Recieved",
    recommendationInfo: "Help me please",
    user: "61773f712728c837f8b77e00",
    officer: "61773f712728c837f8b77e00",
    emailOptIn: true
  },

  setAppInfo: () => {}
};

describe("Personal Information", () => {
  it("render correctly", () => {
    const { getAllByText } = render(<PersonalInformation {...mockProps} />);

    expect(getAllByText("Fake Application")).toBeDefined();
  });

  it("handle change text field properly", () => {
    const { getByTestId } = render(<PersonalInformation {...mockProps} />);
    const guarantorName: any = getByTestId("guarantor-name").querySelector("input");
    const guarantorEmail: any = getByTestId("guarantor-email").querySelector("input");
    const guarantorPhone: any = getByTestId("guarantor-number").querySelector("input");

    fireEvent.change(guarantorName, {
      target: {
        value: "Jimmy John"
      }
    });

    fireEvent.change(guarantorEmail, {
      target: {
        value: "jimmyjohn@daboom.com"
      }
    });

    fireEvent.change(guarantorPhone, {
      target: {
        value: "6131234567"
      }
    });

    expect(guarantorName.value).toBe("Jimmy John");
    expect(guarantorEmail.value).toBe("jimmyjohn@daboom.com");
    expect(guarantorPhone.value).toBe("(613)-123-4567");
  });
});
