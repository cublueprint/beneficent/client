import { cleanup, fireEvent, render } from "@testing-library/react";
import moment from "moment";
import * as React from "react";
import MyTable, { ColumnType } from "../components/common/Table/Table";

const data = [
  {
    name: "Ayla Graham",
    email: "bryana.weimann@hotmail.com",
    phoneNumber: "(756) 345-3293",
    address: "597 Dylan Flats",
    city: "Independence",
    province: "Connecticut",
    sex: "Androgyne",
    dateOfBirth: "2001-01-21T00:00:00.000Z",
    createdAt: "2001-01-25T12:00:00.000Z",
    maritalStatus: "Single",
    citizenship: "United Arab Emirates",
    employementStatus: "Employed Full Time",
    loanAmntRequested: 58241,
    loanType: "Other",
    debtCircumstances: "eos consequatur sed",
    guarantor: false,
    recommendationInfo: "est aspernatur qui",
    status: "Received",
    officer: "60f78576b7c1196121500cda",
    user: "60f78576b7c1196121500cd4",
    id: "60f78577b7c1196121500cdc"
  },
  {
    name: "Jan MacGyver",
    email: "tiana_hansen54@gmail.com",
    phoneNumber: "868-933-1741",
    address: "077 Rolfson Lane",
    city: "Mishawaka",
    province: "Pennsylvania",
    sex: "Transmasculine",
    dateOfBirth: "2001-01-21T00:00:00.000Z",
    createdAt: "2001-01-22T12:00:00.000Z",
    maritalStatus: "Single",
    citizenship: "Norway",
    employementStatus: "Employed Full Time",
    loanAmntRequested: 59671,
    loanType: "Other",
    debtCircumstances: "explicabo voluptatem consectetur",
    guarantor: false,
    recommendationInfo: "est explicabo rerum",
    status: "Received",
    officer: "60f78576b7c1196121500cda",
    user: "60f78576b7c1196121500cd5",
    id: "60f78577b7c1196121500cdd"
  },
  {
    name: "Pansy Dickinson",
    email: "vivienne_mraz@hotmail.com",
    phoneNumber: "252-373-1617",
    address: "15401 Block Stravenue",
    city: "Indianapolis",
    province: "Ohio",
    sex: "Cisgender Woman",
    dateOfBirth: "2001-01-21T00:00:00.000Z",
    createdAt: "2001-01-24T12:00:00.000Z",
    maritalStatus: "Single",
    citizenship: "Cocos (Keeling) Islands",
    employementStatus: "Employed Full Time",
    loanAmntRequested: 49325,
    loanType: "Other",
    debtCircumstances: "dolorem tempore iure",
    guarantor: false,
    recommendationInfo: "eligendi natus esse",
    status: "Received",
    officer: "60f78576b7c1196121500cda",
    user: "60f78576b7c1196121500cd6",
    id: "60f78577b7c1196121500cde"
  },
  {
    name: "Terrence Mohr",
    email: "zula.hackett19@yahoo.com",
    phoneNumber: "952-971-2989",
    address: "352 Ericka Loop",
    city: "Salem",
    province: "Georgia",
    sex: "Genderqueer",
    dateOfBirth: "2001-01-21T00:00:00.000Z",
    createdAt: "2001-01-21T12:00:00.000Z",
    maritalStatus: "Single",
    citizenship: "Reunion",
    employementStatus: "Employed Full Time",
    loanAmntRequested: 99374,
    loanType: "Other",
    debtCircumstances: "officia voluptas inventore",
    guarantor: false,
    recommendationInfo: "rerum sit dicta",
    status: "Received",
    officer: "60f78576b7c1196121500cdb",
    user: "60f78576b7c1196121500cd7",
    id: "60f78577b7c1196121500cdf"
  },
  {
    name: "Garrett Schneider",
    email: "emmet28@hotmail.com",
    phoneNumber: "647-756-1241",
    address: "77424 Verdie Bridge",
    city: "Olathe",
    province: "Rhode Island",
    sex: "T* woman",
    dateOfBirth: "2001-01-21T00:00:00.000Z",
    createdAt: "2001-01-23T12:00:00.000Z",
    maritalStatus: "Single",
    citizenship: "Singapore",
    employementStatus: "Employed Full Time",
    loanAmntRequested: 79219,
    loanType: "Other",
    debtCircumstances: "rerum omnis quo",
    guarantor: false,
    recommendationInfo: "voluptas vel qui",
    status: "Received",
    officer: "60f78576b7c1196121500cdb",
    user: "60f78576b7c1196121500cd8",
    id: "60f78577b7c1196121500ce0"
  },
  {
    name: "Santiago Kuhn",
    email: "savanna10@hotmail.com",
    phoneNumber: "1-416-591-0815",
    address: "3380 Katelyn Square",
    city: "Casas Adobes",
    province: "West Virginia",
    sex: "Gender Fluid",
    dateOfBirth: "2001-01-21T00:00:00.000Z",
    createdAt: "2001-01-22T12:00:00.000Z",
    maritalStatus: "Single",
    citizenship: "Armenia",
    employementStatus: "Employed Full Time",
    loanAmntRequested: 49692,
    loanType: "Other",
    debtCircumstances: "vel voluptatem error",
    guarantor: false,
    recommendationInfo: "nobis accusamus et",
    status: "Received",
    officer: "60f78576b7c1196121500cdb",
    user: "60f78576b7c1196121500cd9",
    id: "60f78577b7c1196121500ce1"
  }
];

const columns: ColumnType<any>[] = [
  {
    title: "Name",
    onSort(dir) {},
    render: (record) => record.name
  },
  {
    title: "Date Applied",
    onSort(dir) {},
    render: (record) => moment(record.createdAt).format("MMM D, YYYY")
  },
  {
    title: "Status",
    render: (record) => record.status
  },
  {
    title: "Assigned To",
    render: (record) => record.officer
  }
];
const pagination = {
  onPaginate: (page) => {},
  totalRows: 6,
  perPage: 5,
  page: 1
};

const sort: { defaultIndex: number; defaultDirection: "asc" | "desc" } = {
  defaultIndex: 1,
  defaultDirection: "desc"
};

afterEach(cleanup);

it("fills data", () => {
  const spyFirst = jest.spyOn(columns[0], "render");
  const spyLast = jest.spyOn(columns[columns.length - 1], "render");

  const { getByTestId } = render(<MyTable data={data.slice(0, 5)} columns={columns} pagination={pagination} sort={sort} />);
  const table = getByTestId("table");

  const headers = table.querySelector("thead tr");
  expect(headers?.firstChild).toHaveTextContent("Name");
  expect(headers?.lastChild).toHaveTextContent("Assigned To");

  const rows = table.querySelector(".table tbody");
  expect(rows?.firstChild?.firstChild).toHaveTextContent("Ayla Graham");
  expect(rows?.lastChild?.firstChild).toHaveTextContent("Garrett Schneider");

  expect(spyFirst).toHaveBeenCalled();
  expect(spyLast).toHaveBeenCalled();
});

describe("handles pagination", () => {
  it("handles forward", () => {
    const { getByTestId } = render(
      <MyTable data={data.slice(0, 5)} columns={columns} pagination={pagination} sort={sort} />
    );
    const paginator = getByTestId("pagination");
    const nextPage = getByTestId("nextPage");
    const prevPage = getByTestId("prevPage");
    const paginateSpy = jest.spyOn(pagination, "onPaginate");

    expect(paginator?.querySelector("p")).toHaveTextContent("1-5 of 6");
    expect(prevPage).toHaveAttribute("disabled");
    expect(nextPage).not.toHaveAttribute("disabled");

    prevPage.click();
    expect(paginateSpy).not.toHaveBeenCalled();

    nextPage.click();
    expect(paginateSpy).toHaveBeenCalledWith(2);
  });

  it("handles backward", () => {
    const lastPagePagination = {
      onPaginate: (page) => {},
      totalRows: 6,
      perPage: 5,
      page: 2
    };

    const { getByTestId } = render(
      <MyTable data={data.slice(5, 6)} columns={columns} pagination={lastPagePagination} sort={sort} />
    );
    const paginator = getByTestId("pagination");
    const nextPage = getByTestId("nextPage");
    const prevPage = getByTestId("prevPage");
    const paginateSpy = jest.spyOn(lastPagePagination, "onPaginate");
    expect(paginator?.querySelector("p")).toHaveTextContent("6-6 of 6");
    expect(prevPage).not.toHaveAttribute("disabled");
    expect(nextPage).toHaveAttribute("disabled");

    nextPage.click();
    expect(paginateSpy).not.toHaveBeenCalled();
    prevPage.click();
    expect(paginateSpy).toHaveBeenCalledWith(1);
  });
});

describe("handles sorting", () => {
  it("handles defaults", () => {
    const { getByTestId } = render(<MyTable data={data} columns={columns} pagination={pagination} sort={sort} />);
    const headers = getByTestId("header").querySelectorAll(".row th");

    expect(headers[1]).toHaveAttribute("aria-sort", "descending");
    expect(headers[1].firstChild).toHaveAttribute("role", "button");
    expect(headers[0]).not.toHaveAttribute("aria-sort");
    expect(headers[0].firstChild).toHaveAttribute("role", "button");
  });

  it("toggles sort", () => {
    const mockFn = jest.fn((dir) => {});
    columns[1].onSort = mockFn;
    const { getByTestId, getByText } = render(<MyTable data={data} columns={columns} pagination={pagination} sort={sort} />);
    const headers = getByTestId("header").children;

    fireEvent.click(getByText("Date Applied"));

    expect(headers[1]).toHaveAttribute("aria-sort", "ascending");
    expect(mockFn).toHaveBeenCalledWith("asc");
  });

  it("changes sort column", () => {
    const mockFn = jest.fn((dir) => {});
    columns[0].onSort = mockFn;
    const { getByTestId, getByText } = render(<MyTable data={data} columns={columns} pagination={pagination} sort={sort} />);
    const headers = getByTestId("header").children;

    fireEvent.click(getByText("Name"));

    expect(headers[0]).toHaveAttribute("aria-sort", "descending");
    expect(headers[1]).not.toHaveAttribute("aria-sort");
    expect(mockFn).toHaveBeenCalledWith("desc");
  });
});
