import * as React from "react";
import { cleanup, render, act, fireEvent } from "@testing-library/react";

import LoginForm from "../components/Login/Login";

afterEach(cleanup);

const mockAccount = {
  email: "myemail@abc.com",
  password: "mypassword"
};

const mockProps = {
  handleSubmitLoginForm: jest.fn(),
  validRefresh: jest.fn(),
  refreshToken: ""
};

describe("Login", () => {
  it("handle change text field properly", () => {
    const { getByPlaceholderText } = render(<LoginForm {...mockProps} />);
    const emailInput: any = getByPlaceholderText("Email");
    const passwordInput: any = getByPlaceholderText("Password");
    act(() => {
      fireEvent.change(getByPlaceholderText("Email"), {
        target: { value: mockAccount.email }
      });
      fireEvent.change(getByPlaceholderText("Password"), {
        target: { value: mockAccount.password }
      });
    });

    expect(emailInput.value).toBe(mockAccount.email);
    expect(passwordInput.value).toBe(mockAccount.password);
  });

  it("submit account object correctly", async () => {
    const { getByPlaceholderText, getByTestId } = render(<LoginForm {...mockProps} />);
    await act(async () => {
      await fireEvent.change(getByPlaceholderText("Email"), {
        target: { value: mockAccount.email }
      });
      await fireEvent.change(getByPlaceholderText("Password"), {
        target: { value: mockAccount.password }
      });
      await fireEvent.click(getByTestId("login-button"));
    });

    expect(mockProps.handleSubmitLoginForm).toBeCalled();
    expect(mockProps.handleSubmitLoginForm).toBeCalledWith(mockAccount);
  });

  it("enter invalid email/password show error properly", async () => {
    const { getByPlaceholderText } = render(<LoginForm {...mockProps} />);
    const emailField = getByPlaceholderText("Email");
    const passwordField = getByPlaceholderText("Password");
    await act(async () => {
      // set email field to a text without giving correct form
      await fireEvent.change(emailField, {
        target: { value: "#" }
      });
      emailField.focus();
      emailField.blur();
      // leaving password field blank
      passwordField.focus();
      passwordField.blur();
    });

    // 'aria-invalid' specifies the failed CSS class of Material UI
    expect(emailField).toHaveAttribute("aria-invalid", "true");
    expect(passwordField).toHaveAttribute("aria-invalid", "true");
  });
});
