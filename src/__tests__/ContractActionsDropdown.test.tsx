import { cleanup, fireEvent, render } from "@testing-library/react";
import * as React from "react";
import ContractActionsDropdown from "../components/ApplicationsLayout/ApplicationDetails/ApplicationOverview/ContractDetails/Contracts/ContractActionsDropdown";
afterEach(cleanup);

const mockProps = {
  applicationId: "614fc591b172ae06d2ba1e3a",
  contractId: "614fc591b172ae06d2ba1e3b",
  actions: ["Delete", "Archive", "Upload Signed Contract"],
  contractFileName: "test",
  onChange: jest.fn()
};

describe("renders properly", () => {
  it("initial value should be more options", async () => {
    const component = render(<ContractActionsDropdown {...mockProps}></ContractActionsDropdown>);
    expect(await component.findByText("More Action")).toBeTruthy();
  });

  it("renders options properly", async () => {
    const component = render(<ContractActionsDropdown {...mockProps}></ContractActionsDropdown>);
    const node = await component.findByTestId("select");
    fireEvent.click(node, {
      target: { value: "Delete" }
    });
    expect(await component.findByText("Delete")).toBeTruthy();
  });

  it("sets variables properly onChange", async () => {
    const component = render(<ContractActionsDropdown {...mockProps}></ContractActionsDropdown>);
    const node = await component.findByTestId("select");
    fireEvent.click(node, {
      target: { value: "Archive" }
    });
    expect(await component.findByText("Archive")).toBeTruthy();
    expect(component.findByTestId("modal")).toBeTruthy(); //should be visible
  });
});
