import { cleanup, fireEvent, render } from "@testing-library/react";
import * as React from "react";
import ContractCard from "../components/ApplicationsLayout/ApplicationDetails/ApplicationOverview/ContractDetails/Contracts/ContractCard";
import { apiContracts } from "../services/api/contracts/apiContracts";

afterEach(cleanup);

const mockProps = {
  applicationId: "614fc591b172ae06d2ba1e3a"
};
const mockContract = {
  id: "614fc591b172ae06d2ba1e32",
  status: "Draft",
  contractFileName: "Contract1.pdf",
  createdAt: "2021-10-02T13:30:00.000",
  dateUpdated: "test",
  approvedLoanAmount: 3,
  firstPaymentDue: "test",
  finalPaymentDue: "test",
  contractStartDate: "test",
  client: "test",
  monthlyPayment: 3,
  finalPayment: 5
};

describe("renders properly", () => {
  beforeAll(() => {
    jest.spyOn(apiContracts, "getAll").mockReturnValue(Promise.resolve({ results: [mockContract], totalResults: 1 }));
    jest.spyOn(apiContracts, "download").mockReturnValue(Promise.resolve({ data: "test" }));
    global.URL.createObjectURL = jest.fn();
    global.Storage.prototype.getItem = jest.fn((key) => "token");
  });

  afterAll(() => {
    jest.resetAllMocks();
  });

  it("renders all contracts", async () => {
    const component = render(<ContractCard {...mockProps}></ContractCard>);
    expect(apiContracts.getAll).toBeCalled();
    expect(await component.findByTestId("file-name")).toHaveTextContent("Contract1.pdf");
    expect(await component.findByTestId("uploaded-time")).toHaveTextContent("2021-10-02 1:30 PM");
    expect(await component.findByTestId("status")).toHaveTextContent("Draft");
  });

  it("displays checkmark if status is active", async () => {
    jest
      .spyOn(apiContracts, "getAll")
      .mockReturnValue(Promise.resolve({ results: [{ ...mockContract, status: "Active" }], totalResults: 1 }));
    const component = render(<ContractCard {...mockProps}></ContractCard>);

    expect(await component.findByTestId("status")).toHaveTextContent("Active");
    expect(component.findByTestId("checkmark-icon")).toBeTruthy();
  });

  it("calls download when download icon clicked", async () => {
    const component = render(<ContractCard {...mockProps}></ContractCard>);
    (await component.findByTestId("download-button")).click();
    expect(apiContracts.download).toBeCalledWith("614fc591b172ae06d2ba1e32");
  });
});
