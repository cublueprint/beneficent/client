import * as React from "react";
import { cleanup, render, act, fireEvent, getByTitle, getByLabelText, getByRole, getByTestId } from "@testing-library/react";

import CreateApplication from "../components/ApplicationsLayout/CreateApplication/CreateApplication";

afterEach(cleanup);

const mockApplication = {
  firstName: "Fake",
  lastName: "Application",
  email: "fake@example.com",
  phone: "1234567891",
  preferredLanguage: "English",
  maritalStatus: "Separated",
  employmentStatus: "Employed Full Time",
  address: "Fake Address Drive",
  city: "Ottawa",
  province: "Ontario",
  postalCode: "A1B 2C3",
  sex: "Male",
  dateOfBirth: "2021-10-02T01:32:00.000Z",
  citizenship: "Canadian",
  loanAmountRequested: 4000,
  loanType: "credit",
  debtCircumstances: "Please help",
  guarantor: true,
  recommendationInfo: "Help me please",
  acknowledgements: {
    0: true,
    1: true,
    2: true,
    3: true,
    4: true,
    5: true
  },
  mailOptIn: true
};

const mockProps = {
  officers: [
    {
      workload: 10,
      user: {
        role: "officer",
        isEmailVerified: true,
        firstName: "Fake",
        lastName: "officer",
        phoneNumber: "6131234567",
        address: "Fake Street",
        country: "Canada",
        email: "fake@example.com",
        id: "fakeid",
        updatedAt: "2021-10-02T01:32:00.000Z",
        createdAt: "2021-10-02T01:32:00.000Z"
      },
      createdAt: "2021-10-02T01:32:00.000Z",
      updatedAt: "2021-10-02T01:32:00.000Z",
      id: "fakeid"
    }
  ],
  handleSubmitApplication: jest.fn()
};

describe("Create Application", () => {
  it("render correctly", () => {
    const { getByText } = render(<CreateApplication {...mockProps} />);
    expect(getByText("Personal Information")).toBeDefined();
  });

  it("handle change text field properly", () => {
    const { getByTestId } = render(<CreateApplication {...mockProps} />);
    const firstNameInput: any = getByTestId("firstName").querySelector("input");
    const lastNameInput: any = getByTestId("lastName").querySelector("input");
    const emailInput: any = getByTestId("email").querySelector("input");
    const phoneInput: any = getByTestId("phone").querySelector("input");
    const addressInput: any = getByTestId("address").querySelector("input");
    const cityInput: any = getByTestId("city").querySelector("input");
    const postalCodeInput: any = getByTestId("postalCode").querySelector("input");
    const loanAmountRequestedInput: any = getByTestId("loanAmountRequested").querySelector("input");
    // const debtCircumstancesInput: any = getByTestId("debtCircumstances").querySelector('input');
    const recommendationInfoInput: any = getByTestId("recommendationInfo").querySelector("input");

    fireEvent.change(firstNameInput, {
      target: {
        value: mockApplication.firstName
      }
    });

    fireEvent.change(lastNameInput, {
      target: {
        value: mockApplication.lastName
      }
    });

    fireEvent.change(emailInput, {
      target: {
        value: mockApplication.email
      }
    });

    fireEvent.change(phoneInput, {
      target: {
        value: mockApplication.phone
      }
    });

    fireEvent.change(addressInput, {
      target: {
        value: mockApplication.address
      }
    });

    fireEvent.change(cityInput, {
      target: {
        value: mockApplication.city
      }
    });

    fireEvent.change(postalCodeInput, {
      target: {
        value: mockApplication.postalCode
      }
    });

    fireEvent.change(loanAmountRequestedInput, {
      target: {
        value: mockApplication.loanAmountRequested
      }
    });

    fireEvent.change(recommendationInfoInput, {
      target: {
        value: mockApplication.recommendationInfo
      }
    });

    // not sure why fireEvent isn't working for debtCircumstances
    // fireEvent.change(debtCircumstancesInput, {
    //   target: {
    //     value: mockApplication.debtCircumstances
    //   }
    // });

    expect(firstNameInput.value).toBe(mockApplication.firstName);
    expect(lastNameInput.value).toBe(mockApplication.lastName);
    expect(emailInput.value).toBe(mockApplication.email);
    expect(phoneInput.value).toBe("(123)-456-7891");
    expect(addressInput.value).toBe(mockApplication.address);
    expect(cityInput.value).toBe(mockApplication.city);
    expect(postalCodeInput.value).toBe(mockApplication.postalCode);
    expect(loanAmountRequestedInput.value).toBe("4,000");
    // expect(debtCircumstancesInput.value).toBe(mockApplication.debtCircumstances);
    expect(recommendationInfoInput.value).toBe(mockApplication.recommendationInfo);
  });
});
