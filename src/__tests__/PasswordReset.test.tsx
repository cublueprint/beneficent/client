import { cleanup, fireEvent, render, wait } from "@testing-library/react";
import * as React from "react";
import { BrowserRouter } from "react-router-dom";
import PasswordResetForm from "../components/PasswordReset/PasswordReset";

afterEach(cleanup);

describe("Password Reset", () => {
  /**
   * Renders with default values
   */
  it("renders properly", () => {
    const { getByPlaceholderText, getByText } = render(
      <BrowserRouter>
        <PasswordResetForm />
      </BrowserRouter>
    );

    const passInput = getByPlaceholderText("Password");
    expect(passInput).toHaveAttribute("type", "password");
    expect(passInput).not.toHaveValue();

    const confirmInput = getByPlaceholderText("Verify Password");
    expect(confirmInput).toHaveAttribute("type", "password");
    expect(confirmInput).not.toHaveValue();

    const submitButton = getByText("Reset");
    expect(submitButton).toBeDisabled();

    expect(getByText("Reset Password")).toHaveClass("heading");
  });

  /**
   * Validates correctly with valid and invalid inputs
   */
  describe("handles validation correctly", () => {
    it("handles invalid inputs", async () => {
      const { getByPlaceholderText } = render(
        <BrowserRouter>
          <PasswordResetForm />
        </BrowserRouter>
      );

      const passInput = getByPlaceholderText("Password");
      const confirmInput = getByPlaceholderText("Verify Password");

      await wait(() => {
        passInput.focus();
        fireEvent.change(passInput, { target: { value: "s" } });
        passInput.blur();
        confirmInput.focus();
        fireEvent.change(confirmInput, { target: { value: "some" } });
        confirmInput.blur();
      });

      expect(passInput).toHaveAttribute("aria-invalid", "true");
      expect(confirmInput).toHaveAttribute("aria-invalid", "true");
    });

    it("handles valid inputs", async () => {
      const { getByPlaceholderText } = render(
        <BrowserRouter>
          <PasswordResetForm />
        </BrowserRouter>
      );

      const passInput = getByPlaceholderText("Password");
      const confirmInput = getByPlaceholderText("Verify Password");

      await wait(() => {
        fireEvent.change(passInput, { target: { value: "NewPassword123!" } });
        passInput.blur();

        fireEvent.change(confirmInput, { target: { value: "NewPassword123!" } });
        confirmInput.blur();
      });

      expect(passInput).toHaveAttribute("aria-invalid", "false");
      expect(confirmInput).toHaveAttribute("aria-invalid", "false");
    });
  });
});
