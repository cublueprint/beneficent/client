export interface createClient {
  dateSigned: string;
  officer: string;
  application: string;
}

export interface getClients {
  nameOrEmail?: string;
  status?: string[] | string;
  officer?: string[] | string;
  startDate?: string;
  endDate?: string;
  populate?: string;
  sortBy?: string;
  limit?: number;
  page?: number;
  application?: string;
}

export interface updateClient {
  officer?: string;
  status?: string;
  contract?: string;
}
