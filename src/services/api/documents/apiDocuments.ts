import { ApiCore } from "../utilities/core";

export const apiDocuments = new ApiCore({
  getAll: true,
  getSingle: true,
  post: true,
  patch: true,
  delete: true,
  upload: true,
  download: true,
  generateHeaders: true,
  url: "documents"
});
