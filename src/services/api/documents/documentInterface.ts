export interface getDocuments {
  docType?: string;
  originalName?: string;
  application?: string;
  populate?: string;
  sortBy?: string;
  limit?: string;
  page?: string;
}

export interface uploadDocument {
  docType: string;
  application: string;
  file: File;
  uploadedBy: string;
}

export interface updateDocument {
  docType: string;
  updatedBy: string;
}
