export interface getUsers {
  nameOrEmail?: string;
  phoneNumber?: string;
  address?: string;
  country?: string;
  role?: string;
  populate?: string;
  sortBy?: string;
  limit?: number;
  page?: number;
}

export interface createUser {
  firstName: string;
  lastName: string;
  phoneNumber: string;
  address: string | null;
  country: string | null;
  email: string;
  password: string | null;
  role: string;
  isLoanOfficer: boolean;
}

export interface updateUser {
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  address?: string;
  country?: string;
  email?: string;
  role?: string;
  password?: string;
  currentPassword?: string;
}
