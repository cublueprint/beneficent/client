import { ApiCore } from "../utilities/core";

export const apiPayments = new ApiCore({
  getAll: true,
  getSingle: false,
  post: false,
  patch: false,
  delete: false,
  upload: false,
  download: false,
  url: "payments"
});
