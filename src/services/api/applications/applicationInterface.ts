export interface createApplication {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  address: string;
  city: string;
  province: string;
  sex: string;
  dateOfBirth: string;
  maritalStatus: string;
  citizenship: string;
  postalCode: string;
  preferredLanguage: string;
  employmentStatus: string;
  loanAmountRequested: number;
  loanType: string;
  debtCircumstances: string;
  guarantor?: {
    hasGuarantor: boolean;
    fullName?: string;
    email?: string;
    phoneNumber?: string;
  };
  recommendationInfo: string;
  acknowledgements: {
    loanPurpose: boolean;
    maxLoan: boolean;
    repayment: boolean;
    residence: boolean;
    netPositiveIncome: boolean;
    guarantorConsent: boolean;
  };
  services: {
    careerCoaching: boolean;
    resumeCritique: boolean;
    financialCoaching: boolean;
    communityResources: boolean;
  };
  emailOptIn: boolean;
  status: string;
  officer?: string;
  user?: string;
}

export interface getApplications {
  nameOrEmail?: string;
  status?: string[] | string;
  displayAll?: boolean;
  officer?: string[] | string;
  user?: string;
  populate?: string;
  sortBy?: string;
  limit?: number;
  page?: number;
}

export interface updateApplication {
  firstName?: string;
  lastName?: string;
  email?: string;
  phoneNumber?: string;
  address?: string;
  city?: string;
  province?: string;
  sex?: string;
  dateOfBirth?: string;
  maritalStatus?: string;
  citizenship?: string;
  postalCode?: string;
  preferredLanguage?: string;
  employmentStatus?: string;
  loanAmountRequested?: number;
  loanType?: string;
  debtCircumstances?: string;
  guarantor?: {
    hasGuarantor?: boolean;
    fullName?: string;
    email?: string;
    phoneNumber?: string;
  };
  acknowledgements?: {
    loanPurpose?: boolean;
    maxLoan: boolean;
    repayment: boolean;
    residence: boolean;
    netPositiveIncome: boolean;
    guarantorConsent: boolean;
  };
  services?: {
    careerCoaching: boolean;
    resumeCritique: boolean;
    financialCoaching: boolean;
    communityResources: boolean;
  };
  recommendationInfo?: string;
  emailOptIn?: boolean;
  status?: string;
  officer?: string;
  user?: string;
}
