import { AxiosError, AxiosResponse } from "axios";
import { apiProvider } from "./provider";
import { GetModel, UpdateModel, CreateModel, UploadModel } from "./types";

export class ApiCore {
  getAll: (model?: GetModel) => Promise<any> | Promise<AxiosError<any>>;
  getSingle: (id: string) => Promise<AxiosResponse<any>> | Promise<AxiosError<any>>;
  post: (model: CreateModel) => Promise<AxiosResponse<any>> | Promise<AxiosError<any>>;
  patch: (id: string, model: UpdateModel) => Promise<AxiosResponse<any>> | Promise<AxiosError<any>>;
  remove: (id: string) => Promise<AxiosResponse<any>> | Promise<AxiosError<any>>;
  upload: (model: UploadModel) => Promise<AxiosResponse<any>> | Promise<AxiosError<any>>;
  download: (id: string) => Promise<any> | Promise<AxiosError<any>>;
  generateHeaders: () => any;

  constructor(options) {
    this.getAll = (model?: GetModel): Promise<any> | Promise<AxiosError<any>> => {
      return apiProvider.getAll(options.url, model);
    };

    this.getSingle = (id: string): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.getSingle(options.url, id);
    };

    this.post = (model: CreateModel): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.post(options.url, model);
    };

    this.patch = (id: string, model: UpdateModel): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.patch(options.url, id, model);
    };

    this.remove = (id: string): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.remove(options.url, id);
    };

    this.generateHeaders = () => {
      return apiProvider.generateHeaders();
    };

    this.upload = (model: UploadModel): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.upload(options.url, model);
    };

    this.download = (id: string): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.download(options.url, id);
    };
  }
}
