import { createApplication, updateApplication, getApplications } from "../applications/applicationInterface";
import { createClient, getClients, updateClient } from "../clients/clientInterface";
import { createContract, updateContract, getContracts, uploadContract } from "../contracts/contractInterface";

import { getDocuments, updateDocument, uploadDocument } from "../documents/documentInterface";

import { createOfficer, updateOfficer, getOfficers } from "../officers/officerInterface";
import { createUser, updateUser, getUsers } from "../users/userInterface";
import { getPayments } from "../payments/paymentsInterface";

export interface CreateModel {
  contracts?: createContract;
  users?: createUser;
  clients?: createClient;
  applications?: createApplication;
  officers?: createOfficer;
}

export interface GetModel {
  contracts?: getContracts;
  users?: getUsers;
  clients?: getClients;
  applications?: getApplications;
  officers?: getOfficers;
  documents?: getDocuments;
  payments?: getPayments;
}

export interface UpdateModel {
  users?: updateUser;
  clients?: updateClient;
  contracts?: updateContract;
  applications?: updateApplication;
  officers?: updateOfficer;
  documents?: updateDocument;
}

export interface ApiModel {
  createModel?: CreateModel;
  getModel?: GetModel;
  updateModel?: UpdateModel;
}

export interface UploadModel {
  documents?: uploadDocument;
  contracts?: uploadContract;
}
