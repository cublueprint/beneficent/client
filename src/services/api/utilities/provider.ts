import axios, { AxiosError, AxiosResponse } from "axios";
import { GetModel, UpdateModel, CreateModel, UploadModel } from "./types";

export const BASE_URL = `${process.env.REACT_APP_API_ENDPOINT || ""}/api/v1`;

/** @param {string} resource*/
/** @param {GetModel} model */
const getAll = (resource: string, model?: GetModel): Promise<any> | Promise<AxiosError<any>> => {
  let urlPath = resource;

  const params = new URLSearchParams();

  if (model !== undefined) {
    for (const key in model[resource]) {
      if (Array.isArray(model[resource][key])) {
        for (const item of model[resource][key]) {
          params.append(key, item);
        }
      } else {
        params.append(key, model[resource][key]);
      }
    }

    urlPath += "?" + params;
  }

  return axios
    .get(`${BASE_URL}/${urlPath}`, generateHeaders())
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} resource */
/** @param {string} id */
const getSingle = (resource: string, id: string): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
  return axios
    .get(`${BASE_URL}/${resource}/${id}`, generateHeaders())
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} resource */
/** @param {CreateModel} model */
const post = (resource: string, model: CreateModel): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
  return axios
    .post(`${BASE_URL}/${resource}`, model[resource], generateHeaders())
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} resource */
/** @param {string} id */
/** @param {UpdateModel} model */
const patch = (resource: string, id: string, model: UpdateModel): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
  return axios
    .patch(`${BASE_URL}/${resource}/${id}`, model[resource], generateHeaders())
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} resource */
/** @param {string} id */
const remove = (resource: string, id: string): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
  return axios
    .delete(`${BASE_URL}/${resource}/${id}`, generateHeaders())
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} resource */
/** @param {UploadModel} model */
const upload = (resource: string, model: UploadModel): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
  const token = localStorage.getItem("accessToken");
  const formData = new FormData();
  for (const key in model[resource]) {
    formData.append(key, model[resource][key]);
  }
  return axios
    .post(`${BASE_URL}/${resource}`, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${token}`
      }
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} resource */
/** @param {string} id */
const download = (resource: string, id: string): Promise<any> | Promise<AxiosError<any>> => {
  const token = localStorage.getItem("accessToken");
  return axios
    .get(`${BASE_URL}/${resource}/${id}/download`, {
      responseType: "arraybuffer",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/pdf",
        Authorization: `Bearer ${token}`
      }
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {ApiModel} model */
const generateHeaders = (): any => {
  const token = localStorage.getItem("accessToken");
  return {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    }
  };
};

export const apiProvider = {
  getAll,
  getSingle,
  post,
  patch,
  remove,
  upload,
  download,
  generateHeaders
};
