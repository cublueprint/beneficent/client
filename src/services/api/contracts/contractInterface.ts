export interface createContract {
  approvedLoanAmount: number;
  firstPaymentDue: string;
  finalPaymentDue: string;
  monthlyPayment: number;
  finalPayment: number;
  status: string;
  application: string;
  client?: string;
  createdBy: string;
}

export interface getContracts {
  application?: string;
  client?: string;
  officer?: string;
  populate?: string;
  sortBy?: string;
  limit?: number;
  page?: number;
  status?: string;
}

export interface updateContract {
  approvedLoanAmount?: string;
  firstPaymentDue?: string;
  finalPaymentDue?: string;
  monthlyPayment?: number;
  finalPayment?: number;
  status?: string;
  client?: string;
}

export interface uploadContract {
  file: File;
}
