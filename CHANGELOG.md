# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [7.4.10](https://gitlab.com/cublueprint/beneficent/client/compare/v7.4.9...v7.4.10) (2022-03-21)

### [7.4.9](https://gitlab.com/cublueprint/beneficent/client/compare/v7.4.8...v7.4.9) (2022-03-20)

### [7.4.8](https://gitlab.com/cublueprint/beneficent/client/compare/v7.4.7...v7.4.8) (2022-03-19)

### [7.4.7](https://gitlab.com/cublueprint/beneficent/client/compare/v7.4.6...v7.4.7) (2022-03-18)

### [7.4.6](https://gitlab.com/cublueprint/beneficent/client/compare/v7.4.5...v7.4.6) (2022-03-17)

### [7.4.5](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/client/compare/v7.4.4...v7.4.5) (2022-03-16)


### Bug Fixes

* incorrect active contract info (EN-547) ([a199865](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/client/commit/a19986537815cd6dddf45b1ad31e7af79c4967fe))
* support empty loan officer dropdown ([0f41155](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/client/commit/0f411557b2b6b508f07cd55b2ff091ebfc85141a))

### [7.4.4](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/client/compare/v7.4.3...v7.4.4) (2022-03-15)


### Bug Fixes

* email verification UI text (EN-571) ([8ea6023](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/client/commit/8ea602309c5e07b37b331ba13a8c0a33bd1cfa2d))

### [7.4.3](https://gitlab.com/cublueprint/beneficent/client/compare/v7.4.2...v7.4.3) (2022-03-15)

### [7.4.2](https://gitlab.com/cublueprint/beneficent/client/compare/v7.4.1...v7.4.2) (2022-03-14)

### [7.4.1](https://gitlab.com/cublueprint/beneficent/client/compare/v7.4.0...v7.4.1) (2022-03-13)

## [7.4.0](https://gitlab.com/cublueprint/beneficent/client/compare/v7.3.7...v7.4.0) (2022-03-12)


### Features

* resend verification email button (EN-560) ([0718cf7](https://gitlab.com/cublueprint/beneficent/client/commit/0718cf7bcf0eafc75f43210b741ed96d45258b03))

### [7.3.7](https://gitlab.com/cublueprint/beneficent/client/compare/v7.3.6...v7.3.7) (2022-03-12)


### Bug Fixes

* Fix account manager search field not working (EN-558) ([4d38f21](https://gitlab.com/cublueprint/beneficent/client/commit/4d38f21b38ba283611cac1f8adf5097f9f84dbd3))

### [7.3.6](https://gitlab.com/cublueprint/beneficent/client/compare/v7.3.5...v7.3.6) (2022-03-11)


### Bug Fixes

* edit account phone number (EN-565) ([b95dad2](https://gitlab.com/cublueprint/beneficent/client/commit/b95dad242ec9377236fb238ef5ddc1e3db5dfad7))

### [7.3.5](https://gitlab.com/cublueprint/beneficent/client/compare/v7.3.4...v7.3.5) (2022-03-10)


### Bug Fixes

* email verification (EN-552) ([545ebe1](https://gitlab.com/cublueprint/beneficent/client/commit/545ebe15a0c805b623c262d3d54c876d1782b8e3))

### [7.3.4](https://gitlab.com/cublueprint/beneficent/client/compare/v7.3.3...v7.3.4) (2022-03-10)

### [7.3.3](https://gitlab.com/cublueprint/beneficent/client/compare/v7.3.2...v7.3.3) (2022-03-09)

### [7.3.2](https://gitlab.com/cublueprint/beneficent/client/compare/v7.3.1...v7.3.2) (2022-03-08)

### [7.3.1](https://gitlab.com/cublueprint/beneficent/client/compare/v7.3.0...v7.3.1) (2022-03-07)


### Bug Fixes

* account manager permission modals (EN-549) ([a206221](https://gitlab.com/cublueprint/beneficent/client/commit/a206221524e37256180da1c32834ff4e63e09a20))

## [7.3.0](https://gitlab.com/cublueprint/beneficent/client/compare/v7.2.4...v7.3.0) (2022-03-06)


### Features

* contract length modal (EN-530) ([b391836](https://gitlab.com/cublueprint/beneficent/client/commit/b391836833d4e1fb2b8f3456f53d084f563be219))

### [7.2.4](https://gitlab.com/cublueprint/beneficent/client/compare/v7.2.3...v7.2.4) (2022-03-05)

### [7.2.3](https://gitlab.com/cublueprint/beneficent/client/compare/v7.2.2...v7.2.3) (2022-03-04)

### [7.2.2](https://gitlab.com/cublueprint/beneficent/client/compare/v7.2.1...v7.2.2) (2022-03-03)

### [7.2.1](https://gitlab.com/cublueprint/beneficent/client/compare/v7.2.0...v7.2.1) (2022-03-02)

## [7.2.0](https://gitlab.com/cublueprint/beneficent/client/compare/v7.1.2...v7.2.0) (2022-03-01)


### Features

* create edit user page (EN-548) ([2cadfc5](https://gitlab.com/cublueprint/beneficent/client/commit/2cadfc581bf9ec757860f303770eb08bf320971d))

### [7.1.2](https://gitlab.com/cublueprint/beneficent/client/compare/v7.1.1...v7.1.2) (2022-02-28)

### [7.1.1](https://gitlab.com/cublueprint/beneficent/client/compare/v7.1.0...v7.1.1) (2022-02-27)

## [7.1.0](https://gitlab.com/cublueprint/beneficent/client/compare/v7.0.2...v7.1.0) (2022-02-26)


### Features

* Account Manager Table ([cebac5d](https://gitlab.com/cublueprint/beneficent/client/commit/cebac5d72a474321e4f27c13f207d6221f083a5a))

### [7.0.2](https://gitlab.com/cublueprint/beneficent/client/compare/v7.0.1...v7.0.2) (2022-02-25)

### [7.0.1](https://gitlab.com/cublueprint/beneficent/client/compare/v7.0.0...v7.0.1) (2022-02-24)

## [7.0.0](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.15...v7.0.0) (2022-02-24)


### ⚠ BREAKING CHANGES

* add create user page (EN-534)

### Features

* add create user page (EN-534) ([67e528d](https://gitlab.com/cublueprint/beneficent/client/commit/67e528d111fcff54bad4bd3e9a4ae6c1832bd744))

### [6.2.15](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.14...v6.2.15) (2022-02-23)

### [6.2.14](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.13...v6.2.14) (2022-02-22)

### [6.2.13](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.12...v6.2.13) (2022-02-21)

### [6.2.12](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.11...v6.2.12) (2022-02-20)

### [6.2.11](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.10...v6.2.11) (2022-02-19)

### [6.2.10](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.9...v6.2.10) (2022-02-18)


### Bug Fixes

* fix expected payment calculation on frontend (EN-545) ([2a95a88](https://gitlab.com/cublueprint/beneficent/client/commit/2a95a88afd7388705ef31001088b68b91c3d3593))
* fix too many contracts not showing loan summary (EN-543) ([98d8ddc](https://gitlab.com/cublueprint/beneficent/client/commit/98d8ddcacd272d15a8d96a6adb2e1806a6ab884d))

### [6.2.9](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.8...v6.2.9) (2022-02-17)

### [6.2.8](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.7...v6.2.8) (2022-02-16)

### [6.2.7](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.6...v6.2.7) (2022-02-15)


### Bug Fixes

* apply changes to edit application (EN-540, EN-459) ([2b5e4eb](https://gitlab.com/cublueprint/beneficent/client/commit/2b5e4eb9ef1c259ac1c8162beb04762ae5afdbe7))

### [6.2.6](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.5...v6.2.6) (2022-02-15)


### Bug Fixes

* create application small fixes (EN-459, EN-538, EN-540, EN-457) ([cfdda53](https://gitlab.com/cublueprint/beneficent/client/commit/cfdda539c9d95a5b4c4dbfeba768e80cd7a6ab76))

### [6.2.5](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.4...v6.2.5) (2022-02-15)

### [6.2.4](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.3...v6.2.4) (2022-02-14)

### [6.2.3](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.2...v6.2.3) (2022-02-13)


### Bug Fixes

* external application form layout (EN-513) ([01c9c95](https://gitlab.com/cublueprint/beneficent/client/commit/01c9c953dae673e17bd8c7067b193372a75432ab))

### [6.2.2](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.1...v6.2.2) (2022-02-12)


### Bug Fixes

* client payment displays (EN-489) ([35bdf6d](https://gitlab.com/cublueprint/beneficent/client/commit/35bdf6dae1171eb0adae8f1c2564c00b715d8914))

### [6.2.1](https://gitlab.com/cublueprint/beneficent/client/compare/v6.2.0...v6.2.1) (2022-02-11)


### Bug Fixes

* add additional fields to create/view/edit application forms (EN-524) ([bbf649d](https://gitlab.com/cublueprint/beneficent/client/commit/bbf649d0b17b68baeb36360979b1f3c5b32ba9a7))
* allow array api inputs (EN-407) ([99e034e](https://gitlab.com/cublueprint/beneficent/client/commit/99e034e5fbc079359751e9972b450d693a2c8f09))
* fix codes for redirect link in home page (EN-514) ([c8532b9](https://gitlab.com/cublueprint/beneficent/client/commit/c8532b93263a8731db64652c51dc8207e9864635))
* homepage routing (EN-527) ([3eb5e6f](https://gitlab.com/cublueprint/beneficent/client/commit/3eb5e6f0d3b767ab04cdb7645b5cb005660ee521))

## [6.2.0](https://gitlab.com/cublueprint/beneficent/client/compare/v6.1.5...v6.2.0) (2022-02-03)


### Features

* Make Homepage the Landing Page (EN-527) ([8afd71a](https://gitlab.com/cublueprint/beneficent/client/commit/8afd71ac338c30150e0c5f4aa4e14cc92ccf14a0))
* redirect home page for users and admins (EN-514) ([ddd3b13](https://gitlab.com/cublueprint/beneficent/client/commit/ddd3b133370f81768052fdfac1e20e3f239cccec))

### [6.1.5](https://gitlab.com/cublueprint/beneficent/client/compare/v6.1.4...v6.1.5) (2022-02-02)

### [6.1.4](https://gitlab.com/cublueprint/beneficent/client/compare/v6.1.3...v6.1.4) (2022-02-01)

### [6.1.3](https://gitlab.com/cublueprint/beneficent/client/compare/v6.1.2...v6.1.3) (2022-01-31)

### [6.1.2](https://gitlab.com/cublueprint/beneficent/client/compare/v6.1.1...v6.1.2) (2022-01-30)

### [6.1.1](https://gitlab.com/cublueprint/beneficent/client/compare/v6.1.0...v6.1.1) (2022-01-29)

## [6.1.0](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.17...v6.1.0) (2022-01-28)


### Features

* homepage ([37e9b18](https://gitlab.com/cublueprint/beneficent/client/commit/37e9b18c7660adcd161a714e70e05c5ac4efa435))


### Bug Fixes

* add event handler on back button click for up-to-date information (EN-449) ([53c1e66](https://gitlab.com/cublueprint/beneficent/client/commit/53c1e663c8441edfbab3b234a2079e3c325a6527))

### [6.0.17](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.16...v6.0.17) (2022-01-27)

### [6.0.16](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.15...v6.0.16) (2022-01-26)

### [6.0.15](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.14...v6.0.15) (2022-01-25)

### [6.0.14](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.13...v6.0.14) (2022-01-24)


### Bug Fixes

* display unassigned dropdown (EN-493) ([b77d91a](https://gitlab.com/cublueprint/beneficent/client/commit/b77d91a04d8c2336de056c6390715fc29eff6d03))

### [6.0.13](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.12...v6.0.13) (2022-01-23)

### [6.0.12](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.11...v6.0.12) (2022-01-22)

### [6.0.11](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.10...v6.0.11) (2022-01-21)

### [6.0.10](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.9...v6.0.10) (2022-01-20)

### [6.0.9](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.8...v6.0.9) (2022-01-20)

### [6.0.8](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.7...v6.0.8) (2022-01-19)

### [6.0.7](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.6...v6.0.7) (2022-01-18)

### [6.0.6](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.5...v6.0.6) (2022-01-17)

### [6.0.5](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.4...v6.0.5) (2022-01-16)

### [6.0.4](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.3...v6.0.4) (2022-01-15)

### [6.0.3](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.2...v6.0.3) (2022-01-14)

### [6.0.2](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.1...v6.0.2) (2022-01-13)

### [6.0.1](https://gitlab.com/cublueprint/beneficent/client/compare/v6.0.0...v6.0.1) (2022-01-12)

## [6.0.0](https://gitlab.com/cublueprint/beneficent/client/compare/v5.1.4...v6.0.0) (2022-01-11)


### ⚠ BREAKING CHANGES

* move release pipeline into a template

### Features

* move release pipeline into a template ([7d458b0](https://gitlab.com/cublueprint/beneficent/client/commit/7d458b011e53f0fcba6994a250271bd07c592726))

### [5.1.4](https://gitlab.com/cublueprint/beneficent/client/compare/v5.1.3...v5.1.4) (2022-01-11)

### [5.1.3](https://gitlab.com/cublueprint/beneficent/client/compare/v5.1.2...v5.1.3) (2022-01-10)

### [5.1.2](https://gitlab.com/cublueprint/beneficent/client/compare/v5.1.1...v5.1.2) (2022-01-09)

### [5.1.1](https://gitlab.com/cublueprint/beneficent/client/compare/v5.1.0...v5.1.1) (2022-01-09)

## [5.1.0](https://gitlab.com/cublueprint/beneficent/client/compare/v5.0.11...v5.1.0) (2022-01-09)


### Features

* add modal when status change fails (EN-478) ([e264b36](https://gitlab.com/cublueprint/beneficent/client/commit/e264b366d0c21d2200a62cd18fafdf3b5b0ee381))

### [5.0.11](https://gitlab.com/cublueprint/beneficent/client/compare/v5.0.10...v5.0.11) (2022-01-09)


### Bug Fixes

* use date applied (EN-498) ([495150a](https://gitlab.com/cublueprint/beneficent/client/commit/495150a26b7b07259cc5b17696e34b761692b954))

### [5.0.10](https://gitlab.com/cublueprint/beneficent/client/compare/v5.0.9...v5.0.10) (2022-01-06)

### [5.0.9](https://gitlab.com/cublueprint/beneficent/client/compare/v5.0.8...v5.0.9) (2022-01-06)

### [5.0.8](https://gitlab.com/cublueprint/beneficent/client/compare/v5.0.7...v5.0.8) (2022-01-05)

### [5.0.7](https://gitlab.com/cublueprint/beneficent/client/compare/v5.0.6...v5.0.7) (2022-01-02)

### [5.0.6](https://gitlab.com/cublueprint/beneficent/client/compare/v5.0.5...v5.0.6) (2021-12-30)

### [5.0.5](https://gitlab.com/cublueprint/beneficent/client/compare/v5.0.4...v5.0.5) (2021-12-30)


### Bug Fixes

* contract view more modal values (EN-472) ([ac183cf](https://gitlab.com/cublueprint/beneficent/client/commit/ac183cfccf2a2bdbcabc0b3eab541e45fa5f47ad))
* fix expected payment amount not being displayed correctly (EN-489) ([48b631b](https://gitlab.com/cublueprint/beneficent/client/commit/48b631b8a9cc63b1f78bc1157bb7fb976154e895))
* Loan summary bugs (EN-489) ([6e30017](https://gitlab.com/cublueprint/beneficent/client/commit/6e300172ad99e8723df85e5db4f336feacb22f95))

### [5.0.4](https://gitlab.com/cublueprint/beneficent/client/compare/v5.0.3...v5.0.4) (2021-12-28)

### [5.0.3](https://gitlab.com/cublueprint/beneficent/client/compare/v5.0.2...v5.0.3) (2021-12-27)


### Bug Fixes

* Update client contract status addition (EN-481) ([60c00f8](https://gitlab.com/cublueprint/beneficent/client/commit/60c00f8e16ebedf09e8cad156bbd1469fd1606d6))

### [5.0.2](https://gitlab.com/cublueprint/beneficent/client/compare/v5.0.1...v5.0.2) (2021-12-23)


### Bug Fixes

* allow archived to active (EN-482) ([4901640](https://gitlab.com/cublueprint/beneficent/client/commit/4901640bad033315cb3bfd58297a267e144b134c))

### [5.0.1](https://gitlab.com/cublueprint/beneficent/client/compare/v5.0.0...v5.0.1) (2021-12-15)


### Bug Fixes

* button to open wave invoice page (EN-424) ([6570f3e](https://gitlab.com/cublueprint/beneficent/client/commit/6570f3eafc37c2e1718183ad5cd3c5fe6acfa2db))

## [5.0.0](https://gitlab.com/cublueprint/beneficent/client/compare/v4.0.0...v5.0.0) (2021-12-12)


### ⚠ BREAKING CHANGES

* remove guarantor name from contract (EN-424)

### Features

* added status change modals (EN-419) ([ace4ced](https://gitlab.com/cublueprint/beneficent/client/commit/ace4ced6e835dabfc1499453fc288bb6613d78b7))
* loan summary card (EN-403) ([97e0d16](https://gitlab.com/cublueprint/beneficent/client/commit/97e0d1669e1d830a85f985dc5333a66f4e2313c2))


### Bug Fixes

* edit application page loan officer drop down (EN-448) ([7e6a97f](https://gitlab.com/cublueprint/beneficent/client/commit/7e6a97ff438b6197ec4dfbd4410cdb069a066763))
* Fix create application page not adding officer on submit (EN-456) ([fea9227](https://gitlab.com/cublueprint/beneficent/client/commit/fea922719fc6a85f7ba201ca7623218efc1f2cab))
* remove guarantor name from contract (EN-424) ([377b64a](https://gitlab.com/cublueprint/beneficent/client/commit/377b64a7d67007bf7881a5e61544969a3c181165))
* rerender on date change (EN-413) ([ede23e3](https://gitlab.com/cublueprint/beneficent/client/commit/ede23e3f2fecfbd38f0a7b021305d5f1e6cd0cf4))
* set documents uploadedby to logged in user (EN-445) ([2ae647d](https://gitlab.com/cublueprint/beneficent/client/commit/2ae647d1411f5905124e3f363ceae789fc8d1482))

## [4.0.0](https://gitlab.com/cublueprint/beneficent/client/compare/v3.1.1...v4.0.0) (2021-12-03)


### ⚠ BREAKING CHANGES

* replace axios with api service and remove token requirement (EN-406)

### Features

* frontend payments card table (EN-404) ([49dcfe0](https://gitlab.com/cublueprint/beneficent/client/commit/49dcfe0838b834e636348cd388272227847fa598))


### Bug Fixes

* fix modal logic (EN-426) ([9fa1b7b](https://gitlab.com/cublueprint/beneficent/client/commit/9fa1b7bdbea6dd68154ca059005b754c92a1261c))
* refactor status dropdown (EN-422) ([98e1634](https://gitlab.com/cublueprint/beneficent/client/commit/98e16346052731d043484b3a06e05ec177cbafc5))
* replace axios with api service and remove token requirement (EN-406) ([d3176ec](https://gitlab.com/cublueprint/beneficent/client/commit/d3176ecdd6d2ebfbecf552a0b9b587b75785a434))
* update created by field in contract card and conflicts in related files (EN-437) ([928bc5e](https://gitlab.com/cublueprint/beneficent/client/commit/928bc5eec9a1098bb46dad6ee6bc5e1a01628026))

### [3.1.1](https://gitlab.com/cublueprint/beneficent/client/compare/v3.1.0...v3.1.1) (2021-11-24)

## [3.1.0](https://gitlab.com/cublueprint/beneficent/client/compare/v3.0.0...v3.1.0) (2021-11-24)


### Features

* add contract modals (EN-325) ([3bb3290](https://gitlab.com/cublueprint/beneficent/client/commit/3bb32907eacd20d8c4fda511fe566783de311e15))
* contract upload modal (EN-214) ([af373c0](https://gitlab.com/cublueprint/beneficent/client/commit/af373c00c766bab1ed7a489fe529cfeeb7d9ae60))


### Bug Fixes

* client detail overview (EN-415) ([2b594bf](https://gitlab.com/cublueprint/beneficent/client/commit/2b594bffa5a162b660899bbdb2c494e3c5471b0c))
* fix application overview to better match design (EN-417) ([d96e831](https://gitlab.com/cublueprint/beneficent/client/commit/d96e831d63c7de2f8d4f264d0d173efe5680f4fb))

## [3.0.0](https://gitlab.com/cublueprint/beneficent/client/compare/v2.0.0...v3.0.0) (2021-11-15)


### ⚠ BREAKING CHANGES

* add postal code to edit application (EN-402)

### Features

* add contract component (EN-380) ([44838fb](https://gitlab.com/cublueprint/beneficent/client/commit/44838fbb1275887b01e64668b20de717fc1568bd))
* add create contract page (EN-378) ([c61ac6d](https://gitlab.com/cublueprint/beneficent/client/commit/c61ac6dc9d473dc37f7ebe48d97d1cdf5b0c1d37))
* Add onChange prop to StatusDropdown ([2209db9](https://gitlab.com/cublueprint/beneficent/client/commit/2209db9d72add2d1dec24321bf9c99edf2ea7a94))
* add postal code to edit application (EN-402) ([8c8647f](https://gitlab.com/cublueprint/beneficent/client/commit/8c8647f560c32ae065735307448c3847bd12f50c))
* added delete button to documents (EN-409) ([8228dd9](https://gitlab.com/cublueprint/beneficent/client/commit/8228dd9cccc17dc6203420aab9103a213e69cbc3))
* application detail view - personal information card (EN-377) ([e80b51c](https://gitlab.com/cublueprint/beneficent/client/commit/e80b51c5488b7d51de2a52968b53ec0a77e54c00))
* creating warning modal (EN-345) ([906a19e](https://gitlab.com/cublueprint/beneficent/client/commit/906a19e54725a067d60d866b35efbfb5a5d899f8))
* Fix client status Archive to Archived ([c7f084a](https://gitlab.com/cublueprint/beneficent/client/commit/c7f084a5e8812fffff0ec3ac16c87fcf3d7b7eef))
* frontend create document card (EN-382) ([b85500d](https://gitlab.com/cublueprint/beneficent/client/commit/b85500d20ec9e050cc639bf96db37c067c157965))
* hooking up the API to the table filters: applications and clients list view ([3d1c9f5](https://gitlab.com/cublueprint/beneficent/client/commit/3d1c9f5638ee4ae1b0c559bf11d44b2f1fbc981a))
* import personal information card (EN-379) ([7573a6d](https://gitlab.com/cublueprint/beneficent/client/commit/7573a6dbc9b6900050bfca360b8890a31140b6a9))
* upload document modal (EN-383) ([7e9c0cb](https://gitlab.com/cublueprint/beneficent/client/commit/7e9c0cb5615551d5996d63334b8e3e490aa9ffe8))


### Bug Fixes

* update eslint rules ([cf1974c](https://gitlab.com/cublueprint/beneficent/client/commit/cf1974c06d6373b7f7574160da7d9e990b61321e))

## 2.0.0 (2021-10-26)


### ⚠ BREAKING CHANGES

* client table filtering (EN-389)

### Features

* add client list view (EN-303) ([b9cae6d](https://gitlab.com/cublueprint/beneficent/client/commit/b9cae6dcdc69087c5e032b5f2e8e1c7ab10eaade))
* client table filtering (EN-389) ([0414ff6](https://gitlab.com/cublueprint/beneficent/client/commit/0414ff6f2f2f72a851d89cf311dfd0a62a547bcf))
* create application manually (EN-290) ([23d3bf0](https://gitlab.com/cublueprint/beneficent/client/commit/23d3bf043b195802de0d22cde89e068cf708fd1b))
* display officers by workload (EN-279) ([21134ec](https://gitlab.com/cublueprint/beneficent/client/commit/21134ec0235e0401dda441b8780f4c09594ac392))
* finish app overview (EN-379) ([a2ea340](https://gitlab.com/cublueprint/beneficent/client/commit/a2ea340479a99dcff82e5692b208f90cf4fdf940))
* get nginx config (EN-361) ([ea378d1](https://gitlab.com/cublueprint/beneficent/client/commit/ea378d17187ce3539cb4d74aff737742c4903f20))


### Bug Fixes

* add jwt decoding and current user to LO dropdown (EN-287) ([7393fbe](https://gitlab.com/cublueprint/beneficent/client/commit/7393fbeec6140a1bc0b97416298ab81d31ad6021))
* add nginx config url (EN-361) ([7dd4f73](https://gitlab.com/cublueprint/beneficent/client/commit/7dd4f7314e0ede77f139ff12bf7e7a72803f79db))
* invalid paths in pipeline (EN-361) ([0839664](https://gitlab.com/cublueprint/beneficent/client/commit/08396648ee08cf341513cdedad7cd9a358d5b3a3))
* missing nginx config (EN-361) ([7b6dae0](https://gitlab.com/cublueprint/beneficent/client/commit/7b6dae03461cc24cc162b7d6c59b8fc458698bc4))
* missing nginx config url (EN-361) ([fba65e0](https://gitlab.com/cublueprint/beneficent/client/commit/fba65e0e0cd0f2668c5f2d17f9d6fd188a709768))
* move nginx config to prod (EN-361) ([28c58de](https://gitlab.com/cublueprint/beneficent/client/commit/28c58de640a4bc5d2bfe1c9ed4384fd97012a12c))

## 1.0.0 (2021-08-16)


### Features

* get nginx config (EN-361) ([ea378d1](https://gitlab.com/cublueprint/beneficent/client/commit/ea378d17187ce3539cb4d74aff737742c4903f20))


### Bug Fixes

* add nginx config url (EN-361) ([7dd4f73](https://gitlab.com/cublueprint/beneficent/client/commit/7dd4f7314e0ede77f139ff12bf7e7a72803f79db))
* invalid paths in pipeline (EN-361) ([0839664](https://gitlab.com/cublueprint/beneficent/client/commit/08396648ee08cf341513cdedad7cd9a358d5b3a3))
* missing nginx config (EN-361) ([7b6dae0](https://gitlab.com/cublueprint/beneficent/client/commit/7b6dae03461cc24cc162b7d6c59b8fc458698bc4))
* missing nginx config url (EN-361) ([fba65e0](https://gitlab.com/cublueprint/beneficent/client/commit/fba65e0e0cd0f2668c5f2d17f9d6fd188a709768))
* move nginx config to prod (EN-361) ([28c58de](https://gitlab.com/cublueprint/beneficent/client/commit/28c58de640a4bc5d2bfe1c9ed4384fd97012a12c))
